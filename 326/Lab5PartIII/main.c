/**********************************************************************
 * Title: Lab 5
 * Authors: George, Bilguun
 * Date: 10/4/2019
 * Instructor: Dr Krug
 * Description: Part III
 * At restart, the date and time must be entered. Next, a * can be
 * pressed followed by a 1, 2, or 3.
 * * -> 1 will print the month day and year.
 * * -> 2 will print the hours minutes and seconds.
 * * -> 3 will print the temperature in celsius.
 *********************************************************************/

#include "stdio.h"
#include "msp.h"
#define SLAVE_ADDR 0x68

//Prototypes
void delayMs(int n);
void I2C1_init(void);
int I2C1_burstRead(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data);
int I2C1_burstWrite(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data);
uint8_t Read_Keypad();
void keypad_init();

unsigned char timeDateReadback[7];
unsigned char timeDateToSet[15] = {0x00, 0x38, 0x17, 0x04, 0x02, 0x10, 0x19, 0};

uint8_t num, pressed=0;
int input[14];


int main(void)
{
    System_Init();

    //This array will hold the information

    //This is the array that holds the information that needs to be sent to the RTC.
    //It is pre loaded with values for debugging purposes:
    //2019 October 2, Wednesday, 17:38:00

    //Read from the keypad
 //   RecordKeypadInput(pressed);

    //Transfer the input values to the time array that will be sent to the RTC.
 //   StageRecordedValues(timeDateToSet, input);

    //Write the given time to the RTC
    I2C1_burstWrite(SLAVE_ADDR, 0, 7, timeDateToSet);

    while(1)
    {
        //Read the keypad until a * has been recevied.
        WaitForUser();

        //Read an input from the user selecting a display.
        DisplaySelection();

        //Print out information based on the user selection.
        DisplayOutput();
    }
}

/**********************************************************************
 * Performs all functions calls and register adjustments to initialize
 * the program.
 *********************************************************************/
void System_Init()
{
    //Stop watchdog timer
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;

    keypad_init();
    I2C1_init();
}

/**********************************************************************
 * Blocks the function from proceeding until a * has been read from
 * the keypad.
 *********************************************************************/
void WaitForUser()
{
    printf("\nTo begin display selection, press *...\n");

    while (input[0]!=10)
    {
        pressed = Read_Keypad();
    }
    input[0] = 0;
    pressed = 0;
}

/**********************************************************************
 * Blocks the program from proceeding until a numerical input has been
 * given by the user.
 *********************************************************************/
void DisplaySelection()
{
    printf("\nEnter 1 for month-day-year\nEnter 2 for hour-minute-second\nEnter 3 for temperature\n");
    //Read the keypad until a number has been received.
    while(!pressed)
    {
        pressed = Read_Keypad();
    }

}

/**********************************************************************
 * Displays information on the screen based on what was selected
 * by the user.
 *
 * The user selection is stored in the input[] array.
 *********************************************************************/
void DisplayOutput()
{
    //Print month, day, year
    if (input[0] == 1)
    {
        I2C1_burstRead(SLAVE_ADDR, 4, 3, timeDateReadback);
        printf("\nMonth-Day-Year");
        printf("\n%x-%x-20%.2x\n\n", timeDateReadback[1], timeDateReadback[0],
               timeDateReadback[2]);
    }

    //Print hour, minutes, seconds
    else if (input[0] == 2)
    {
        I2C1_burstRead(SLAVE_ADDR, 0, 3, timeDateReadback);
        printf("\nhour:minute:second");
        printf("\n%x:%.2x:%.2x\n\n", timeDateReadback[2], timeDateReadback[1],
               timeDateReadback[0]);
    }

    //Print the temperature
    else if (input[0] == 3)
    {
        I2C1_burstRead(SLAVE_ADDR, 0x11, 0x02, timeDateReadback);

        //The data from memAddr 0x12 comes in as two bits that represent quarters of a degree,
        //and this tree of if statements converts those numbers to decimal.
        int DegreeDecimal;
        if(timeDateReadback[1]==192)
            DegreeDecimal = 75;
        else if(timeDateReadback[1]==128)
            DegreeDecimal = 50;
        else if(timeDateReadback[1]==64)
            DegreeDecimal = 25;
        else if(timeDateReadback[1] == 0)
            DegreeDecimal = 0;

        printf("\nTemperature in Celsius: %d.%.2d\n\n", timeDateReadback[0], DegreeDecimal);
    }
    else
        printf("Invalid Selection: %d\n\n", input[0]);
}

/**********************************************************************
 * Stores the values from the keypad in the input array. The first value will
 * be stored at the end of the input array.
 *********************************************************************/
void RecordKeypadInput(uint8_t pressed)
{
    uint8_t receivedCount = 0;
    printf("Please enter time in the format: \n1)YY\n2)MM\n3)DD\n4)DOW\n5)HR\n6)mm\n7)ss\n");
    //Until all values for the date and time are received, wait for them to be given.
    while (receivedCount < 13)
    {
        pressed = Read_Keypad();
        if (pressed)
        {
            uint8_t index;
            /*
             * This for loop will shift numbers deeper into the array.
             * Numbers stored in lower indices (ex input[10]), will be
             * moved to next indice up (input[10]->input[11]).
             */
            for (index = 13; index > 0; index--)
            {
                input[index] = input[index - 1];
            }
            receivedCount++; //Add to the running count of numbers received.
            pressed = 0; //This tells the while loop to begin looking for a new button pressed.
        }
    }
}

/**********************************************************************
 * Stores the recorded values into the array that will be sent to the RTC via I2C
 *********************************************************************/
void StageRecordedValues(unsigned char timeDateToSet[15])
{
    // ss mm hr DOW dd mm yy
    //Transfer the input values to the time array that will be sent to the RTC.
    timeDateToSet[0] = Converter(input[2], input[1]); //ss
    timeDateToSet[1] = Converter(input[4], input[3]); //mm
    timeDateToSet[2] = Converter(input[6], input[5]); //hr
    timeDateToSet[3] = input[7]; //DOW
    timeDateToSet[4] = Converter(input[9], input[8]); //DD
    timeDateToSet[5] = Converter(input[11], input[10]); //MM
    timeDateToSet[6] = Converter(input[13], input[12]); //YY
}

/**********************************************************************
 * Converts the decimal value into hexidecimals numbers where the digits
 * of the hexidecimal match the digits of the decimal values.
 *
 * ex: 19 (base 10) -> 19 (base 16)
 * ex: 35 (base 10) -> 35 (base35)
 *********************************************************************/
int Converter(uint8_t tens, uint8_t ones)
{
    return tens*16+ones;
}

/**********************************************************************
 * Writes data to a given address on the specified slave device.
 * Data is written via the I2C communication standard.
 *
 * @param slaveAddr The address of the slave device
 * @param memAddr The memory address of the slave to be overwritten
 * @param byteCount The number of bytes being sent
 * @param *data The address of the array storing data to be sent
 *
 * Modified from code written by Dr Kandalaft
 *********************************************************************/
int I2C1_burstWrite(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data)
 {
     if (byteCount <= 0)
         //No write was performed
         return -1;

     //Set up slave address
     EUSCI_B1->I2CSA = slaveAddr;
     //Enable transmitter
     EUSCI_B1->CTLW0 |= 0x0010;
     //Generate start and send slave address
     EUSCI_B1->CTLW0 |= 0x0002;
     //Wait until ready to transmit
     while(!(EUSCI_B1->IFG & 2));
     //Send memory address to slave
     EUSCI_B1->TXBUF = memAddr;

     //Send data one byte at a time
     do
     {
         //Wait until port is ready for transmit
         while (!(EUSCI_B1->IFG & 2));
         //Send data to slave
         EUSCI_B1->TXBUF = *data++;
         byteCount--;
     } while (byteCount > 0);

     //Wait until last transmit is done
     while(!(EUSCI_B1->IFG & 2));
     //Send stop
     EUSCI_B1->CTLW0 |=0x0004;
     //Wait until stop is sent
     while (EUSCI_B1->CTLW0 & 4);

     //Return zero for no error
     return 0;
 }

/**********************************************************************
 * Configure UCB1 as I2C on pins 6.5 (CLK) and 6.4 (SDA).
 *********************************************************************/
void I2C1_init(void)
{
    //Disable UCB1 during configuration
    EUSCI_B1 -> CTLW0 |=  1;
    //7-bit slave address, master, I2C, sync mode, SMCLK
    EUSCI_B1 -> CTLW0  =  0x0F81;
    //Set clock prescaler 3 MHz / 30 = 100 kHz
    EUSCI_B1 -> BRW    =  30;
    //P6.5 and P6.4 for UCB1 clk is 6.5, sda is 6.4
    P6       -> SEL0  |=  0x30;
    P6       -> SEL1  &= ~0x30;
    //Enable UCB1 after configuration
    EUSCI_B1 -> CTLW0 &= ~1;
}

/**********************************************************************
 * Use burst read to read multiple bytes from a specified memory
 * address on from a specified slave.
 *
 * @param slaveAddr The address of the slave device
 * @param memAddr The memory address of the slave to be read
 * @param byteCount The number of bytes to retrieve
 * @param *data The address of the array to store the bytes to
 *
 * Modified from code written by Dr Kandalaft
 *********************************************************************/
int I2C1_burstRead(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data)
{
    if (byteCount <= 0)
        //No read was performed
        return -1;

    //Setup slave address
    EUSCI_B1 -> I2CSA  = slaveAddr;
    //Enable transmitter
    EUSCI_B1 -> CTLW0 |= 0x0010;
    //Generate START and send slave address
    EUSCI_B1 -> CTLW0 |= 0x0002;
    //Wait until slave address is sent
    while((EUSCI_B1->CTLW0 & 2));
    //Send memory address to slave
    EUSCI_B1 -> TXBUF = memAddr;
    //Wait till last transmit is done
    while(!(EUSCI_B1 -> IFG & 2));
    //Enable receiver
    EUSCI_B1 -> CTLW0 &= ~0x0010;
    //Generate RESTART and send slave address
    EUSCI_B1 -> CTLW0 |= 0x0002;
    //Wait till RESTART is finished
    while(EUSCI_B1 -> CTLW0 & 2);

    //Receive data one byte at a time
    do
    {
        //When only one byte of data is left
        if (byteCount == 1)
            //Setup to send STOP after the last byte is received
            EUSCI_B1 -> CTLW0 |= 0x0004;
        //Wait till data is received
        while(!(EUSCI_B1 -> IFG & 1));
        //Read the received data
        *data++ = EUSCI_B1 -> RXBUF;
        byteCount--;
    } while (byteCount);

    //Wait until stop is sent
    while(EUSCI_B1 -> CTLW0 & 4) ;

    //Return zero for no error
    return 0;
}

/**********************************************************************
 * Delay the sysytem by a given number of miliseconds.
 *
 * Uses a for-loop delay method.
 *
 * System clock at 3 MHz
 *
 * @param miliseconds to be dealyed
 *********************************************************************/
void delayMs(int n)
{
    int j, i;
    for(j = 0; j < n; j++)
        //Delay 1 ms
        for(i = 750; i > 0; i--);
}

/**********************************************************************
 * Reads the keypad, and calculates the value of the button that was pressed.
 * This is done by setting every column to be an input, and then each column,
 * individually, is set to an output sending LOW. If any row is read to be LOW
 * the rows will be read individually to determine what number was pressed.
 *
 * @return true when a number was read from the keypad
 *********************************************************************/
uint8_t Read_Keypad()
{
    uint8_t col, row;
    for (col = 0; col < 3; col++)
    {
        P4->DIR = 0x00; // Set Columns to inputs
        P4->DIR |= BIT(4 + col); // Set column 3 to output
        P4->OUT &= ~BIT(4 + col); // Set column 3 to LOW
        delayMs(10); // Delay the while loop
        row = P4->IN & 0x0F; // read all rows
        while (!(P4IN & BIT0 ) | !(P4IN & BIT1 ) | !( P4IN & BIT2 )
                | !( P4IN & BIT3 ))
            ;
        delayMs(5); //stall the code as a secondary debounce
        if (row != 0x0F)
            break; // if one of the input is low, some key is pressed.
    }
    P4->DIR = 0x00; // Set Columns to inputs
    if (col == 3)
        return 0;
    if (row == 0x0E)
        num = 0 + col + 1; // key in row 0
    else if (row == 0x0D)
        num = 3 + col + 1; // key in row 1
    else if (row == 0x0B)
        num = 6 + col + 1; // key in row 2
    else if (row == 0x07)
        num = 9 + col + 1; // key in row 3
    else
        num = 40;

    //If a * was pressed, save this to input[0], but return 0
    //because a number wasn't pressed
    if (num == 10)
    {
        input[0] = 10;
        return 0;
    }
    //Return 0 for a #
    else if (num == 12)
        return 0;
    //Set num to 0 for a 0
    else if (num == 11)
        num = 0;

    printf("Value entered: %d\n", num);
    input[0] = num;
    return 1;
}

/**********************************************************************
 * Setup port 4 to receive input from a keypad.
 * All pins 0-6 are GPIO input with pullup resistors.
 *********************************************************************/
void keypad_init()
{
    P4->SEL0 &= ~0x7F;
    P4->SEL1 &= ~0x7F;
    P4->DIR &= ~0xFF;
    P4->REN |= 0xFF;
    P4->OUT |= 0xFF;
// 456 are columns
// 0123 are rows
}
