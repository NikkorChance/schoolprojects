/**********************************************************************
 * Title: Lab 5
 * Authors: George, Bilguun
 * Date: 10/3/2019
 * Instructor: Dr Krug
 * Description: Part I
 * At restart, the date and time must be entered. Then a * can be
 * pressed to cause the date and time to be printed to the console.
 *********************************************************************/



//Preprocessor directives
#include "stdio.h"
#include "msp.h"
#define SLAVE_ADDR 0x68

//Prototypes
void delayMs(int n);
void I2C1_init(void);
int I2C1_burstRead(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data);
int I2C1_burstWrite(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data);
uint8_t Read_Keypad();
void keypad_init();

uint8_t num, pressed=0;
int input[14];



int main(void)
{
    //Stop watchdog timer
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;

    keypad_init();

    unsigned char timeDateReadback[7];
    //Init onboard LEDs
    P2->DIR &=~ 7;
    //Init pins
    I2C1_init();

    //2019 November 21, Thursday, 16:58:55
    unsigned char timeDateToSet[15] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};

    //Read from the keypad
   // RecordKeypadInput(pressed);

    //Transfer the input values to the time array that will be sent to the RTC.
//    StageRecordedValues(timeDateToSet, input);

    //Write the first seven bytes of the registers
    I2C1_burstWrite(SLAVE_ADDR, 0, 7, timeDateToSet);



    while(1)
    {


//        pressed = Read_Keypad();
//        if (input[0] == 10)
//        {

            //Part 1 code
            //Read the first seven bytes of the registers
            I2C1_burstRead(SLAVE_ADDR, 0, 7, timeDateReadback);
            //Write second count to LEDs
            P2->OUT = timeDateReadback[0];
            //Print date and time
            printf("%x:%x:%x %x %x-%x-20%x\n", timeDateReadback[2],
                   timeDateReadback[1], timeDateReadback[0],
                   timeDateReadback[3], timeDateReadback[5],
                   timeDateReadback[4], timeDateReadback[6]);

            input[0] = 0;
//        }
    }
}

/**
 * Stores the values from the keypad in an array
 * This is blocking
 */
void RecordKeypadInput(uint8_t pressed)
{
    uint8_t receivedCount = 0;
    printf("Please enter time in the format: \n1)YY\n2)MM\n3)DD\n4)DOW\n5)HR\n6)mm\n7)ss\n");
    //Until all values for the date and time are received, wait for them to be given.
    while (receivedCount < 13)
    {
        pressed = Read_Keypad();
        if (pressed)
        {
            uint8_t index;
            /*
             * This for loop will shift numbers deeper into the array.
             * Numbers stored in lower indices (ex input[10]), will be
             * moved to next indice up (input[10]->input[11]).
             */
            for (index = 13; index > 0; index--)
            {
                input[index] = input[index - 1];
            }
            receivedCount++; //Add to the running count of numbers received.
            pressed = 0; //This tells the while loop to begin looking for a new button pressed.
        }
    }
}

/**
 * Stores the recorded values into the array that will be sent to the RTC via I2C
 */
void StageRecordedValues(unsigned char timeDateToSet[15])
{
    // ss mm hr DOW dd mm yy
    //Transfer the input values to the time array that will be sent to the RTC.
    timeDateToSet[0] = Converter(input[2], input[1]); //ss
    timeDateToSet[1] = Converter(input[4], input[3]); //mm
    timeDateToSet[2] = Converter(input[6], input[5]); //hr
    timeDateToSet[3] = input[7]; //DOW
    timeDateToSet[4] = Converter(input[9], input[8]); //DD
    timeDateToSet[5] = Converter(input[11], input[10]); //MM
    timeDateToSet[6] = Converter(input[13], input[12]); //YY
}

/*
 * Converts the decimal value into hexidecimals numbers where the digits
 * of the hexidecimal match the digits of the decimal values.
 * ex: 19 (base 10) -> 19 (base 16)
 * ex: 35 (base 10) -> 35 (base35)
 */
int Converter(uint8_t tens, uint8_t ones)
{
    return tens*16+ones;
}

//
int I2C1_burstWrite(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data)
 {
     if (byteCount <= 0)
         //No write was performed
         return -1;

     //Set up slave address
     EUSCI_B1->I2CSA = slaveAddr;
     //Enable transmitter
     EUSCI_B1->CTLW0 |= 0x0010;
     //Generate start and send slave address
     EUSCI_B1->CTLW0 |= 0x0002;
     //Wait until ready to transmit
     while(!(EUSCI_B1->IFG & 2));
     //Send memory address to slave
     EUSCI_B1->TXBUF = memAddr;

     //Send data one byte at a time
     do
     {
         //Wait until port is ready for transmit
         while (!(EUSCI_B1->IFG & 2));
         //Send data to slave
         EUSCI_B1->TXBUF = *data++;
         byteCount--;
     } while (byteCount > 0);

     //Wait until last transmit is done
     while(!(EUSCI_B1->IFG & 2));
     //Send stop
     EUSCI_B1->CTLW0 |=0x0004;
     //Wait until stop is sent
     while (EUSCI_B1->CTLW0 & 4);

     //Return zero for no error
     return 0;
 }


//Configure UCB1 as I2C
void I2C1_init(void)
{
    //Disable UCB1 during configuration
    EUSCI_B1 -> CTLW0 |=  1;
    //7-bit slave address, master, I2C, sync mode, SMCLK
    EUSCI_B1 -> CTLW0  =  0x0F81;
    //Set clock prescaler 3 MHz / 30 = 100 kHz
    EUSCI_B1 -> BRW    =  30;
    //P6.5 and P6.4 for UCB1 clk is 6.5, sda is 6.4
    P6       -> SEL0  |=  0x30;
    P6       -> SEL1  &= ~0x30;
    //Enable UCB1 after configuration
    EUSCI_B1 -> CTLW0 &= ~1;
}

//Use burst read to read multiple bytes from consecutive locations
int I2C1_burstRead(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data)
{
    if (byteCount <= 0)
        //No read was performed
        return -1;

    //Setup slave address
    EUSCI_B1 -> I2CSA  = slaveAddr;
    //Enable transmitter
    EUSCI_B1 -> CTLW0 |= 0x0010;
    //Generate START and send slave address
    EUSCI_B1 -> CTLW0 |= 0x0002;
    //Wait until slave address is sent
    while((EUSCI_B1->CTLW0 & 2));
    //Send memory address to slave
    EUSCI_B1 -> TXBUF = memAddr;
    //Wait till last transmit is done
    while(!(EUSCI_B1 -> IFG & 2));
    //Enable receiver
    EUSCI_B1 -> CTLW0 &= ~0x0010;
    //Generate RESTART and send slave address
    EUSCI_B1 -> CTLW0 |= 0x0002;
    //Wait till RESTART is finished
    while(EUSCI_B1 -> CTLW0 & 2);

    //Receive data one byte at a time
    do
    {
        //When only one byte of data is left
        if (byteCount == 1)
            //Setup to send STOP after the last byte is received
            EUSCI_B1 -> CTLW0 |= 0x0004;
        //Wait till data is received
        while(!(EUSCI_B1 -> IFG & 1));
        //Read the received data
        *data++ = EUSCI_B1 -> RXBUF;
        byteCount--;
    } while (byteCount);

    //Wait until stop is sent
    while(EUSCI_B1 -> CTLW0 & 4) ;

    //Return zero for no error
    return 0;
}

/**
 *
 */
//System clock at 3 MHz
void delayMs(int n)
{
    int j, i;
    for(j = 0; j < n; j++)
        //Delay 1 ms
        for(i = 750; i > 0; i--);
}

/**
 *
 */
uint8_t Read_Keypad()
{
    uint8_t col, row;
    for (col = 0; col < 3; col++)
    {
        P4->DIR = 0x00; // Set Columns to inputs
        P4->DIR |= BIT(4 + col); // Set column 3 to output
        P4->OUT &= ~BIT(4 + col); // Set column 3 to LOW
        delayMs(10); // Delay the while loop
        row = P4->IN & 0x0F; // read all rows
        while (!(P4IN & BIT0 ) | !(P4IN & BIT1 ) | !( P4IN & BIT2 )
                | !( P4IN & BIT3 ))
            ;
        if (row != 0x0F)
            break; // if one of the input is low, some key is pressed.
    }
    P4->DIR = 0x00; // Set Columns to inputs
    if (col == 3)
        return 0;
    if (row == 0x0E)
        num = 0 + col + 1; // key in row 0
    else if (row == 0x0D)
        num = 3 + col + 1; // key in row 1
    else if (row == 0x0B)
        num = 6 + col + 1; // key in row 2
    else if (row == 0x07)
        num = 9 + col + 1; // key in row 3
    else
        num = 40;

    if (num == 10)
    {
        input[0] = 10;
        return 0;
    }
    else if (num == 12)
        return 0;
    else if (num == 11)
        num = 0;

    input[0] = num;
    return 1;
}

/**
 *
 */
void keypad_init()
{
    P4->SEL0 &= ~0x7F;
    P4->SEL1 &= ~0x7F;
    P4->DIR &= ~0xFF;
    P4->REN |= 0xFF;
    P4->OUT |= 0xFF;
// 456 are columns
// 0123 are rows
}
