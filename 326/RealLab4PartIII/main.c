#include "msp.h"
#include "math.h"

uint8_t Read_Keypad();
void keypad_init();
void SysTick_Init();
void SysTick_delay(uint16_t delay);
void Timer_A0_init();

volatile uint8_t num, pressed;

volatile int CCW = 0; //Counter clock wise counter
volatile int CW = 0; //Clock wise counter
volatile int buttonPressed = 0; //A flag for the rotar button

volatile int input; //Holds the input values

enum state
{
    keypad, rotar
};

/**
 * Program for 326 lab 4 Part III
 * Authors: George, Bilguun
 *
 * The program begins in a keypad state. Here the program waits for a
 * numerical input from a keypad. When input is given, the state
 * switches to the toar state. Here the program will count the
 * difference between the number of clockwise and counterclockwise
 * turns of the encoder. If the difference is ever larger than the
 * value received from the keypad, an LED will turn on.
 *
 * - Blue if the rotary encoder was turned CCW more than CW
 * - Red if the rotary encoder was turned CW more than CCW
 */
void main(void)
{
    System_Init();

    input = 0;
    pressed = 0;

    uint8_t state = keypad; //Initial state is the keypad

    while (1)
    {

        if (state == keypad)
        {
            KeypadState();

            state = rotar;
        }
        if (state == rotar)
        {
            RotarState();

            state = keypad;
        }
    }
}

/**
 * Calls all functions needed to start the program and enables interrupts
 */
void System_Init()
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD; // stop watchdog timer
    keypad_init(); // Call initialization functions for functionality
    SysTick_Init();
    Port5_Init(); //Initialize rotary encoder
    Port2_Init(); //Initialize LEDs

    NVIC_EnableIRQ(PORT5_IRQn);
    __enable_irq();
}

/**
 * A blocking function that will hold the program in the keypad state
 * until a button on they keypad is pressed.
 */
void KeypadState()
{
    while (!pressed)
        pressed = Read_Keypad();

    pressed = 0;
    CountLEDReset(); //All values are reset to ensure the next state has clean variables.
}

/**
 * A blocking function that will hold the program in the rotar state
 * until the button on the rotary encoder is pressed.
 */
void RotarState()
{
    while (!buttonPressed)
    {
        //Find the difference between the clockwise and counterclockwise turns
        int delta = CW - CCW;

        if (delta >= input) //cumulative CW turns >= the input
        {
            P2->OUT |= BIT4; //Red
            P2->OUT &= ~BIT5; //Blue
        }
        else if (delta <= input) //cumulative CCW >= the input
        {
            P2->OUT |= BIT5; //Blue
            P2->OUT &= ~BIT4; //Red
        }
        else //Cumulative is less than the input
        {
            P2->OUT &= ~BIT5; //Blue
            P2->OUT &= ~BIT4; //Red
        }
    }
    buttonPressed = 0;
    CountLEDReset();//All values are reset to ensure the next state has clean variables.
}

/**
 * Turns off LEDs and resets the rotary encoder counters.
 */
void CountLEDReset()
{
    CCW = 0;
    CW = 0;
    P2->OUT = 0;
}

/**
 * Turns BIT4 and BIT5 to GPIO outputs sending LOW
 * BIT4 is a red LED
 * BIT5 is a blue LED
 */
void Port2_Init()
{
    P2->SEL0 &= ~BIT4 | BIT5;
    P2->SEL1 &= ~BIT4 | BIT5;
    P2->DIR |= BIT4 | BIT5;
    P2->OUT = 0;
}

/**
 * Initialize port 5 to interface with the rotary encoder.
 *
 * BIT0 is CLK
 * BIT1 is DT
 * BIT2 is SW
 */
void Port5_Init()
{
    P5->SEL0 &= ~0x07;
    P5->SEL1 &= ~0x07;
    P5->DIR |= 0x07;
    P5->REN |= 0x07;
    P5->OUT |= 0x07;

    P5->IES |= 0x01; //CLK is rising edge, SW is falling edge
    P5->IE |= 0x05; //Enable interrupts on CLK and SW
    P5->IFG = 0;
}

/**
 * Is triggered by a rising edge from CLK or a falling edge from SW.
 *
 * If triggered by CLK, DT is checked to determine if rotation was
 * clockwise or counterclockwise.
 * If triggered by SW, a global flag is enabled.
 * If triggred by neither, all interrupt flags are reset.
 */
void PORT5_IRQHandler()
{
    //Interrupt from CLK?
    if (P5->IFG & BIT0)
    {
        //If DT is on, rotation was clockwise
        if (P5->IN & BIT1)
        {
            CW++;
        }
        //If DT isn't on, rotation was counterclockwise
        else
            CCW++;

        P5->IFG &= ~BIT0; //Disable only the CLK interrupt
    }
    //Interrupt from SW?
    else if (P5->IFG & BIT2)
    {
        buttonPressed++;
        P5->IFG &= ~BIT2; //Disable only the SW interrupt
    }
    else //If this was reached, an unhandled interrupt occurred
        P5->IFG = 0; //Reset all interrupts

}

/**
 * Initializes the keypad. All pins, 0-6 are outputs sending HIGH
 **/
void keypad_init()
{
    P4->SEL0 &= ~0x7F;
    P4->SEL1 &= ~0x7F;
    P4->DIR &= ~0xFF;
    P4->REN |= 0xFF;
    P4->OUT |= 0xFF;
// 456 are columns
// 0123 are rows
}

/**
 * Reads the keypad, and calculates the value of the button that was pressed.
 * This is done by setting every column to be an input, and then each column,
 * individually, is set to an output sending LOW. If any row is read to be LOW,
 **/
uint8_t Read_Keypad()
{
    uint8_t col, row, num;

    //Determine the column
    for (col = 0; col < 3; col++)
    {
        P4->DIR = 0x00; // Set Columns to inputs
        P4->DIR |= BIT(4 + col); // Set column 3 to output
        P4->OUT &= ~BIT(4 + col); // Set column 3 to LOW
        SysTick_delay(10); // Delay the while loop
        row = P4->IN & 0x0F; // read all rows
        while (!(P4IN & BIT0 ) | !(P4IN & BIT1 ) | !( P4IN & BIT2 )
                | !( P4IN & BIT3 )) ; //This is a debouncer
        if (row != 0x0F)
            break; // if one of the input is low, some key is pressed.
    }

    P4->DIR = 0x00; // Set Columns to inputs

    //Determine the button that was pressed
    if (col == 3)
        return 0;
    if (row == 0x0E)
        num = 0 + col + 1; // key in row 0
    else if (row == 0x0D)
        num = 3 + col + 1; // key in row 1
    else if (row == 0x0B)
        num = 6 + col + 1; // key in row 2
    else if (row == 0x07)
        num = 9 + col + 1; // key in row 3
    else
        num = 99;


    input = num; //Saves the number to the input[0] global variable
    return num; //Returns as pressed
}

/**
 * Initializes Systick to have max load, no interrupts, and be on.
 **/
void SysTick_Init()
{
    SysTick->CTRL = 0;
    SysTick->LOAD = 0x00FFFFFF; //Use max load
    SysTick->VAL = 0;
    SysTick->CTRL = 0X00000005; //Core clock, enabled
}
/**
 * Uses an input of mili seconds to delay the system.
 *
 * @delay the desired number of miliseconds to delay
 **/
void SysTick_delay(uint16_t delay)
{
    SysTick->LOAD = ((delay * 3000) - 1);
    SysTick->VAL = 0;
    while ((SysTick->CTRL & 0x00010000) == 0)
        ;
}

