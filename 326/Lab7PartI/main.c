#include "msp.h"

void Port1_Init();
void Port1_IRQHandler();

#define TURNDELAY 15

enum state{LineA, LineB, LineC, LineD};
enum direction{CCW,CW};
volatile uint8_t rotation = CCW;
volatile uint8_t buttonState = 0;
volatile uint8_t buttonFlag = 0;
volatile uint8_t NextWire = LineD;


/*********************************************************************
 * Lab 7 Part I:
 * Authors: Bilguun, George
 * Date: 10/18/2019
 * Instructor: Dr. Krug
 * Description:
 *
 * This program interfaces a button and a motor. When the button is
 * pressed the motor will rotate ten steps forward. On the next button
 * press the motor will rotate ten steps backward. This cycle will
 * continue for proceeding button presses.
 ********************************************************************/
void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

	System_Init();

    while (1)
    {
        if(buttonFlag)
        {

            Motor_Clockwise(50);
            if (ButtonDebounce())
            {
                if (rotation == CW)
                    Motor_Clockwise(50);
                else if (rotation == CCW)
                    Motor_Counterclockwise(50);
            }
        }
    }
}


/**********************************************************************
 * Performs all functions calls and register adjustments to initialize
 * the program.
 *********************************************************************/
void System_Init()
{
    __enable_irq();
        NVIC_EnableIRQ (PORT1_IRQn);
        SysTick_Init();
        Port2_Init();
        Port1_Init();
}

/**********************************************************************
 * Accepts the desired number of steps for the motor to turn in the
 * clockwise direction, and runs the motor through that many steps.
 *
 *
 * @param steps The number of steps to rotate the motor
 *********************************************************************/
void Motor_Clockwise(uint8_t steps)
{
    steps++;  //Increment steps by 1 to accommodate the NextWire change

    NextWire++; //Change the next wire to be the current wire

    //If NextWire was pushed passed LineD, NextWire should be LineA
    if(NextWire > LineD) NextWire = LineA;

    //Turn off all wires
    P2->OUT &= ~0xF0;

    //Run the motor through every step
    while(steps > 0)
    {
        RunMotorClockWise();
        steps--;
    }
}

/**********************************************************************
 * Accepts the desired number of steps for the motor to turn in the
 * clockwise direction, and runs the motor through that many steps.
 *
 *
 * @param steps The number of steps to rotate the motor
 *********************************************************************/
void Motor_Counterclockwise(uint8_t steps)
{
    steps++; //Increment steps by 1 to accommodate the NextWire change

    NextWire--; //Change the next wire to be the current wire

    if(NextWire == 255) NextWire = LineD; //Subtracting from LineA will cause roll over, this corrects that

    //Turn off all wires
    P2->OUT &= ~0xF0;

    //Run the motor through every step
    while(steps > 0)
       {
           RunMotorCounterClockWise();
           steps--;
       }
}


/**********************************************************************
 * Will run one step of the motor based on the current state of the motor
 *********************************************************************/
void RunMotorCounterClockWise()
{
    switch (NextWire)
    {
    case (LineA):
        P2->OUT &= ~0xF0;
        P2->OUT |= BIT4;
        NextWire = LineD;
        break;
    case (LineB):
        P2->OUT &= ~0xF0;
        P2->OUT |= BIT5;
        NextWire = LineA;
        break;
    case (LineC):
        P2->OUT &= ~0xF0;
        P2->OUT |= BIT6;
        NextWire = LineB;
        break;
    case (LineD):
        P2->OUT &= ~0xF0;
        P2->OUT |= BIT7;
        NextWire = LineC;
        break;
    }
    SysTick_delay(TURNDELAY);
}

/**********************************************************************
 * Will run one step of the motor based on the current state of the motor
 *********************************************************************/
void RunMotorClockWise()
{
    switch (NextWire)
    {
    case (LineA):
        P2->OUT &= ~0xF0;
        P2->OUT |= BIT4;
        NextWire = LineB;
        break;
    case (LineB):
        P2->OUT &= ~0xF0;
        P2->OUT |= BIT5;
        NextWire = LineC;
        break;
    case (LineC):
        P2->OUT &= ~0xF0;
        P2->OUT |= BIT6;
        NextWire = LineD;
        break;
    case (LineD):
        P2->OUT &= ~0xF0;
        P2->OUT |= BIT7;
        NextWire = LineA;
        break;
    }
    SysTick_delay(TURNDELAY);
}

/**********************************************************************
 * Setup port 2 to run the motor
 *********************************************************************/
void Port2_Init()
{
    //GPIO on bits 4,5,6,7
    P2->SEL0 &= ~0xF0;
    P2->SEL1 &= ~0xF0;
    //Output on BIT4,5,6,7
    P2->DIR  |= 0xF0;
    //All outputs are zero
    P2->OUT  = 0;
}

/**********************************************************************
 * Setup a bit 6 port 1 as an input.
 *********************************************************************/
void Port1_Init()
{
    //GPIO on bit 6
    P1->SEL0 &= ~BIT6;
    P1->SEL1 &= ~BIT6;
    //Input on bit 6
    P1->DIR  &= ~BIT6;
    //Pull up/down resistor on bit 6
    P1->REN  |=  BIT6;
    //Pull up on bit 6
    P1->OUT  |= BIT6;

    //Interrupt on a falling edge
    P1->IES  &= ~BIT6;
    //All interrupt flags are disabled on port 1
    P1->IFG   =     0;
    //Enable the interrupt on bit 6
    P1->IE   |=  BIT6;
}

/**********************************************************************
 * If the button on port 6 is pressed, set a flag so that the button is debonced.
 *********************************************************************/
void PORT1_IRQHandler()
{
    if(P1->IFG & BIT6)
    {
        buttonFlag = 1;
        P1->IFG &= ~BIT6;
    }
    else
        P1->IFG = 0; //Disable all interrupt flags if none were handled.
}

/**********************************************************************
 * Check if the button was pressed. If it was, set the direction
 *********************************************************************/
uint8_t ButtonDebounce()
{
    static uint16_t button = 0; //Current debounce status
    button = (button << 1) | (P1->IN & BIT6 ) | 0xe000;

    if (button == 0xf000)
    {
        buttonFlag = 0;
        SetDirection();
        SysTick_delay(1);
        return 1;
    }

    return 0;
}

/**********************************************************************
 * Toggle the direction of rotation.
 *********************************************************************/
void SetDirection()
{
    if (rotation == CW)
        rotation = CCW;
    else if (rotation == CCW)
        rotation = CW;
}

/**********************************************************************
 * Initializes Systick to have maximum load, no interrupts, and to be enabled
 **********************************************************************/
void SysTick_Init()
{
    SysTick->CTRL = 0;
    SysTick->LOAD = 0x00FFFFFF; //Max load
    SysTick->VAL = 0;
    SysTick->CTRL = 0X00000005; //Use core clock, enabled
}

/**********************************************************************
 * Uses an input of mili seconds to delay the system.
 **********************************************************************/
void SysTick_delay(uint16_t delay)
{
    SysTick->LOAD = ((delay * 3000) - 1);
    SysTick->VAL = 0;
    while ((SysTick->CTRL & 0x00010000) == 0)
        ;
}
