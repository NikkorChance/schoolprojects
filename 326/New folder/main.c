#include "msp.h"

#define B1 BIT6 //Button 1
#define B2 BIT7 //Button 2
//These macros are used for grey encoding of the direction
#define CCW 0x01
#define CW  0x02

#define SLAVE_ADDRESS 0x48
#define MOTORDELAY 715

volatile uint8_t button6Flag = 0;
volatile uint8_t button7Flag = 0;
volatile uint8_t slaveComplete = 0;

volatile uint8_t input[2];
volatile uint8_t num, pressed=0;
volatile uint8_t direction = 0;

volatile char TXData;
volatile uint8_t RXData[5] = {0};
volatile RXDataPointer;


/*********************************************************************
 * Lab 7 Part III:
 * Authors: Bilguun, George
 * Date: 10/18/2019
 * Instructor: Dr. Krug
 * Description: Master
 * Interfaces with a slave MSP with slave address 0x48.
 *
 * This program accepts a keypad input 1-9. This will be the number of
 * cycles to rotate a motor. The program also accepts input from one
 * of two buttons. When one button is pressed, the number of cycles
 * and the direction is sent to the slave via I2C.
 *
 * Data being sent: | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
 *                  |  # of cycles  |   direction   |
 *
 * The direction is grey encoded. CCW is 0x1, CW is 0x2.
 *
 ********************************************************************/
void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer
    System_Init();

    //Get number of cycles
    GetCycles();

    while (1)
    {
        //Get direction of rotation
        uint8_t direction = GetDirection();

        //Combine the inputs and set flag for transfer
        Prep_TransferCycleDir_ToSlave(direction);

        //Calculate the position and print it to the screen
        PrintPosition();
    }
}

/**********************************************************************
 * Performs all functions calls and register adjustments to initialize
 * the program.
 *********************************************************************/
void System_Init()
{
    Port1_Init();
    Port2_Init();
    I2C_Init();
    keypad_init();
    SysTick_Init();

    __enable_irq(); // All interrupts are enabled

    NVIC->ISER[0] = 0x00100000; // EUSCI_B0 interrupt is enabled in NVIC
    NVIC_EnableIRQ (PORT2_IRQn);

    //Hold while the stop transmit bit is set.
    while (EUSCI_B0->CTLW0 & EUSCI_B_CTLW0_TXSTP)
        ;
    //Receive break character, transmit break
    EUSCI_B0->CTLW0 |= EUSCI_B_CTLW0_TR | EUSCI_B_CTLW0_TXSTT;
}


/**********************************************************************
 * Configure UCB1 as I2C on pins 1.7 (SCL) and 1.6 (SDA).
 *********************************************************************/
void I2C_Init()
{
    EUSCI_B0->CTLW0 |= EUSCI_B_CTLW0_SWRST; // Hold EUSCI_B0 module in reset state
    EUSCI_B0->CTLW0 |= EUSCI_B_CTLW0_MODE_3 | EUSCI_B_CTLW0_MST
            | EUSCI_B_CTLW0_SYNC;
    EUSCI_B0->CTLW0 |= EUSCI_B_CTLW0_UCSSEL_2; // Select SMCLK as EUSCI_B0 clock
    EUSCI_B0->BRW = 0x001E; // Set BITCLK = BRCLK / (UCBRx+1) = 3 MHz / 30 = 100 kHz
    EUSCI_B0->I2CSA = SLAVE_ADDRESS;
    EUSCI_B0->CTLW0 &= ~EUSCI_B_CTLW0_SWRST; // Clear SWRST to resume operation
}

/*********************************************************************
 * If the transmit interrupt was triggered,
 * transmit the data stored in TXData to the slave MSP.
 ********************************************************************/
void EUSCIB0_IRQHandler(void)
{
    uint32_t status = EUSCI_B0->IFG; // Get EUSCI_B0 interrupt flag
    EUSCI_B0->IFG &= ~ EUSCI_B_IFG_TXIFG0; // Clear EUSCI_B0 TX interrupt flag

    if (status & EUSCI_B_IFG_TXIFG0)
    { // Check if transmit interrupt occurs
        EUSCI_B0->TXBUF = TXData; // Load current TXData value to transmit buffer
        EUSCI_B0->IE &= ~EUSCI_B_IE_TXIE0;
    } // Disable EUSCI_B0 TX interrupt
}

/*********************************************************************
 * Setup port 1 for I2C communication
 ********************************************************************/
void Port1_Init()
{
    P1->SEL0 |= BIT6 | BIT7; // P1.6 and P1.7 as UCB0SDA and UCB0SCL
}

/*********************************************************************
 * Sets up port 2 pin 6 and 7 to interrupt with a pushbutton.
 ********************************************************************/
void Port2_Init()
{
    //GPIO
    P2->SEL0 &= ~(B1|B2);
    P2->SEL1 &= ~(B1|B2);
    //Set to input
    P2->DIR  &= ~(B1|B2);
    //Enable pull up/down resistor
    P2->REN  |=  (B1|B2);
    //Pull up resistor
    P2->OUT  |= (B1|B2);

    //Interrupt on a rising edge
    P2->IES  &= ~(B1|B2);
    //All interrupt flags are disabled on port 1
    P2->IFG   =     0;
    //Enable the interrupts
    P2->IE   |=  (B1|B2);
}

/*********************************************************************
 * Holds the program until input from the keypad has been received.
 ********************************************************************/
void GetCycles()
{
    //Wait until a successful read has occurred from the keypad
    while (!pressed)
        pressed = Read_Keypad();
    pressed = 0;
}

/*********************************************************************
 * Holds the program until a direction new direction is set. A
 * direction is set by pressing B1 or B2.
 ********************************************************************/
uint8_t GetDirection()
{
   int direction = 0;

    while (!direction)
    {
        if (button6Flag)
            if(Button6Debounce())
                direction = CCW; //If B1 passed, CCW
        if (button7Flag)
            if(Button7Debounce())
                direction = CW; //If B2 passed, CW
    }
}

/*********************************************************************
 * Combines the entered information into 8-bits and then enables the
 * I2C transfer interrupt.
 *
 * @param direction The desired direction of rotation
 ********************************************************************/
void Prep_TransferCycleDir_ToSlave(uint8_t direction)
{
    //Sets number of cycles to the four most significant bits
    //Sets the direction to the four least significant bits
    TXData = (input[0] << 4) + direction;

    EUSCI_B0->IE |= EUSCI_B_IE_TXIE0; // Enable EUSCI_A0 TX interrupt
}

/*********************************************************************
 * Prints the number total number of cycles that the motor has moved.
 ********************************************************************/
void PrintPosition()
{
    delayMs(MOTORDELAY * input[0]);

    /**
     * Delta = Delta + number of cycles * the direction
     * where the direction, CCW or CW, is represented here as a
     * -1 or +1. This causes delta to increase or decrease based
     * on the direction or rotation.
     */
    static int delta += input[0]*(direction == 1 ? -1 : 1);
    printf("Total number of Cycles: %d\n",delta);
}

/*********************************************************************
 * If B1 or B2 were pressed, set the appropriate button flags.
 * These flags signal the need to denounce the button.
 ********************************************************************/
void PORT2_IRQHandler()
{
    if(P2->IFG & B1)
    {
        button6Flag = 1;
        P2->IFG &= ~B1;
    }
    else if(P2->IFG & B2)
    {
        button7Flag = 1;
        P2->IFG &= ~B2;
    }
    else
        P2->IFG = 0; //Disable all interrupt flags if none were handled.
}

/*********************************************************************
 * When the button is actually pressed, the direction will be set to CCW
 ********************************************************************/
uint8_t Button6Debounce()
{
    static uint16_t button = 0; //Current debounce status
    button = (button << 1) | (P2->IN & B1 ) | 0xe000;

    if (button == 0xf000)
    {
        button6Flag = 0;
        return 1;
    }
    return 0;
}

/*********************************************************************
 * When the button is actually pressed, the direction will be set to CW
 ********************************************************************/
uint8_t Button7Debounce()
{
    static uint16_t button = 0; //Current debounce status
    button = (button << 1) | (P2->IN & B2 ) | 0xe000;

    if (button == 0xf000)
    {
        button7Flag = 0;
        return 1;
    }
    return 0;
}

/**********************************************************************
 * Delay the sysytem by a given number of miliseconds.
 *
 * Uses a for-loop delay method.
 *
 * System clock at 3 MHz
 *
 * @param miliseconds to be delayed
 *********************************************************************/
void delayMs(int n)
{
    int j, i;
    for(j = 0; j < n; j++)
        //Delay 1 ms
        for(i = 750; i > 0; i--);
}

/**********************************************************************
 * Reads the keypad, and calculates the value of the button that was pressed.
 * This is done by setting every column to be an input, and then each column,
 * individually, is set to an output sending LOW. If any row is read to be LOW
 * the rows will be read individually to determine what number was pressed.
 *
 * @return true when a number was read from the keypad
 *********************************************************************/
uint8_t Read_Keypad()
{
    uint8_t col, row;
    for (col = 0; col < 3; col++)
    {
        P4->DIR = 0x00; // Set Columns to inputs
        P4->DIR |= BIT(4 + col); // Set columns to output
        P4->OUT &= ~BIT(4 + col); // Set columns  to LOW
        delayMs(10); // Delay the while loop
        row = P4->IN & 0x0F; // read all rows
        while (!(P4IN & BIT0 ) | !(P4IN & BIT1 ) | !( P4IN & BIT2 )
                | !( P4IN & BIT3 ))
            ;
        delayMs(5); //stall the code as a secondary debounce
        if (row != 0x0F)
            break; // if one of the input is low, some key is pressed.
    }
    P4->DIR = 0x00; // Set Columns to inputs
    if (col == 3)
        return 0;
    if (row == 0x0E)
        num = 0 + col + 1; // key in row 0
    else if (row == 0x0D)
        num = 3 + col + 1; // key in row 1
    else if (row == 0x0B)
        num = 6 + col + 1; // key in row 2
    else if (row == 0x07)
        num = 9 + col + 1; // key in row 3
    else
        num = 40;

    //If a * was pressed, save this to input[0], but return 0
    //because a number wasn't pressed
    if (num == 10)
    {
        input[0] = 10;
        return 0;
    }
    //Return 0 for a #
    else if (num == 12)
        return 0;
    //Set return 0 for a 0
    else if (num == 11){
        return 0;
    }

    printf("Value entered: %d\n", num);
    input[0] = num;
    return 1;
}

/**********************************************************************
 * Setup port 4 to receive input from a keypad.
 * All pins 0-6 are GPIO input with pullup resistors.
 *********************************************************************/
void keypad_init()
{
    P4->SEL0 &= ~0x7F;
    P4->SEL1 &= ~0x7F;
    P4->DIR &= ~0xFF;
    P4->REN |= 0xFF;
    P4->OUT |= 0xFF;
// 456 are columns
// 0123 are rows
}

/*********************************************************************
 * Initializes Systick to have maximum load, no interrupts, and to be enabled
 ********************************************************************/
void SysTick_Init()
{
    SysTick->CTRL = 0;
    SysTick->LOAD = 0x00FFFFFF; //Max load
    SysTick->VAL = 0;
    SysTick->CTRL = 0X00000005; //Use core clock, enabled
}

/*********************************************************************
 * Uses an input of mili seconds to delay the system.
 *
 * @param delay the desired delay in miliseconds
 ********************************************************************/
void SysTick_delay(uint16_t delay)
{
    SysTick->LOAD = ((delay * 3000) - 1);
    SysTick->VAL = 0;
    while ((SysTick->CTRL & 0x00010000) == 0)
        ;
}
