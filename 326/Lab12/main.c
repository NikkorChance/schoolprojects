#include "msp.h"
#include "LCD7735.h"
#include "ST7735.h"

volatile uint8_t sevenCount = 90;
volatile uint8_t buttonCount = 0;
volatile uint8_t debounce57 = 0;

/*********************************************************************
 * Lab 12:
 * Authors: Bilguun, George
 * Date: 10/18/2019
 * Instructor: Dr. Krug
 * Description:
 *
 * This program interfaces a seven segment display and a pushbutton.
 * When the button is pressed, a counter on the left side of the
 * display is incremented and a counter on the right side of the
 * display begins to increment every second. If the button is pressed
 * again, the timer on the left side of the display is incremented
 * again and the counter on the right side of the display stops
 * incrementing.
 * The counter on the right side of the display will stop incrementing
 * at 100.
 ********************************************************************/
void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer
    System_Init();


	while(1){
	    //Display the value of the time counter
	    DisplayTimerCounter();
	    //Display the value of the button counter
	    DisplayButtonCounter();

	    //If the button was pressed, debounce it
	    if(debounce57)
	        DebounceP57();
	}
}

/**
 * Splits the digits of the counter on the right side of the seven
 * segment display and displays those digits on the display.
 */
void DisplayTimerCounter()
{
    if (sevenCount <= 100)
    {
        //Save the value of sevenCount
        uint16_t temp = sevenCount;

        //Split the digits of seven count
        uint8_t dig1 = sevenCount % 10;
        sevenCount = sevenCount / 10;
        uint8_t dig2 = sevenCount % 10;
        sevenCount = sevenCount / 10;
        uint8_t dig3 = sevenCount % 10;
        sevenCount = sevenCount / 10;
        uint8_t dig4 = sevenCount % 10;
        sevenCount = sevenCount / 10;

        //Save the original value back to sevenCount
        sevenCount = temp;

        //Display the digits to the seven segemnt display
        send16BitData(0x01, dig1);
        send16BitData(0x02, dig2);
        send16BitData(0x03, dig3);
        send16BitData(0x04, dig4);
    }
}

/**
 * Splits the digits of the counter on the left side of the seven
 * segment display and displays those digits on the display.
 */
void DisplayButtonCounter()
{
    //Save the buttonCount value
    uint16_t temp = buttonCount;

    //Split up the digits of the message
    uint8_t dig1 = buttonCount % 10;
    buttonCount = buttonCount / 10;
    uint8_t dig2 = buttonCount % 10;
    buttonCount = buttonCount / 10;
    uint8_t dig3 = buttonCount % 10;
    buttonCount = buttonCount / 10;
    uint8_t dig4 = buttonCount % 10;
    buttonCount = buttonCount / 10;

    //Set the original value to buttonCount
    buttonCount = temp;

    //Display the data
    send16BitData(0x05, dig1);
    send16BitData(0x06, dig2);
    send16BitData(0x07, dig3);
    send16BitData(0x08, dig4);

}

/**
 * Calls all initialization functions required for system operation.
 */
void System_Init()
{
    System_Clock_Init(); //Setup the system clock to run at 48Mhz, SMCLK at 12Mhz, and ACLK at 124Khz
    Port1_Init();
    Port5_Init();
    Timer32_1_Init();
    SPI_Init();

    NVIC_EnableIRQ(T32_INT1_IRQn);
    NVIC_EnableIRQ(PORT5_IRQn);
    __enable_irq(); // All interrupts are enabled

    SevenSeg_Init();
}

/**
 * Enables pin 5 and 6 for SPI communication
 * Enables pin 7 as an output. This is used as the CS of SPI
 */
void Port1_Init()
{
    //spi mode
    P1->SEL0 |=  (BIT5 | BIT6);
    P1->SEL1 &=~ (BIT5 | BIT6);

    //gpio
    P1->SEL0 &=~BIT7;
    P1->SEL1 &=~BIT7;

    P1->DIR |= BIT7; //output
    P1->OUT |= BIT7; //output a zero
}

/**
 * Pin 7 is setup to take input from a button. Pin 7 is active high
 */
void Port5_Init()
{
    //GPIO
    P5->SEL0 &=~ BIT7;
    P5->SEL1 &=~ BIT7;

    //INPUT with PULLUP resistor
    P5->DIR  &=~ BIT7;
    P5->REN  |=  BIT7;
    P5->OUT  |=  BIT7;

    //INTERRUPT on falling edge
    P5->IES  &=~ BIT7;
    P5->IFG   =     0;
    P5->IE   |=  BIT7;
}

/**********************************************************************
 * Debounces the input signal from Pin 7. When the signal is fully
 * debounced, the timer is reset and toggled and the button counter
 * is incremented.
 *********************************************************************/
uint8_t DebounceP57()
{

    static uint16_t State = 0;

    State=(State<<1) | (P5IN & BIT7)>>1 | 0xf800;

    if (State == 0xfc00) //if button was pressed
    {
        buttonCount++; //Increment button counter
        TIMER32_1->CONTROL ^= 0b10000000; //Toggle timer
        TIMER32_1->LOAD = 48000000; //Reset timer 32

        debounce57= 0;
        return 1;
    }
    __delay_cycles(2);
    return 0;
}

/**
 * If the interrupt is from pin 7, a debounce flag for pin 7 is set.
 */
void  PORT5_IRQHandler()
{
    if(P5->IFG & BIT7)
    {
        debounce57= 1;
        P5->IFG &=~ BIT7; //clear the interrupt
    }
    else
        P5->IFG = 0; //clear all interrupts
}

/**
 * Sets up the EUSCI_B0 control register for SPI communication as
 * master without interrupts in reset mode using smclk on a 120
 * divider.
 */
void SPI_Init()
{
    EUSCI_B0->CTLW0 = 0x0001; //Reset mode
    EUSCI_B0 -> CTLW0 |= 0x6980;  //Uses smclk, master mode
    EUSCI_B0 -> BRW = 120; //divider
    EUSCI_B0 -> CTLW0 &= ~0x0001; // Exit reset mode
    EUSCI_B0 -> IE    &=~ 0X0003; //disable interrupts
}

/**
 * Initializes the seven segment display to be on, intensity of 3,
 * in decode mode, and all digits at zero.
 */
void SevenSeg_Init()
{
    send16BitData(0x0F, 0x00); //Display test
    send16BitData(0x0C, 0x01); // disable shutdown
    send16BitData(0x0B, 0x07);  //scan limit
    send16BitData(0x09, 0xFF); //Decode mode
    send16BitData(0x0A, 0x03);  // set intensity

    //Set every digit to zero
    send16BitData(0x01, 0x00);
    send16BitData(0x02, 0x00);
    send16BitData(0x03, 0x00);
    send16BitData(0x04, 0x00);
    send16BitData(0x05, 0x00);
    send16BitData(0x06, 0x00);
    send16BitData(0x07, 0x00);
    send16BitData(0x08, 0x00);
}

/**
 * Sends 16 bit data through EUSCI_B0.
 * @param addr An 8 bit address value
 * @param value An 8 bit data value
 */
void send16BitData(uint8_t addr, uint8_t value)
{
    P1->OUT &=~ BIT7; //CS low

    while (!(EUSCI_B0->IFG & 2)); //Wait for transmit
    EUSCI_B0->TXBUF = addr;

    SPIDelay(6);

    while (!(EUSCI_B0->IFG & 2));
    EUSCI_B0->TXBUF = value;

    while (!(EUSCI_B0->IFG & 2)); //Wait for transmit

    SPIDelay(10);

    P1->OUT |= BIT7; //CS high
}

/**
 * This delay is used to separate the address and data messages
 * sent during SPI.
 */
void SPIDelay(int n) {
    int i, j;

    for (j = 0; j < n; j++)
        for (i = 50; i > 0; i--);      /* delay 1 ms */
}


/**********************************************************************
 * Sets up Timer32_1 to interrupt every 1 second for a 48MHz clock
 *********************************************************************/
void Timer32_1_Init()
{
    TIMER32_1->CONTROL = 0b11100010; //Sets timer as disabled on startup,
                                    //wrapping
                                    //With Interrupt
                                    //No Prescaler, 32 bit mode
                                    //.  See 589 / reference manual
    TIMER32_1->CONTROL &=~ 0x80;
    TIMER32_1->LOAD = 48000000; //1 seconds
}


/**********************************************************************
 * Increments counters every one second
 *********************************************************************/
void T32_INT1_IRQHandler ( )    //Interrupt Handler for Timer 1
{
    TIMER32_1->INTCLR = 1;      //Clear interrupt flag so it does not interrupt again immediately.
    sevenCount++;

}


