#include "msp.h"
#include "ST7735.h"
#include "LCD7735.h"
#include "string.h"

/**********************************************************************
 * Calls the functions required to setup the LCD screen
 *********************************************************************/
void LCD_Init()
{
    System_Clock_Init(); //Setup system to run at 48Mhz, SMCLK at 12Mhz, ACLK to REFOCLK

    ST7735_InitB(); //Initialize the screen to be black
    Output_On(); //Enable the screen
}

/**********************************************************************
 * Changes the master clock to operate at 48 MHz and the submaster clock to
 * operate at 12 MHz.
 *
 * This is from example code supplied by TI
 *********************************************************************/
void System_Clock_Init()
{

    uint32_t currentPowerState;


    currentPowerState = PCM->CTL0 & PCM_CTL0_CPM_MASK;
    if (currentPowerState != PCM_CTL0_CPM_0)
        error();

    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    PCM->CTL0 = PCM_CTL0_KEY_VAL | PCM_CTL0_AMR_1;
    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    if (PCM->IFG & PCM_IFG_AM_INVALID_TR_IFG)
        error();

    if ((PCM->CTL0 & PCM_CTL0_CPM_MASK) != PCM_CTL0_CPM_1)
        error();

    /* Step 2: Configure Flash wait-state to 1 for both banks 0 & 1 */
    FLCTL->BANK0_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK0_RDCTL_WAIT_MASK)) |
    FLCTL_BANK0_RDCTL_WAIT_1;
    FLCTL->BANK1_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK1_RDCTL_WAIT_MASK)) |
    FLCTL_BANK1_RDCTL_WAIT_1;
    /* Step 3: Configure HFXT to use 48MHz crystal, source to MCLK & HSMCLK*/
    PJ->SEL0 |= BIT2 | BIT3;
    PJ->SEL1 &= ~(BIT2 | BIT3 );
    CS->KEY = CS_KEY_VAL;
    CS->CTL2 |= CS_CTL2_HFXT_EN | CS_CTL2_HFXTFREQ_2 | CS_CTL2_HFXTDRIVE;
    while (CS->IFG & CS_IFG_HFXTIFG)
        CS->CLRIFG |= CS_CLRIFG_CLR_HFXTIFG;

    /* Select MCLK & HSMCLK = HFXT, smclk divided to 12MHz */
    CS->CTL1 = CS->CTL1 & ~(CS_CTL1_SELM_MASK |
                CS_CTL1_DIVM_MASK |
                CS_CTL1_SELS_MASK |
                CS_CTL1_DIVHS_MASK) |
                CS_CTL1_SELM__HFXTCLK |
                CS_CTL1_SELS__HFXTCLK;

    CS->CTL1 |= 0x20000000; //clock divided sub master clock
    CS->KEY = 0; //lock CS module from unintended accesses

    CS->KEY = 0; // Lock CS module from unintended accesses

}

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1
 * bit 0 indefinitely.
 *
 * This will trigger due to a power state error. Likely, this happened
 * because the System_Clock_Init function was called more than once.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/
void error(void)
{
    volatile uint32_t i;

    while (1)
    {
        P1->OUT ^= BIT0;
        for(i = 20000; i> 0; i--);   // Blink LED forever
    }
}

/**********************************************************************
 * Prints a given string to the LCD at given x,y coordinates.
 *
 * When font is 1:
 * Max of 16 characters horizontally
 *
 * When font is 4:
 * top left      x = 0   y = 32
 * top right     x = 108 y = 32
 * bottom left   x = 0   y = 132
 * bottom right  x = 108 y = 132
 * center        x = 54  y = 82
 *
 * @param word The string to be printed
 * @param xcor The x coordinate of the top left pixel
 * @param ycor The y coordinate of the top left pixel
 * @param font The desired font for the string
 *********************************************************************/
void PrintLCD(unsigned char word[], uint8_t xcor, uint8_t ycor, uint8_t font)
{
    uint8_t index;
    uint8_t len = strlen(word);

    /* Draws every character from the given string to the LCD
     * individually. */
    for(index = 0; index <= len; index++)
    {
        ST7735_DrawCharS(xcor,ycor,word[index],ST7735_BLACK,0,font);
        xcor+=8*font;
    }
}

/**********************************************************************
 * Grabs the time and date information stored in a given flash memory location,
 * and then prints that information to the LCD.
 *
 * @param addr_loc The location where the time and date information was saved
 * @param xcor The LCD x coordinate to print the information
 * @param ycor The LCD y coordinate to print the time line, date line will be one line below
 * @param font The size of the font on the LCD
 *********************************************************************/
uint8_t LCDDisplayTimeDateMemory(uint8_t* addr_loc, uint8_t xcor, uint8_t ycor, uint8_t font)
{
    unsigned char timeToPrint[9];
    unsigned char dateToPrint[9];

    //sec
    timeToPrint[6] = (*addr_loc >> 4)+48;
    timeToPrint[7] = *addr_loc % 16 +48;
    *addr_loc++;
    timeToPrint[2] = 58; // ':'

    //min
    timeToPrint[3] = (*addr_loc >> 4)+48;
    timeToPrint[4] = *addr_loc % 16 +48;
    *addr_loc++;
    timeToPrint[5] = 58; // ':'

    //hour
    timeToPrint[0] = (*addr_loc >> 4)+48;
    timeToPrint[1] = *addr_loc % 16 +48;
    *addr_loc++;

    *addr_loc++; //day of the week is skipped

    //month
    dateToPrint[3] = (*addr_loc >> 4)+48;
    dateToPrint[4] = *addr_loc % 16 +48;
    *addr_loc++;
    dateToPrint[2] = 47; // '/'

    //day
    dateToPrint[0] = (*addr_loc >> 4)+48;
    dateToPrint[1] = *addr_loc % 16 +48;
    *addr_loc++;
    dateToPrint[5] = 47; // '/'

    //year
    dateToPrint[6] = (*addr_loc >> 4)+48;
    dateToPrint[7] = *addr_loc % 16 +48;
    *addr_loc++;
    dateToPrint[8] = 0; //Null character

    PrintLCD(timeToPrint, xcor, ycor, font);
    ycor+=8;
    PrintLCD(dateToPrint, xcor, ycor, font);

    return ycor+8;
}



