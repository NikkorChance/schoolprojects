//Skip to content
//Using Grand Valley State University Mail with screen readers
//1 of 1,549
//Fwd:
//Bilguun Baatar
//
//4:28 PM (23 minutes ago)
//
//to me
//
//---------- Forwarded message ---------
//From: Matthew Sherman <shermanm@mail.gvsu.edu>
//Date: Mon, Nov 18, 2019 at 4:27 PM
//Subject:
//To: <baatarb@mail.gvsu.edu>
//
///*
// *
// * P1.5 UCB0CLK
// * P1.6 UCB0SIMO
// * P4.6 Slave Select
// *
// */
//
//#include "msp.h"
//
//void delayMs(int n);
//
//volatile int start = 0;
//volatile int buttonCount = 0;
//volatile int sec = 0;
//
//int main(void)
//{
//    int c;
//    //Clock_Init48MHz();
//
//    TIMER_A1->CTL = 0x02D1; /* SMCLK, ID=/8, up mode, TA clear */
//    TIMER_A1->CCR[0] = 46875 - 1; /* for 1 sec */
//    TIMER_A1->EX0 = 7; /* IDEX = /8 */
//    TIMER_A1->CCTL[0] |= 0x10; /* enable TA1.0 interrupt */
//
//    NVIC_SetPriority(TA1_0_IRQn, 3); /* set priority to 3 in NVIC */
//    NVIC_EnableIRQ(TA1_0_IRQn); /* enable interrupt in NVIC */
//
//    EUSCI_B0->CTLW0 = 0x0001; /* disable UCB0 during config */
//
//    // configure EUSCI_A3->CTLW0 for:
//    // bit15      EUSCI_A_CTLW0_CKPH = 1; data shifts in on first edge, out on following edge
//    // bit14      EUSCI_A_CTLW0_CKPL = 0; clock is low when inactive
//    // bit13      EUSCI_B_CTLW0_MSB = 1; MSB first
//    // bit12      EUSCI_B_CTLW0_SEVENBIT = 0; 8-bit data
//    // bit11      EUSCI_B_CTLW0_MST = 1; master mode
//    // bits10-9   UCMODEx = 2; UCSTE active low
//    // bit8       EUSCI_B_CTLW0_SYNC = 1; synchronous mode
//    // bits7-6    UCSSELx = 2; eUSCI clock SMCLK
//    // bits5-2    reserved
//    // bit1       EUSCI_B_CTLW0_STEM = 1; UCSTE pin enables slave
//    // bit0       EUSCI_A_CTLW0_SWRST = 1; reset enabled
//    EUSCI_B0->CTLW0 = 0b1010100110000011;
//    // EUSCI_B0->MCTLW = 0;
//    EUSCI_B0->BRW = 30; /* 3 MHz / 1 = 3 MHz */
//    EUSCI_B0->CTLW0 &= ~0x0001; /* enable UCB0 after config */
//    EUSCI_B0->IE &= ~0x0003;
//
//    P5->SEL0 &= ~BIT7;                           //P1.7 SEL for regular GPIO
//    P5->SEL1 &= ~BIT7;                           //P1.7 SEL for regular GPIO
//    P5->DIR &= ~BIT7;                           //P1.7 Direction for input
//    P5->REN |= BIT7;                           //P1.7 set to Pull up
//    P5->OUT |= BIT7;            //P1.7 set the pin to high because it is pull up
//    P5->IES &= ~BIT7;         //make interrupt trigger on high-to-low transition
//    P5->IE |= BIT7;                           //enable interrupt from P1.1, P1.4
//    P5->IFG = 0;                        //clear all pending interrupt from PORT1
//
//    NVIC_SetPriority(PORT5_IRQn, 3);             //set priority to 3 in NVIC
//    NVIC_EnableIRQ(PORT5_IRQn);                  //enable interrupt in NVIC
//
//    P1->SEL0 |= BIT5 | BIT6; /* P1.5, P1.6 for UCB0CLK, UCB0SIMO */
//    P1->SEL1 &= ~(BIT5 | BIT6 );
//    //P1->DIR  |= (BIT5 | BIT6);
//
//    P4->DIR |= BIT6; /* P2.3 set as output for slave select */
//    P4->OUT |= BIT6; /* slave select idle high */
//
//    send16BitData(0x0F, 0x00);
//    send16BitData(0x0C, 0x01); // disable shutdown
//    send16BitData(0x0B, 0x07);  //scan limit
//    send16BitData(0x09, 0xFF); //Decode mode
//    send16BitData(0x0A, 0x03);  // set intensity
//
//    send16BitData(0x01, 0x00);
//    send16BitData(0x02, 0x00);
//    send16BitData(0x03, 0x00);
//    send16BitData(0x04, 0x00);
//    send16BitData(0x05, 0x00);
//    send16BitData(0x06, 0x00);
//    send16BitData(0x07, 0x00);
//    send16BitData(0x08, 0x00);
//
//    while (1)
//    {
//
//        if (buttonCount < 10)
//            send16BitData(0x05, buttonCount);
//        else if (buttonCount > 9 && buttonCount < 20)
//        {
//            send16BitData(0x06, 0x01);
//            send16BitData(0x05, buttonCount - 10);
//        }
//        else if (buttonCount > 19 && buttonCount < 30)
//        {
//            send16BitData(0x06, 0x02);
//            send16BitData(0x05, buttonCount - 20);
//        }
//
//        if (sec < 10)
//            send16BitData(0x01, sec);
//        else if (sec > 9 && sec < 20)
//        {
//            send16BitData(0x01, sec - 10);
//            send16BitData(0x02, 0x01);
//        }
//        else if (sec > 19 && sec < 30)
//        {
//            send16BitData(0x01, sec - 20);
//            send16BitData(0x02, 0x02);
//        }
//        else if (sec > 29 && sec < 40)
//        {
//            send16BitData(0x01, sec - 30);
//            send16BitData(0x02, 0x03);
//        }
//        else if (sec > 39 && sec < 50)
//        {
//            send16BitData(0x01, sec - 40);
//            send16BitData(0x02, 0x04);
//        }
//        else if (sec > 49 && sec < 60)
//        {
//            send16BitData(0x01, sec - 50);
//            send16BitData(0x02, 0x05);
//        }
//        else if (sec > 59 && sec < 70)
//        {
//            send16BitData(0x01, sec - 60);
//            send16BitData(0x02, 0x06);
//        }
//        else if (sec > 69 && sec < 80)
//        {
//            send16BitData(0x01, sec - 70);
//            send16BitData(0x02, 0x07);
//        }
//        else if (sec > 79 && sec < 90)
//        {
//            send16BitData(0x01, sec - 80);
//            send16BitData(0x02, 0x08);
//        }
//        else if (sec > 89 && sec < 100)
//        {
//            send16BitData(0x01, sec - 90);
//            send16BitData(0x02, 0x09);
//        }
//        else if (sec > 100)
//        {
//            send16BitData(0x01, 0x00);
//            send16BitData(0x02, 0x00);
//            send16BitData(0x03, 0x00);
//            send16BitData(0x04, 0x00);
//            sec = 0;
//        }
//        /*send16BitData(0x01, 0x01);
//         send16BitData(0x02, 0x02);
//         send16BitData(0x03, 0x03);
//         send16BitData(0x04, 0x04);
//         send16BitData(0x05, 0x05);
//         send16BitData(0x06, 0x06);
//         send16BitData(0x07, 0x07);
//         send16BitData(0x08, 0x08);
//
//
//         send16BitData(0x01, 0x08);
//         send16BitData(0x02, 0x07);
//         send16BitData(0x03, 0x06);
//         send16BitData(0x04, 0x05);
//         send16BitData(0x05, 0x04);
//         send16BitData(0x06, 0x03);
//         send16BitData(0x07, 0x02);
//         send16BitData(0x08, 0x01);*/
//    }
//}
//
//void PORT5_IRQHandler(void)
//{                    //Set Alarm . White Button
//    if (P5->IFG & BIT7)
//    {                        //If the Flag is high
//        delayMs(10);
//        if (P5->IN & BIT7)
//        {                 //IF the Flag is high
//            if (start)
//                start = 0;
//            else
//                start = 1;
//
//            buttonCount++;
//        }
//    }
//    P5->IFG &= ~BIT7;                   //clear the interrupt flag before return
//}
//
//void Clock_Init48MHz(void)
//{
//    // Configure Flash wait-state to 1 for both banks 0 & 1
//    FLCTL->BANK0_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK0_RDCTL_WAIT_MASK))
//            | FLCTL_BANK0_RDCTL_WAIT_1;
//    FLCTL->BANK1_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK0_RDCTL_WAIT_MASK))
//            | FLCTL_BANK1_RDCTL_WAIT_1;
//    PJ->SEL0 |= (BIT2 | BIT3 ); //configure PJ.2/3 HFTX FUNCTION
//    PJ->SEL1 &= ~(BIT2 | BIT3 );
//    CS->KEY = CS_KEY_VAL; //unlock CS module for register access
//    CS->CTL2 |= CS_CTL2_HFXT_EN | CS_CTL2_HFXTFREQ_6 | CS_CTL2_HFXTDRIVE;
//    while (CS->IFG & CS_IFG_HFXTIFG)
//        CS->CLRIFG |= CS_CLRIFG_CLR_HFXTIFG;
//    //select MCLK & HSMCLK = HFXT, no divider
//    CS->CTL1 = CS->CTL1 & ~(CS_CTL1_SELM_MASK |
//    CS_CTL1_DIVM_MASK |
//    CS_CTL1_SELS_MASK |
//    CS_CTL1_DIVHS_MASK) |
//    CS_CTL1_SELM__HFXTCLK |
//    CS_CTL1_SELS__HFXTCLK;
//    CS->CTL1 |= 0x20000000; //clock divided sub master clock
//    CS->KEY = 0; //lock CS module from unintended accesses
//}
//
//void send16BitData(uint8_t addr, uint8_t value)
//{
//
//    delayMs(10);
//    P4->OUT &= ~BIT6;
//    delayMs(10);
//    while ((EUSCI_B0->IFG & 0x0002) == 0x0000)
//    {
//    };
//    EUSCI_B0->TXBUF = addr;
//    while (EUSCI_B0->STATW & 1)
//        ;
//    while ((EUSCI_B0->IFG & 0x0001) == 0x0000)
//    {
//    };
//    delayMs(10);
//
//    while ((EUSCI_B0->IFG & 0x0002) == 0x0000)
//    {
//    };
//    EUSCI_B0->TXBUF = value;
//    while (EUSCI_B0->STATW & 1)
//        ;
//    while ((EUSCI_B0->IFG & 0x0001) == 0x0000)
//    {
//    };
//    delayMs(10);
//
//    P4->OUT |= BIT6;  // CS HIGH
//    delayMs(10);
//}
//
//void TA1_0_IRQHandler(void)
//{
//    TIMER_A1->CCTL[0] &= ~1; /* clear interrupt flag */
//    if (start)
//        sec++;
//}
//
///* system clock at 3 MHz */
//void delayMs(int n)
//{
//    int i, j;
//
//    for (j = 0; j < n; j++)
//        for (i = 750; i > 0; i--)
//            ; /* delay 1 ms */
//}
//
