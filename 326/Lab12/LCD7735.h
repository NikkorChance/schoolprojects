#ifndef LCD7735
#define LCD7735

/**********************************************************************
 * Calls the functions required to setup the LCD screen
 *********************************************************************/
void LCD_Init();

/**********************************************************************
 * Changes the master clock to operate at 48 MHz and the submaster clock to
 * operate at 12 MHz.
 *
 * This is from example code supplied by TI
 *********************************************************************/
void System_Clock_Init();

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1
 * bit 0 indefinitely.
 *
 * This will trigger due to a power state error. Likely, this happened
 * because the System_Clock_Init function was called more than once.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/

void error(void);
/**********************************************************************
 * Prints a given string to the LCD at given x,y coordinates.
 *
 * When font is 1:
 * Max of 16 characters horizontally
 *
 * When font is 4:
 * top left      x = 0   y = 32
 * top right     x = 108 y = 32
 * bottom left   x = 0   y = 132
 * bottom right  x = 108 y = 132
 * center        x = 54  y = 82
 *
 * @param word The string to be printed
 * @param xcor The x coordinate of the top left pixel
 * @param ycor The y coordinate of the top left pixel
 * @param font The desired font for the string
 *********************************************************************/
void PrintLCD(unsigned char word[], uint8_t xcor, uint8_t ycor, uint8_t font);

/**********************************************************************
 * Grabs the time and date information stored in a given flash memory location,
 * and then prints that information to the LCD.
 *
 * @param addr_loc The location where the time and date information was saved
 * @param xcor The LCD x coordinate to print the information
 * @param ycor The LCD y coordinate to print the time line, date line will be one line below
 * @param font The size of the font on the LCD
 *********************************************************************/

uint8_t LCDDisplayTimeDateMemory(uint8_t* addr_loc, uint8_t xcor, uint8_t ycor, uint8_t font);


#endif
