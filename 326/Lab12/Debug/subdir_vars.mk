################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../LCD7735.c \
../ST7735.c \
../main.c \
../startup_msp432p401r_ccs.c \
../system_msp432p401r.c \
../tester.c 

C_DEPS += \
./LCD7735.d \
./ST7735.d \
./main.d \
./startup_msp432p401r_ccs.d \
./system_msp432p401r.d \
./tester.d 

OBJS += \
./LCD7735.obj \
./ST7735.obj \
./main.obj \
./startup_msp432p401r_ccs.obj \
./system_msp432p401r.obj \
./tester.obj 

OBJS__QUOTED += \
"LCD7735.obj" \
"ST7735.obj" \
"main.obj" \
"startup_msp432p401r_ccs.obj" \
"system_msp432p401r.obj" \
"tester.obj" 

C_DEPS__QUOTED += \
"LCD7735.d" \
"ST7735.d" \
"main.d" \
"startup_msp432p401r_ccs.d" \
"system_msp432p401r.d" \
"tester.d" 

C_SRCS__QUOTED += \
"../LCD7735.c" \
"../ST7735.c" \
"../main.c" \
"../startup_msp432p401r_ccs.c" \
"../system_msp432p401r.c" \
"../tester.c" 


