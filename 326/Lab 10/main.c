#include "msp.h"

enum Bistate
{
    Step1, Step2, Step3, Step4
};
enum direction
{
    CCW, CW
};

#define BILINEA  BIT3
#define BILINEB  BIT6
#define BILINEAP BIT5
#define BILINEBP BIT7

#define BITURNDELAY 150

volatile uint8_t direction = CCW;
volatile uint8_t NextWire = Step1;
volatile uint8_t button6Flag = 0;
volatile uint8_t button7Flag = 0;

/**
 * main.c
 */
void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer
    System_Init();

    while(1)
    {
        if(button6Flag)
            if(Button6Debounce())
            {
                BiMotor_Counterclockwise(4);
            }
        if(button7Flag)
            if(Button7Debounce()){
                BiMotor_Clockwise(4);
            }
    }

}

/**********************************************************************
 * Performs all functions calls and register adjustments to initialize
 * the program.
 *********************************************************************/
void System_Init()
{
    __enable_irq();
    NVIC_EnableIRQ(PORT1_IRQn);
    SysTick_Init();
    Port2_Init(); //Bipolar Motor
    Port1_Init(); //Buttons
}

/**********************************************************************
 * Accepts the desired number of steps for the motor to turn in the
 * clockwise direction, and runs the motor through that many steps.
 *
 *
 * @param steps The number of steps to rotate the motor
 *********************************************************************/
void BiMotor_Clockwise(uint8_t steps)
{
    steps++;  //Increment steps by 1 to accommodate the NextWire change

    NextWire++; //Change the next wire to be the current wire

    //If NextWire was pushed passed LineBp, NextWire should be LineA
    if (NextWire > Step4)
        NextWire = Step1;

    //Turn off all wires
    P2->OUT &= ~0xF0;

    //Run the motor through every step
    while (steps > 0)
    {
        RunBiMotorClockWise();
        steps--;
    }
}

/**********************************************************************
 * Accepts the desired number of steps for the motor to turn in the
 * clockwise direction, and runs the motor through that many steps.
 *
 *
 * @param steps The number of steps to rotate the motor
 *********************************************************************/
void BiMotor_Counterclockwise(uint8_t steps)
{
    steps++; //Increment steps by 1 to accommodate the NextWire change

    NextWire--; //Change the next wire to be the current wire

    if (NextWire == 255)
        NextWire = Step1; //Subtracting from LineA will cause roll over, this corrects that

    //Turn off all wires
    P2->OUT &= ~0xF0;

    //Run the motor through every step
    while (steps > 0)
    {
        RunBiMotorCounterClockWise();
        steps--;
    }
}

/**********************************************************************
 * Will run one step of the motor based on the current state of the motor
 *********************************************************************/
void RunBiMotorCounterClockWise()
{
    switch (NextWire)
    {
    case (Step1):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEAP | BILINEBP;
        P2->OUT |= BILINEA | BILINEB;
        NextWire = Step2;
        break;
    case (Step2):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEBP;
        P2->OUT |= BILINEB | BILINEAP;
        NextWire = Step3;
        break;
    case (Step3):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEB;
        P2->OUT |= BILINEAP | BILINEBP;
        NextWire = Step4;
        break;
    case (Step4):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEB | BILINEAP;
        P2->OUT |= BILINEA | BILINEBP;
        NextWire = Step1;
        break;
    }
    SysTick_3MHz_delay(BITURNDELAY);
}

/**********************************************************************
 * Will run one step of the motor based on the current state of the motor
 *********************************************************************/
void RunBiMotorClockWise()
{
    switch (NextWire)
    {
    case (Step1):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEAP | BILINEBP;
        P2->OUT |= BILINEA | BILINEB;
        NextWire = Step4;
        break;
    case (Step2):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEBP;
        P2->OUT |= BILINEB | BILINEAP;
        NextWire = Step1;
        break;
    case (Step3):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEB;
        P2->OUT |= BILINEAP | BILINEBP;
        NextWire = Step2;
        break;
    case (Step4):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEB | BILINEAP;
        P2->OUT |= BILINEA | BILINEBP;
        NextWire = Step3;
        break;
    }
    SysTick_3MHz_delay(BITURNDELAY);
}

/**********************************************************************
 * Setup a bit 6 port 1 as an input.
 *********************************************************************/
void Port1_Init()
{
    //GPIO on bit 6,7
    P1->SEL0 &= ~BIT6 | BIT7;
    P1->SEL1 &= ~BIT6 | BIT7;
    //Input on bit 6,7
    P1->DIR &= ~BIT6 | BIT7;
    //Pull up/down resistor on bit 6,7
    P1->REN |= BIT6 | BIT7;
    //Pull up on bit 6,7
    P1->OUT |= BIT6 | BIT7;

    //Interrupt on a falling edge
    P1->IES &= ~BIT6 | BIT7;
    //All interrupt flags are disabled on port 1
    P1->IFG = 0;
    //Enable the interrupt on bit 6,7
    P1->IE |= BIT6 | BIT7;
}

/**********************************************************************
 * If the button on port 6 is pressed, set a flag so that the button is debonced.
 * If the button on port 7 is pressed, set a flag so that the button is debonced.
 *
 *********************************************************************/
void PORT1_IRQHandler()
{
    if (P1->IFG & BIT6)
    {
        button6Flag = 1;
        button7Flag = 0;
        P1->IFG &= ~BIT6;
    }
    else if (P1->IFG & BIT7)
    {
        button7Flag = 1;
        button6Flag = 0;
        P1->IFG &= ~BIT7;
    }
    else
        P1->IFG = 0; //Disable all interrupt flags if none were handled.
}

/**********************************************************************
 * Setup port 2 to run a motor
 *
 * P2.1 is LineA
 * P2.2 is LineA'
 * P2.2 is LineB
 * P2.3 is LineB'
 *********************************************************************/
void Port2_Init()
{
    //GPIO on bits 4,5,6,7
    P2->SEL0 &= ~0xF8;
    P2->SEL1 &= ~0xF8;
    //Output on BIT4,5,6,7
    P2->DIR |= 0xF8;
    //All outputs are zero
    P2->OUT = 0;
}

/**********************************************************************
 * Check if the button was pressed. If it was, set the direction
 *********************************************************************/
uint8_t Button6Debounce()
{
    static uint16_t button = 0; //Current debounce status
    button = (button << 1) | (P1->IN & BIT6 ) | 0xe000;

    if (button == 0xf000)
    {
        button6Flag = 0;
        direction = CCW;
        return 1;
    }

    return 0;
}

/**********************************************************************
 * Check if the button was pressed. If it was, set the direction
 *********************************************************************/
uint8_t Button7Debounce()
{
    static uint16_t button = 0; //Current debounce status
    button = (button << 1) | (P1->IN & BIT7 ) | 0xe000;

    if (button == 0xf000)
    {
        button7Flag = 0;
        direction = CW;
        return 1;
    }

    return 0;
}

/**********************************************************************
 * Initializes Systick to have maximum load, no interrupts, and to be enabled
 **********************************************************************/
void SysTick_Init()
{
    SysTick->CTRL = 0;
    SysTick->LOAD = 0x00FFFFFF; //Max load
    SysTick->VAL = 0;
    SysTick->CTRL = 0X00000005; //Use core clock, enabled
}

/**********************************************************************
 * Uses an input of mili seconds to delay the system.
 **********************************************************************/
void SysTick_3MHz_delay(uint16_t delay)
{
    SysTick->LOAD = ((delay * 3000) - 1);
    SysTick->VAL = 0;
    while ((SysTick->CTRL & 0x00010000) == 0)
        ;
}
