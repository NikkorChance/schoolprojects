#include "msp.h"

void Port2_Init();
uint8_t DebounceP45();
uint8_t DebounceP46();

enum state{
    off,
    red,
    green,
    blue
};


/**
 * main.c
 */
void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    uint8_t state = off;
    Port2_Init();


    while(1){

        uint8_t R_G_B = DebounceP45();
        uint8_t B_G_R = DebounceP46();
        while(DebounceP45() || DebounceP46()){
            if(DebounceP45())
                if(DebounceP46()) B_G_R = 1;
            if(DebounceP46())
                if(DebounceP45()) R_G_B = 1;
        }


        if(R_G_B && B_G_R)
        {
          state = off;

        }
        else if(R_G_B)
        {
            //Rotate to R -> G -> B
            if(state == off){
                state = red;
            }
            else if(state == red){
                state = green;
            }
            else if(state == green){
                state = blue;
            }
            else if(state == blue){
                state = red;
            }
        }
        else if(B_G_R)
        {
            //Rotate B -> G -> R
            if(state == off){
                state = blue;
            }
            else if(state == blue){
                state = green;
            }
            else if(state == green){
                state = red;
            }
            else if(state == red){
                state = blue;
            }
        }
        StateMachine(state);

    }
}

/**********************************************************************
 *Changes the current state of the LEDs based on the nextState variable
 *********************************************************************/
void StateMachine(uint8_t nextState)
{
    switch (nextState)
    {
    case off:
        P4->OUT = 0x00; //0000
        break;
    case red:
        P4->OUT = 0x02; //0010
        break;
    case green:
        P4->OUT = 0x04; //0100
        break;
    case blue:
        P4->OUT = 0x08; //1000
        break;
    default:
        printf("Error, default case reached");
        P4->OUT ^= BIT1; //toggles the current
        break;
    }
}

/**********************************************************************
 * Switch debouncer from ganssle.com/debouncing-pt2.htm
 *
 * The bit reader is hard coded into this function to save space.
 *********************************************************************/
uint8_t DebounceP45() {
    uint8_t state = 0;
    int i = 0;
    for(i=0;i<5000;i++){
        if((P4-> IN & BIT5)==0x20)
            return state; //If the button
    }
    state = 1;
    return state;

}


/**********************************************************************
 * Switch debouncer from ganssle.com/debouncing-pt2.htm
 *
 * The bit reader is hard coded into this function to save space.
 *********************************************************************/
uint8_t DebounceP46() {
    uint8_t state = 0;
       int i = 0;
       for(i=0;i<5000;i++){
           if((P4-> IN & BIT6)==0x40)
               return state; //If the button
       }
       state = 1;
       return state;
}


/**********************************************************************
 * P2.1,2,3 are set to GPIO outputs, and P2.5,6 is set to a GPIO input
 *********************************************************************/
void Port2_Init() {
    P4->SEL0 &= ~0xff; //GPIO
    P4->SEL1 &= ~0xff; //GPIO
    P4->DIR  |=  0x0e; //p4.1,2,3 are outputs and p2.5,6 are inputs
    P4->REN  |=  0X08; //Pull down on p2.1,2,3
    P4->OUT  |=  0x00; //Pull down and set outputs to 0
}

