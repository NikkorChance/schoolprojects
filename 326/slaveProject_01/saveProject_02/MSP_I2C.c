#include "MSP_I2C.h"
#include "MSP.h"

#define SLAVE_ADDRESS 0x48
volatile char OneTen;
volatile char Deci;

/*********************************************************************
 * When a signal is received from the master, toggle the LEDs on port 2
 * based on the 8-bit message received.
 ********************************************************************/
void EUSCIB1_IRQHandler(void)
{
    uint32_t status = EUSCI_B1->IFG; // Get EUSCI_B0 interrupt flag
    EUSCI_B1->IFG &= ~ EUSCI_B_IFG_RXIFG0; // Clear EUSCI_B0 RX interrupt flag
    if (status & EUSCI_B_IFG_RXIFG0)
    {// Check if receive interrupt occurs
        Deci = EUSCI_B1->RXBUF; // Load current RXData value to transmit buffer
        while(!(EUSCI_B1 -> IFG & 1));
        OneTen = EUSCI_B1->RXBUF; // Load current RXData value to transmit buffer

    } // Toggle P2.2 if 'B' is received
}

/**********************************************************************
 * Set up the slave device to trigger the receive interrupt when data is
 * received from the master.
 *********************************************************************/
void I2C_Init()
{
    P6->SEL0 |= BIT5 | BIT4;

    EUSCI_B1->CTLW0 |= EUSCI_B_CTLW0_SWRST; // Hold EUSCI_B1 module in reset state
    EUSCI_B1->CTLW0 |= EUSCI_B_CTLW0_MODE_3 | EUSCI_B_CTLW0_SYNC;
    EUSCI_B1->I2COA0 = SLAVE_ADDRESS | EUSCI_B_I2COA0_OAEN;
    EUSCI_B1->CTLW0 &= ~EUSCI_B_CTLW0_SWRST; // Clear SWRST to resume operation
    EUSCI_B1->IFG  &= ~EUSCI_B_IFG_RXIFG0; // Clear EUSCI_B1 RX interrupt flag
    EUSCI_B1->IE |= EUSCI_B_IE_RXIE0; // Enable EUSCI_B1 RX interrupt
}

/**
 *
 */
char GetIntSpeed()
{
    return OneTen;
}

/**
 *
 */
char GetDeciSpeed()
{
    return Deci;
}
