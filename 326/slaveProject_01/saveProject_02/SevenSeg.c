#include "SevenSeg.h"
#include "Clocks.h"
#include "msp.h"

/**
 * Sets up the EUSCI_B0 control register for SPI communication as
 * master without interrupts in reset mode using smclk on a 120
 * divider.
 */
void SPI_Init()
{
    EUSCI_B0->CTLW0 = 0x0001; //Reset mode
    EUSCI_B0 -> CTLW0 |= 0x6980;  //Uses smclk, master mode
    EUSCI_B0 -> BRW = 30; //divider
    EUSCI_B0 -> CTLW0 &= ~0x0001; // Exit reset mode
    EUSCI_B0 -> IE    &=~ 0X0003; //disable interrupts
}

/**
 * Initializes the seven segment display to be on, intensity of 3,
 * in decode mode, and all digits at zero.
 */
void SevenSeg_Init()
{
    send16BitData(0x0F, 0x00); //Display test
    send16BitData(0x0C, 0x01); // disable shutdown
    send16BitData(0x0B, 0x07);  //scan limit
    send16BitData(0x09, 0xFF); //Decode mode
    send16BitData(0x0A, 0x03);  // set intensity

    //Set every digit to zero
    send16BitData(0x01, 0x00);
    send16BitData(0x02, 0x00);
    send16BitData(0x03, 0x00);
    send16BitData(0x04, 0x00);
    send16BitData(0x05, 0x00);
    send16BitData(0x06, 0x00);
    send16BitData(0x07, 0x00);
    send16BitData(0x08, 0x00);
}

/**
 * Splits the digits of the counter on the right side of the seven
 * segment display and displays those digits on the display.
 */
void DisplaySpeed(double fspeed)
{
    //Get the first decimal value into integer space
    uint16_t speed = fspeed * 10;


    //Split the digits of seven count
    uint8_t dig1 = speed % 10; //decimal
    speed = speed / 10;
    uint8_t dig2 = speed % 10; //ones
    speed = speed / 10;
    dig2 |= BIT7; //Enable the decimal point
    uint8_t dig3 = speed % 10; //tens
    speed = speed / 10;
    uint8_t dig4 = speed % 10; //hundreds
    speed = speed / 10;

    //Display the digits to the seven segemnt display
    send16BitData(0x01, dig1);
    send16BitData(0x02, dig2);
    send16BitData(0x03, dig3);
    send16BitData(0x04, dig4);
}


/**
 * Sends 16 bit data through EUSCI_B0.
 * @param addr An 8 bit address value
 * @param value An 8 bit data value
 */
void send16BitData(uint8_t addr, uint8_t value)
{
    P1->OUT &=~ BIT7; //CS low

    while (!(EUSCI_B0->IFG & 2)); //Wait for transmit
    EUSCI_B0->TXBUF = addr;

    SPIDelay(6);

    while (!(EUSCI_B0->IFG & 2));
    EUSCI_B0->TXBUF = value;

    while (!(EUSCI_B0->IFG & 2)); //Wait for transmit

    SPIDelay(10);

    P1->OUT |= BIT7; //CS high
}
