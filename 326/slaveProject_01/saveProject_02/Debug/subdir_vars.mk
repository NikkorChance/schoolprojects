################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../Clocks.c \
../MSP_I2C.c \
../PhotoDiode.c \
../SevenSeg.c \
../main.c \
../motor127.c \
../proxSense.c \
../startup_msp432p401r_ccs.c \
../system_msp432p401r.c 

C_DEPS += \
./Clocks.d \
./MSP_I2C.d \
./PhotoDiode.d \
./SevenSeg.d \
./main.d \
./motor127.d \
./proxSense.d \
./startup_msp432p401r_ccs.d \
./system_msp432p401r.d 

OBJS += \
./Clocks.obj \
./MSP_I2C.obj \
./PhotoDiode.obj \
./SevenSeg.obj \
./main.obj \
./motor127.obj \
./proxSense.obj \
./startup_msp432p401r_ccs.obj \
./system_msp432p401r.obj 

OBJS__QUOTED += \
"Clocks.obj" \
"MSP_I2C.obj" \
"PhotoDiode.obj" \
"SevenSeg.obj" \
"main.obj" \
"motor127.obj" \
"proxSense.obj" \
"startup_msp432p401r_ccs.obj" \
"system_msp432p401r.obj" 

C_DEPS__QUOTED += \
"Clocks.d" \
"MSP_I2C.d" \
"PhotoDiode.d" \
"SevenSeg.d" \
"main.d" \
"motor127.d" \
"proxSense.d" \
"startup_msp432p401r_ccs.d" \
"system_msp432p401r.d" 

C_SRCS__QUOTED += \
"../Clocks.c" \
"../MSP_I2C.c" \
"../PhotoDiode.c" \
"../SevenSeg.c" \
"../main.c" \
"../motor127.c" \
"../proxSense.c" \
"../startup_msp432p401r_ccs.c" \
"../system_msp432p401r.c" 


