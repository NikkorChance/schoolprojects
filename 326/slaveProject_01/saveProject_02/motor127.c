#include "motor127.h"
#include "msp.h"

enum Bistate
{
    Step1, Step2, Step3, Step4
};
enum direction
{
    CCW, CW
};

#define BILINEA  BIT3
#define BILINEB  BIT6
#define BILINEAP BIT5
#define BILINEBP BIT7

//This was set expecting the 3Mhz clock, but this should
//really be done with an interrupt instead of a delay
#define BITURNDELAY 30000

volatile uint8_t direction = CCW;
volatile uint8_t NextWire = Step1;
volatile uint8_t button6Flag = 0;
volatile uint8_t button7Flag = 0;
volatile float motorDeg;

/**********************************************************************
 * Runs the bipolar motor so through 610 steps to ensure that the
 * motor is in the zero degree position.
 *********************************************************************/
void Motor_Position_Init()
{
    int step = 0;

    //Run the motor through 610 steps.
    BiMotor_Counterclockwise(40);

    motorDeg = 0; //Set the motor position degree to zero
}

/**
 * Will turn the motor to point in the direction of the speed it was
 * given.
 * 90 mph at 270 degrees.
 *
 * Returns whether it has matched the speed it was given
 */
uint8_t MotorSpeedControl(double speed)
{
    const uint8_t numsteps = 8;
    if((motorDeg / 1.5) >= speed + 1 )
    {
        //Run it two steps at a time
        BiMotor_Clockwise(numsteps);
        motorDeg -= .525 * 8 ;
        return 0;
    }
    else if((motorDeg / 1.5) < speed - 1)
    {
        //Run it two steps at a time
        BiMotor_Counterclockwise(numsteps);
        motorDeg += .525 * 8;
        return 0;
    }
    else
        return 1;
}

/**********************************************************************
 * Accepts the desired number of steps for the motor to turn in the
 * counter clockwise direction, and runs the motor through that many
 * steps.
 *
 *
 * @param steps The number of steps to rotate the motor
 *********************************************************************/
void BiMotor_Counterclockwise(uint8_t steps)
{
    steps++;  //Increment steps by 1 to accommodate the NextWire change

    NextWire++; //Change the next wire to be the current wire

    //If NextWire was pushed passed LineBp, NextWire should be LineA
    if (NextWire > Step4)
        NextWire = Step1;

    //Turn off all wires
    P2->OUT &= ~0xF0;

    //Run the motor through every step
    while (steps > 0)
    {
        RunBiMotorClockWise();
        steps--;
    }
}

/**********************************************************************
 * Accepts the desired number of steps for the motor to turn in the
 * clockwise direction, and runs the motor through that many steps.
 *
 *
 * @param steps The number of steps to rotate the motor
 *********************************************************************/
void BiMotor_Clockwise(uint8_t steps)
{
    steps++; //Increment steps by 1 to accommodate the NextWire change

    NextWire--; //Change the next wire to be the current wire

    if (NextWire == 255)
        NextWire = Step1; //Subtracting from LineA will cause roll over, this corrects that

    //Turn off all wires
    P2->OUT &= ~0xF0;

    //Run the motor through every step
    while (steps > 0)
    {
        RunBiMotorCounterClockWise();
        steps--;
    }
}

/**********************************************************************
 * Will run one step of the motor based on the current state of the motor
 *********************************************************************/
void RunBiMotorCounterClockWise()
{
    switch (NextWire)
    {
    case (Step1):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEAP | BILINEBP;
        P2->OUT |= BILINEA | BILINEB;
        NextWire = Step2;
        break;
    case (Step2):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEBP;
        P2->OUT |= BILINEB | BILINEAP;
        NextWire = Step3;
        break;
    case (Step3):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEB;
        P2->OUT |= BILINEAP | BILINEBP;
        NextWire = Step4;
        break;
    case (Step4):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEB | BILINEAP;
        P2->OUT |= BILINEA | BILINEBP;
        NextWire = Step1;
        break;
    }
    __delay_cycles(BITURNDELAY);
}

/**********************************************************************
 * Will run one step of the motor based on the current state of the motor
 *********************************************************************/
void RunBiMotorClockWise()
{
    switch (NextWire)
    {
    case (Step1):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEAP | BILINEBP;
        P2->OUT |= BILINEA | BILINEB;
        NextWire = Step4;
        break;
    case (Step2):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEBP;
        P2->OUT |= BILINEB | BILINEAP;
        NextWire = Step1;
        break;
    case (Step3):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEB;
        P2->OUT |= BILINEAP | BILINEBP;
        NextWire = Step2;
        break;
    case (Step4):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEB | BILINEAP;
        P2->OUT |= BILINEA | BILINEBP;
        NextWire = Step3;
        break;
    }
    __delay_cycles(BITURNDELAY);
}


