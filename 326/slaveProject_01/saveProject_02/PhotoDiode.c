#include "PhotoDiode.h"
#include "msp.h"

void ADC14_init(void)
{
    P5SEL0 |= 0X20;                     // configure pin 5.5 for A0 input
    P5SEL1 |= 0X20;
    ADC14->CTL0 &= ~0x00000002;         // disable ADC14ENC during configuration
    ADC14->CTL0 |= 0x04400110;        // S/H pulse mode, SMCLK, 16 sample clocks
    ADC14->CTL1 = 0x00000030;          // 14 bit resolution
    ADC14->CTL1 |= 0x00000000;        // Selecting ADC14CSTARTADDx mem0 REGISTER
    ADC14->MCTL[0] = 0x00000000;            // ADC14INCHx = 0 for mem[0]
// ADC14->MCTL[0] = ADC14_MCTLN_INCH_0;
    ADC14->CTL0 |= 0x00000002; // enable ADC14ENC, starts the ADC after configuration
}

uint16_t Potentiometer_Position()
{
    static volatile uint16_t result;
    float nADC;

    ADC14->CTL0 |= 1;              //start conversation
    while (!ADC14->IFGR0)
        ;          //wait for conversation to complete
    result = ADC14->MEM[0];            // get the value from the ADC
    return result;
//    nADC = (result * 3.3) / 16384; // Calculate the voltage received
}

/**
 * Controls the duty cycle of the LCD LED screen
 */
void SetLCDLED()
{
    double photocoll = Potentiometer_Position()+500;
    if(photocoll > 10000) photocoll = 14000; //if over 10,000 set it to max
    photocoll = photocoll / 14000;

    TIMER_A1->CCR[2]=photocoll*1000;
}
