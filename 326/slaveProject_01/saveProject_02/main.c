#include "msp.h"
#include "motor127.h"
#include "Clocks.h"
#include "SevenSeg.h"
#include "proxSense.h"
#include "MSP_I2C.h"

/**
 * Slave program for the 326 Term Project
 */
void main(void)
{
    System_Init();


    while(1){ //change the warning pin
        UpdateWarningLine();

       int intspee = (int)GetIntSpeed();
       int decispee = (int)GetDeciSpeed();
       double speed = intspee*10;
       speed = speed + decispee;

       speed = speed / 10;
       DisplaySpeed(speed);
       MotorSpeedControl(speed);
       SetLCDLED();
    }
}

/**********************************************************************
 * Enables pin 5 and 6 for SPI communication
 * Enables pin 7 as an output. This is used as the CS of SPI
 *********************************************************************/
void Port1_Init()
{
    //spi mode
    P1->SEL0 |=  (BIT5 | BIT6);
    P1->SEL1 &=~ (BIT5 | BIT6);

    //gpio
    P1->SEL0 &=~BIT7;
    P1->SEL1 &=~BIT7;

    P1->DIR |= BIT7; //output
    P1->OUT |= BIT7; //output a zero
}

/**********************************************************************
 * Setup port 2 to run a motor
 *
 * P2.1 is LineA
 * P2.2 is LineA'
 * P2.2 is LineB
 * P2.3 is LineB'
 *********************************************************************/
void Port2_Init()
{
    //GPIO on bits 4,5,6,7
    P2->SEL0 &= ~0xF8;
    P2->SEL1 &= ~0xF8;
    //Output on BIT4,5,6,7
    P2->DIR |= 0xF8;
    //All outputs are zero
    P2->OUT = 0;
}

/**
 * BIT2 GPIO output: Proximity warning light
 */
void Port4_Init()
{
    P4->SEL0 &= ~BIT2|BIT1;
    P4->SEL1 &= ~BIT2|BIT1;
    P4->DIR  |=  BIT2|BIT1;
    //All outputs are zero
    P4->OUT = BIT2|BIT1;
}

/*********************************************************************
 * BIT1 warning line
 * Sets up Bit 4 to be echo.
 * Sets up BIT 7 to be trig.
 * Sets up TimerA for Port 2 and both pin 6 and 7.
 ********************************************************************/
void Port6_Init()
{
    int MAXPERIOD = 48000;

    //GPIO output starting at 0
    P6->SEL0 &= ~BIT1;
    P6->SEL1 &= ~BIT1;
    P6->DIR  |=  BIT1;
    P6->OUT  &= ~BIT1;

    P6->DIR |= BIT6;
    P6->DIR &= ~ BIT7;
    P6->SEL0 |= BIT6 | BIT7;
    P6->SEL1 &= ~(BIT6 | BIT7);

    TIMER_A2->CCR[0] = MAXPERIOD; //Set period
    TIMER_A2->CCTL[3] = TIMER_A_CCTLN_OUTMOD_7; //Set reset
    TIMER_A2->CCR[3] = 8;  //60 ms
    //SMCLK, up mode, clear timer, use divider of 4
    TIMER_A2->CTL = TIMER_A_CTL_TASSEL_2 | TIMER_A_CTL_MC__UP | TIMER_A_CTL_CLR
            | TIMER_A_CTL_ID_2;
    TIMER_A2->CCTL[4] = TIMER_A_CCTLN_CM_3 | TIMER_A_CCTLN_CCIS_0
            | TIMER_A_CCTLN_CCIE | TIMER_A_CCTLN_CAP | TIMER_A_CCTLN_SCS;
    TIMER_A2->CCTL[4] &= ~(TIMER_A_CCTLN_CCIFG);
}

/**
 * BIT6 pwm output - LCD screen LED
 */
void Port7_Init()
{
    P7->DIR |= BIT6|BIT7;
    P7->SEL0 |= BIT6|BIT7;
    P7->SEL1 &= ~( BIT6|BIT7 );

    TIMER_A1->CCR[0] = 1000; //Set period
//    TIMER_A1->CCTL[1] = TIMER_A_CCTLN_OUTMOD_7; //reset/set
//    TIMER_A1->CCR[1] = 750; //Set duty cycle
    TIMER_A1->CCTL[2] = TIMER_A_CCTLN_OUTMOD_7;
    TIMER_A1->CCR[2] = 1000; //fully on
    //SMCLK, up mode, clear timer
    TIMER_A1->CTL = TIMER_A_CTL_SSEL__SMCLK | TIMER_A_CTL_MC__UP | TIMER_A_CTL_CLR;

}


/**********************************************************************
 * Starts the system
 *********************************************************************/
void System_Init()
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    SPI_Init();
    I2C_Init();
    Timer32_1_Init();
    Port1_Init();
    Port2_Init();
    Port4_Init();
    Port6_Init();
    Port7_Init();
    ADC14_init();
//    SysTick_Init();

    SevenSeg_Init();

    Motor_Position_Init(); //This is a slow start up command
    NVIC_EnableIRQ(T32_INT1_IRQn);
    NVIC_EnableIRQ (EUSCIB1_IRQn); //Enable EUSCIB1 interrupt
    NVIC->ISER[0]=1<<((TA2_N_IRQn)&31); //Enable timer A interrupt

    __enable_irq();
}
