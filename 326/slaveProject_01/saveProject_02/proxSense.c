#include "proxSense.h"
#include "Clocks.h"
#include "msp.h"

/**
 * Accepts a 1 or a 0 to either turn on of off the line
 */
void UpdateWarningObjectLine(uint8_t distWarn)
{
   if (distWarn)
    {
        P6->OUT |= BIT1;
        return 0;
    }
   else
    {
        P6->OUT &= ~BIT1;
        return 1;
    }//this pin will need to change
}


void UpdateWarningLine()
{
    UpdateWarningObjectLine(CheckDist());
}

