#include "msp.h"
#include "Clocks.h"

#define TOTALDIST 10

volatile double printablePeriod = 0;

volatile int distance[TOTALDIST];

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1
 * bit 0 indefinitely.
 *
 * This will trigger due to a power state error. Likely, this happened
 * because the System_Clock_Init function was called more than once.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/
void error(void)
{
    volatile uint32_t i;

    while (1)
    {
        P1->OUT ^= BIT0;
        for(i = 20000; i> 0; i--);   // Blink LED forever
    }
}

/**
 * This delay is used to separate the address and data messages
 * sent during SPI.
 * Not really a clock, but it's close enough
 */
void SPIDelay(int n) {
    int i, j;

    for (j = 0; j < n; j++)
        for (i = 50; i > 0; i--);      /* delay 1 ms */
}


/**********************************************************************
 * Setup SysTick to trigger an interrupt every two seconds.
 *********************************************************************/
void SysTick_Init()
{
    SysTick->LOAD = 6000000;
    SysTick->VAL = 0;
    SysTick->CTRL = 0x00000007;
}
/**********************************************************************
 * Sets a flag that represents the desired LED state.
 *********************************************************************/
void SysTick_Handler()
{

}

/**********************************************************************
 * Sets up Timer32_1 to interrupt every 1 second for a 48MHz clock
 *********************************************************************/
void Timer32_1_Init()
{
    TIMER32_1->CONTROL = 0b11100010; //Sets timer as disabled on startup,
                                    //wrapping
                                    //With Interrupt
                                    //No Prescaler, 32 bit mode
                                    //.  See 589 / reference manual
    TIMER32_1->LOAD = 300000; //.1 seconds
}

/**********************************************************************
 * Increments counters every one second
 *********************************************************************/
void T32_INT1_IRQHandler ( )    //Interrupt Handler for Timer 1
{
    TIMER32_1->INTCLR = 1;      //Clear interrupt flag so it does not interrupt again immediately.

    //Store the distance to the array
    static uint8_t index = 0;
    distance[index] = (GetPrintablePeriod() / 148) - 1;
    if(distance[index] == 2) distance[index] = 15; //errors in the calculation results in a 2, so this removes that issue
    index = (index > (TOTALDIST-1)) ? 0 : index + 1;

}

/**
 * Calculates the average distance read by the proximity sensor
 */
uint8_t CheckDist()
{
    uint8_t mean = 0;
    uint8_t index = 0;
    for (index = 0; index < (TOTALDIST - 1); index++)
    {
       if(distance[index] >= 15)
           return 0;
    }

    return 1;
}


/*********************************************************************
 * Calculates the pulse width from the proximity sensor by taking
 * multiplying the number of cycles counted since the last interrupt
 * by the resolution of the counter.
 ********************************************************************/
void TA2_N_IRQHandler(void)
{
    TIMER_A2->CTL |= TIMER_A_CTL_CLR; //Reset the timer
    int currentedge = TIMER_A2->CCR[4];
    double period = currentedge * 1.333;

    if (400 < period < 65000)
    {
        printablePeriod = abs(period);
    }
    TIMER_A2->CCTL[4] &= ~(TIMER_A_CCTLN_CCIFG);
}

/**
 * Returns the value of printablePeriod
 *
 * Use:
 * double distance = (GetPrintablePeriod() / 148) - 1;
 */
int GetPrintablePeriod()
{
    return (int)printablePeriod;
}
