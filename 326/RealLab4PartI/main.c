#include "msp.h"
uint8_t Read_Keypad();
void keypad_init();
void SysTick_Init();
void SysTick_delay(uint16_t delay);

uint8_t num, pressed;
volatile int input[3]; //Used to hold input from the keypad

/**********************************************************************
 * Program for 326 lab 4 Part I
 * Copied and altered from 226 Lab 7
 *
 * When a keypad button is pressed the number or symbol that was pressed
 * is printed to the CCS console.
 *********************************************************************/
void main(void)
{
    System_Init();

    input[0] = 0;
    pressed = 0;

    printf("You may press a key\n");

    while (1)
    {
        while (!pressed) //Blocks progress while waiting for a keypad press
        {
            pressed = Read_Keypad(); // Call Function to read Keypad
        }

        //The bottom three ifs handle the bottom row of the keypad
        if(input[0] <= 9)
            printf("The pressed key was %d\n", (input[0]));
        else if(input[0] == 10)
            printf("The pressed key was *\n");
        else if(input[0] == 11)
            printf("The pressed key was 0\n");
        else if(input[0] == 12)
            printf("The pressed key #\n");

        //Reset values
        pressed = 0;
        input[0] = 0;
        printf("You may press a new key\n");

    }
}

/**
 * Calls all functions needed to initializes the program
 */
void System_Init()
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD; // stop watchdog timer
    keypad_init(); // Call initialization functions for functionality
    __enable_irq();
    SysTick_Init();
}

/**
 * Initializes the keypad. All pins, 0-6 are outputs sending HIGH
 **/
void keypad_init()
{
    P4->SEL0 &= ~0x7F;
    P4->SEL1 &= ~0x7F;
    P4->DIR &= ~0xFF;
    P4->REN |= 0xFF;
    P4->OUT |= 0xFF;
// 456 are columns
// 0123 are rows
}

/**
 * Reads the keypad, and calculates the value of the button that was pressed.
 * This is done by setting every column to be an input, and then each column,
 * individually, is set to an output sending LOW. If any row is read to be LOW,
 **/
uint8_t Read_Keypad()
{
    uint8_t col, row, num;

    //Determine the column
    for (col = 0; col < 3; col++)
    {
        P4->DIR = 0x00; // Set Columns to inputs
        P4->DIR |= BIT(4 + col); // Set column 3 to output
        P4->OUT &= ~BIT(4 + col); // Set column 3 to LOW
        SysTick_delay(10); // Delay the while loop
        row = P4->IN & 0x0F; // read all rows
        while (!(P4IN & BIT0 ) | !(P4IN & BIT1 ) | !( P4IN & BIT2 )
                | !( P4IN & BIT3 )) ; //This is a debouncer
        if (row != 0x0F)
            break; // if one of the input is low, some key is pressed.
    }

    P4->DIR = 0x00; // Set Columns to inputs

    //Determine the button that was pressed
    if (col == 3)
        return 0;
    if (row == 0x0E)
        num = 0 + col + 1; // key in row 0
    else if (row == 0x0D)
        num = 3 + col + 1; // key in row 1
    else if (row == 0x0B)
        num = 6 + col + 1; // key in row 2
    else if (row == 0x07)
        num = 9 + col + 1; // key in row 3
    else
        num = 99;


    input[0] = num; //Saves the number to the input[0] global variable
    return num; //Returns as pressed
}

/**
 * Initializes Systick to have maximum load, no interrupts, and to be enabled
 **/
void SysTick_Init()
{
    SysTick->CTRL = 0;
    SysTick->LOAD = 0x00FFFFFF; //Max load
    SysTick->VAL = 0;
    SysTick->CTRL = 0X00000005; //Use core clock, enabled
}
/**
 * Uses an input of mili seconds to delay the system.
 **/
void SysTick_delay(uint16_t delay)
{
    SysTick->LOAD = ((delay * 3000) - 1);
    SysTick->VAL = 0;
    while ((SysTick->CTRL & 0x00010000) == 0)
        ;
}

