#include "msp.h"
#include "stdint.h"
#include "ST7735.h"
#include "string.h"
#include <SunFlower.h>

/**********************************************************************
 * Lab 6 Part III
 * Authors: George, Bilguun
 *
 * Initialize the master clock to run at 48MHz, and the smclk to run at
 * 12MHz.
 * Interfaces with the LCD so that the numbers 0-9 are counted in each
 * corner and the center of the screen on a rotation.
 *********************************************************************/
void System_Clock_Init(volatile uint32_t i);
void error(void);

int main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW |             // Stop WDT
                 WDT_A_CTL_HOLD;

    System_Init();

    //Calls the counting function with the corner x y coordinates.
    while(1)
    {
        ScreenCornerCount(0, 32); //Top left
        ScreenCornerCount(108, 32); //Top right
        ScreenCornerCount(0, 132); //Bottom left
        ScreenCornerCount(108, 132); //Bottom right
        ScreenCornerCount(54, 82); //Center

        delayMs(3000); //Wait three seconds
    }
}

/**********************************************************************
 * Calls all initialization functions necessary for the program to run
 *********************************************************************/
void System_Init()
{
    volatile uint32_t i;
    System_Clock_Init(i); //Set the master clock to 48MHz and smclk to 12MHz

    ST7735_InitB(); //Initialize the screen to be black

    Output_On(); //Enable the screen
}

/**********************************************************************
 * Counts from 0 to 9 at the given x,y coordinates.
 *
 * top left      x = 0   y = 32
 * top right     x = 108 y = 32
 * bottom left   x = 0   y = 132
 * bottom right  x = 108 y = 132
 * center        x = 54  y = 82
 *
 * @param xcor The x coordinate of the top left pixel
 * @param ycor The y coordinate of the top left pixel
 *********************************************************************/
void ScreenCornerCount(uint8_t xcor, uint8_t ycor)
{
    uint8_t index;
    char num = '0'; //The number to be printed to the screen

    /*
     * Prints every integer from 0 through 9 to the LCD screen using the
     * DrawCharS function.
     */
    for(index = 0; index <= 9; index++)
    {
        ST7735_DrawCharS(xcor,ycor,num,32000,0,4); //Draw num on the LCD
        num++;
        delayMs(1000);
    }
    num = ' ';
    ST7735_DrawCharS(xcor,ycor,num,32000,0,4); //Remove the last number displayed
}

/**********************************************************************
 * System delay for a given number of miliseconds
 *
 * @param n number of miliseconds to delay.
 *********************************************************************/
void delayMs(int n)
{
    int j, i;
    for(j = 0; j < n; j++)
        //Delay 1 ms
        for(i = 3000; i > 0; i--);
}

/**********************************************************************
 * Changes the master clock to operate at 48 MHz and the submaster clock to
 * operate at 12 MHz.
 *
 * This is from example code supplied by TI
 *********************************************************************/
void System_Clock_Init(volatile uint32_t i)
{
    /* Step 1: Transition to VCORE Level 1: AM0_LDO --> AM1_LDO */
    /* Get current power state, if it's not AM0_LDO, error out */

    uint32_t currentPowerState;


    currentPowerState = PCM->CTL0 & PCM_CTL0_CPM_MASK;
    if (currentPowerState != PCM_CTL0_CPM_0)
        error();

    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    PCM->CTL0 = PCM_CTL0_KEY_VAL | PCM_CTL0_AMR_1;
    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    if (PCM->IFG & PCM_IFG_AM_INVALID_TR_IFG)
        error();

    if ((PCM->CTL0 & PCM_CTL0_CPM_MASK) != PCM_CTL0_CPM_1)
        error();

    /* Step 2: Configure Flash wait-state to 1 for both banks 0 & 1 */
    FLCTL->BANK0_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK0_RDCTL_WAIT_MASK)) |
    FLCTL_BANK0_RDCTL_WAIT_1;
    FLCTL->BANK1_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK1_RDCTL_WAIT_MASK)) |
    FLCTL_BANK1_RDCTL_WAIT_1;
    /* Step 3: Configure HFXT to use 48MHz crystal, source to MCLK & HSMCLK*/
    PJ->SEL0 |= BIT2 | BIT3;
    PJ->SEL1 &= ~(BIT2 | BIT3 );
    CS->KEY = CS_KEY_VAL;
    CS->CTL2 |= CS_CTL2_HFXT_EN | CS_CTL2_HFXTFREQ_3 | CS_CTL2_HFXTDRIVE;
    while (CS->IFG & CS_IFG_HFXTIFG)
        CS->CLRIFG |= CS_CLRIFG_CLR_HFXTIFG;
    /* Select MCLK & HSMCLK = HFXT, smlk has the 4 divider to bring it down to 12MHz */
    CS->CTL1 = CS->CTL1
            & ~(CS_CTL1_SELM_MASK | CS_CTL1_DIVM_MASK | CS_CTL1_SELS_MASK
                    | CS_CTL1_DIVHS_MASK) |
    CS_CTL1_SELM__HFXTCLK | CS_CTL1_SELS__HFXTCLK | 0x20000000; // selects the 4 divider for the smclk


    CS->KEY = 0; // Lock CS module from unintended accesses
}

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1 bit 0 indefinitely.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/
void error(void)
{
    volatile uint32_t i;

    while (1)
    {
        P1->OUT ^= BIT0;
        for(i = 20000; i> 0; i--);          // Blink LED forever
    }
}

