#include "msp.h"
#include "stdint.h"
#include "ST7735.h"
#include "string.h"


/**********************************************************************
 * Lab 6 Part I
 * Authors: George, Bilguun
 *
 * Interfaces with an LCD screen to display "EGR326".
 * Sets the master clock to run at 48MHz and the smclk to run
 * at 12MHz.
 *
 * The program writes each letter individually.
 *********************************************************************/
void System_Clock_Init(volatile uint32_t i);
void error(void);
void ScreenWrite();

int main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW |             // Stop WDT
                 WDT_A_CTL_HOLD;


   System_Init();

   ScreenWrite(); //Write EGR326 to the LCD

    while(1);
}

/**
 * Calls all initialization functions necessary for the program to run
 */
void System_Init()
{
    volatile uint32_t i;
    System_Clock_Init(i); //Set the master clock to 48MHz and smclk to 12MHz
    ST7735_InitB(); //Initialize the black tab of the LCD
    Output_On(); //Enable the LCD to begin working
}

/*********************************************************************
 * Writes 'EGR326' to the LCD screen in the center of the screen.
 * This is done by writing each character individually.
 *********************************************************************/
void ScreenWrite()
{
    char EGR[] = "E";

    ST7735_DrawString(5, 8, EGR, 32000);

    char EGR1[] = "G";

    ST7735_DrawString(7, 8, EGR1, 60000);

    char EGR2[] = "R";

    ST7735_DrawString(9, 8, EGR2, 32000);

    char _3[] = "3";

    ST7735_DrawString(11, 8, _3, 3000);

    char _2[] = "2";

    ST7735_DrawString(13, 8, _2, 32000);

    char _6[] = "6";

    ST7735_DrawString(15, 8, _6, 32000);


}

/*********************************************************************
 * Changes the master clock to operate at 48 MHz and the submaster clock to
 * operate at 12 MHz.
 *
 * This is example code supplied by TI. Some code to control LEDs were
 * removed. The code was also modified to divide the smclk to 12MHz.
 *********************************************************************/
void System_Clock_Init(volatile uint32_t i)
{

    uint32_t currentPowerState;


    currentPowerState = PCM->CTL0 & PCM_CTL0_CPM_MASK;
    if (currentPowerState != PCM_CTL0_CPM_0)
        error();

    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    PCM->CTL0 = PCM_CTL0_KEY_VAL | PCM_CTL0_AMR_1;
    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    if (PCM->IFG & PCM_IFG_AM_INVALID_TR_IFG)
        error();

    if ((PCM->CTL0 & PCM_CTL0_CPM_MASK) != PCM_CTL0_CPM_1)
        error();

    /* Step 2: Configure Flash wait-state to 1 for both banks 0 & 1 */
    FLCTL->BANK0_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK0_RDCTL_WAIT_MASK)) |
    FLCTL_BANK0_RDCTL_WAIT_1;
    FLCTL->BANK1_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK1_RDCTL_WAIT_MASK)) |
    FLCTL_BANK1_RDCTL_WAIT_1;
    /* Step 3: Configure HFXT to use 48MHz crystal, source to MCLK & HSMCLK*/
    PJ->SEL0 |= BIT2 | BIT3;
    PJ->SEL1 &= ~(BIT2 | BIT3 );
    CS->KEY = CS_KEY_VAL;
    CS->CTL2 |= CS_CTL2_HFXT_EN | CS_CTL2_HFXTFREQ_2 | CS_CTL2_HFXTDRIVE;
    while (CS->IFG & CS_IFG_HFXTIFG)
        CS->CLRIFG |= CS_CLRIFG_CLR_HFXTIFG;
    /* Select MCLK & HSMCLK = HFXT, smclk divided to 12MHz */
    CS->CTL1 = CS->CTL1
            & ~(CS_CTL1_SELM_MASK | CS_CTL1_DIVM_MASK | CS_CTL1_SELS_MASK
                    | CS_CTL1_DIVHS_MASK) |
    CS_CTL1_SELM__HFXTCLK | CS_CTL1_SELS__HFXTCLK;
    CS->KEY = 0; // Lock CS module from unintended accesses
}

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1 bit 0 indefinitely.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/
void error(void)
{
    volatile uint32_t i;

    while (1)
    {
        P1->OUT ^= BIT0;
        for(i = 20000; i> 0; i--);          // Blink LED forever
    }
}
