/* DriverLib Includes */
#include "msp.h"
#include "driverlib.h"

/* Standard Includes */
#include <stdint.h>
#include <stdbool.h>

#define SLAVE_ADDR 0x68

#define INITIAL_LOCATION 0x000200000
#define MEM_SPACE 2
uint8_t simulatedCalibrationData[] = "test data block"; // array to hold data
uint8_t* addr_pointer = INITIAL_LOCATION + 4; // pointer to address in flash for reading back values


uint8_t num, pressed=0;
int input[14];

int main(void)
{
    /* Stop Watchdog  */
    MAP_WDT_A_holdTimer();

   // halting the watch dog is done in the system_msp432p401r.c startup

    System_Clock_Init(); // Setting MCLK to 48MHz for faster programming

    System_Init();

    unsigned char timeDateReadback[7];

    uint8_t read_back_data[16]; // array to hold values read back from flash

    Cleaner();

    uint8_t run = 0;
    while (1)
    {
        //Reads input from the keypad and stores it in input[0].
        pressed = Read_Keypad();
        //If a * was pressed, read the time from the RTC and print the information to the console.
        if (input[0] == 3)
        {

            I2C1_burstRead(SLAVE_ADDR, 0, 7, read_back_data);


            uint8_t written = WriteToFlash(read_back_data, run);


            ConsolePrintStoredData(written);

            input[0] = 0;
            run++;
        }

    }
}

uint8_t WriteToFlash(uint8_t read_back_data[], uint8_t runNum)
{
    if (runNum < 4)
    {
        /* Unprotecting Info Bank 0, Sector 0 */
        MAP_FlashCtl_unprotectSector(FLASH_INFO_MEMORY_SPACE_BANK0,
        FLASH_SECTOR0);


        /* Program the flash with the new data. */
        while (!MAP_FlashCtl_programMemory(
                read_back_data, (void*) addr_pointer + MEM_SPACE, 7))
                ; // leave first 4 bytes unprogrammed


        /* Setting the sector back to protected */
        MAP_FlashCtl_protectSector(FLASH_INFO_MEMORY_SPACE_BANK0,
        FLASH_SECTOR0);

        addr_pointer += MEM_SPACE;

        return 1;
    }
    else
        return 0;
}

/**
 * TODO: set this up to receive a value to start the cleaning from
 */
void Cleaner()
{
    /* Unprotecting Info Bank 0, Sector 0 */
    MAP_FlashCtl_unprotectSector(FLASH_INFO_MEMORY_SPACE_BANK0, FLASH_SECTOR0);
    /* Erase the flash sector starting INITIAL_LOCATION. */
    while (!MAP_FlashCtl_eraseSector(INITIAL_LOCATION))
        ;
    /* Setting the sector back to protected */
    MAP_FlashCtl_protectSector(FLASH_INFO_MEMORY_SPACE_BANK0, FLASH_SECTOR0);
}

void ConsolePrintStoredData(uint8_t success)
{
    if(success)
    {
        printf("Information stored in memory\n");

        uint8_t i;
        for (i = 0; i < 7; i++)
        { // read values in flash after programming
            printf("%x\n", *addr_pointer++);
        }
    }
    else
    {
        printf("Memory was not written to\n");
    }


}

void System_Clock_Init()
{
    uint8_t i; // index

    uint32_t currentPowerState;


    currentPowerState = PCM->CTL0 & PCM_CTL0_CPM_MASK;
    if (currentPowerState != PCM_CTL0_CPM_0)
        error();

    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    PCM->CTL0 = PCM_CTL0_KEY_VAL | PCM_CTL0_AMR_1;
    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    if (PCM->IFG & PCM_IFG_AM_INVALID_TR_IFG)
        error();

    if ((PCM->CTL0 & PCM_CTL0_CPM_MASK) != PCM_CTL0_CPM_1)
        error();

    /* Step 2: Configure Flash wait-state to 1 for both banks 0 & 1 */
    FLCTL->BANK0_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK0_RDCTL_WAIT_MASK)) |
    FLCTL_BANK0_RDCTL_WAIT_1;
    FLCTL->BANK1_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK1_RDCTL_WAIT_MASK)) |
    FLCTL_BANK1_RDCTL_WAIT_1;
    /* Step 3: Configure HFXT to use 48MHz crystal, source to MCLK & HSMCLK*/
    PJ->SEL0 |= BIT2 | BIT3;
    PJ->SEL1 &= ~(BIT2 | BIT3 );
    CS->KEY = CS_KEY_VAL;
    CS->CTL2 |= CS_CTL2_HFXT_EN | CS_CTL2_HFXTFREQ_2 | CS_CTL2_HFXTDRIVE;
    while (CS->IFG & CS_IFG_HFXTIFG)
        CS->CLRIFG |= CS_CLRIFG_CLR_HFXTIFG;
    /* Select MCLK & HSMCLK = HFXT, smclk divided to 12MHz */
    CS->CTL1 = CS->CTL1
            & ~(CS_CTL1_SELM_MASK | CS_CTL1_DIVM_MASK | CS_CTL1_SELS_MASK
                    | CS_CTL1_DIVHS_MASK) |
    CS_CTL1_SELM__HFXTCLK | CS_CTL1_SELS__HFXTCLK;

    CS->KEY = 0; // Lock CS module from unintended accesses

}

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1 bit 0 indefinitely.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/
void error(void)
{
    volatile uint32_t i;

    while (1)
    {
        P1->OUT ^= BIT0;
        for(i = 20000; i> 0; i--);          // Blink LED forever
    }
}

/**********************************************************************
 * Performs all functions calls and register adjustments to initialize
 * the program.
 *********************************************************************/
void System_Init()
{
    //Stop watchdog timer
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;
    keypad_init();
    I2C1_init();
}


/**
 * Stores the values from the keypad in an array
 * This is blocking
 */
void RecordKeypadInput(uint8_t pressed)
{
    uint8_t receivedCount = 0;
    printf("Please enter time in the format: \n1)YY\n2)MM\n3)DD\n4)DOW\n5)HR\n6)mm\n7)ss\n");
    //Until all values for the date and time are received, wait for them to be given.
    while (receivedCount < 13)
    {
        pressed = Read_Keypad();
        if (pressed)
        {
            uint8_t index;
            /*
             * This for loop will shift numbers deeper into the array.
             * Numbers stored in lower indices (ex input[10]), will be
             * moved to next indice up (input[10]->input[11]).
             */
            for (index = 13; index > 0; index--)
            {
                input[index] = input[index - 1];
            }
            receivedCount++; //Add to the running count of numbers received.
            pressed = 0; //This tells the while loop to begin looking for a new button pressed.
        }
    }
}

/**
 * Stores the recorded values into the array that will be sent to the RTC via I2C
 */
void StageRecordedValues(unsigned char timeDateToSet[15])
{
    // ss mm hr DOW dd mm yy
    //Transfer the input values to the time array that will be sent to the RTC.
    timeDateToSet[0] = Converter(input[2], input[1]); //ss
    timeDateToSet[1] = Converter(input[4], input[3]); //mm
    timeDateToSet[2] = Converter(input[6], input[5]); //hr
    timeDateToSet[3] = input[7]; //DOW
    timeDateToSet[4] = Converter(input[9], input[8]); //DD
    timeDateToSet[5] = Converter(input[11], input[10]); //MM
    timeDateToSet[6] = Converter(input[13], input[12]); //YY
}

/*
 * Converts the decimal value into hexidecimals numbers where the digits
 * of the hexidecimal match the digits of the decimal values.
 * ex: 19 (base 10) -> 19 (base 16)
 * ex: 35 (base 10) -> 35 (base35)
 */
int Converter(uint8_t tens, uint8_t ones)
{
    return tens*16+ones;
}

//
int I2C1_burstWrite(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data)
 {
     if (byteCount <= 0)
         //No write was performed
         return -1;

     //Set up slave address
     EUSCI_B1->I2CSA = slaveAddr;
     //Enable transmitter
     EUSCI_B1->CTLW0 |= 0x0010;
     //Generate start and send slave address
     EUSCI_B1->CTLW0 |= 0x0002;
     //Wait until ready to transmit
     while(!(EUSCI_B1->IFG & 2));
     //Send memory address to slave
     EUSCI_B1->TXBUF = memAddr;

     //Send data one byte at a time
     do
     {
         //Wait until port is ready for transmit
         while (!(EUSCI_B1->IFG & 2));
         //Send data to slave
         EUSCI_B1->TXBUF = *data++;
         byteCount--;
     } while (byteCount > 0);

     //Wait until last transmit is done
     while(!(EUSCI_B1->IFG & 2));
     //Send stop
     EUSCI_B1->CTLW0 |=0x0004;
     //Wait until stop is sent
     while (EUSCI_B1->CTLW0 & 4);

     //Return zero for no error
     return 0;
 }


//Configure UCB1 as I2C
void I2C1_init(void)
{
    //Disable UCB1 during configuration
    EUSCI_B1 -> CTLW0 |=  1;
    //7-bit slave address, master, I2C, sync mode, SMCLK
    EUSCI_B1 -> CTLW0  =  0x0F81;
    //Set clock prescaler 12 MHz / 120 = 100kHz
    EUSCI_B1 -> BRW    =  120;
    //P6.5 and P6.4 for UCB1 clk is 6.5, sda is 6.4
    P6       -> SEL0  |=  0x30;
    P6       -> SEL1  &= ~0x30;
    //Enable UCB1 after configuration
    EUSCI_B1 -> CTLW0 &= ~1;
}

//Use burst read to read multiple bytes from consecutive locations
int I2C1_burstRead(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data)
{
    if (byteCount <= 0)
        //No read was performed
        return -1;

    //Setup slave address
    EUSCI_B1 -> I2CSA  = slaveAddr;
    //Enable transmitter
    EUSCI_B1 -> CTLW0 |= 0x0010;
    //Generate START and send slave address
    EUSCI_B1 -> CTLW0 |= 0x0002;
    //Wait until slave address is sent
    while((EUSCI_B1->CTLW0 & 2));
    //Send memory address to slave
    EUSCI_B1 -> TXBUF = memAddr;
    //Wait till last transmit is done
    while(!(EUSCI_B1 -> IFG & 2));
    //Enable receiver
    EUSCI_B1 -> CTLW0 &= ~0x0010;
    //Generate RESTART and send slave address
    EUSCI_B1 -> CTLW0 |= 0x0002;
    //Wait till RESTART is finished
    while(EUSCI_B1 -> CTLW0 & 2);

    //Receive data one byte at a time
    do
    {
        //When only one byte of data is left
        if (byteCount == 1)
            //Setup to send STOP after the last byte is received
            EUSCI_B1 -> CTLW0 |= 0x0004;
        //Wait till data is received
        while(!(EUSCI_B1 -> IFG & 1));
        //Read the received data
        *data++ = EUSCI_B1 -> RXBUF;
        byteCount--;
    } while (byteCount);

    //Wait until stop is sent
    while(EUSCI_B1 -> CTLW0 & 4) ;

    //Return zero for no error
    return 0;
}

/**
 *
 */
//System clock at 3 MHz
void delayMs(int n)
{
    int j, i;
    for(j = 0; j < n; j++)
        //Delay 1 ms
        for(i = 750; i > 0; i--);
}

/**
 *
 */
uint8_t Read_Keypad()
{
    uint8_t col, row;
    for (col = 0; col < 3; col++)
    {
        P4->DIR = 0x00; // Set Columns to inputs
        P4->DIR |= BIT(4 + col); // Set column 3 to output
        P4->OUT &= ~BIT(4 + col); // Set column 3 to LOW
        delayMs(10); // Delay the while loop
        row = P4->IN & 0x0F; // read all rows
        while (!(P4IN & BIT0 ) | !(P4IN & BIT1 ) | !( P4IN & BIT2 )
                | !( P4IN & BIT3 ))
            ;
        delayMs(5);

        if (row != 0x0F)
            break; // if one of the input is low, some key is pressed.
    }
    P4->DIR = 0x00; // Set Columns to inputs
    if (col == 3)
        return 0;
    if (row == 0x0E)
        num = 0 + col + 1; // key in row 0
    else if (row == 0x0D)
        num = 3 + col + 1; // key in row 1
    else if (row == 0x0B)
        num = 6 + col + 1; // key in row 2
    else if (row == 0x07)
        num = 9 + col + 1; // key in row 3
    else
        num = 40;

    if (num == 10)
    {
        input[0] = 10;
        return 0;
    }
    else if (num == 12)
        return 0;
    else if (num == 11)
        num = 0;

    input[0] = num;
    return 1;
}

/**
 *
 */
void keypad_init()
{
    P4->SEL0 &= ~0x7F;
    P4->SEL1 &= ~0x7F;
    P4->DIR &= ~0xFF;
    P4->REN |= 0xFF;
    P4->OUT |= 0xFF;
// 456 are columns
// 0123 are rows
}
