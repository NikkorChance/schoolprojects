#include "HallEffect.h"
#include "msp.h"
#include "Timers.h"
#include "stdlib.h"

#define NUM_RECORDS 10

volatile uint8_t hallCounter = 0;
volatile double SpeedRecord[NUM_RECORDS];
volatile double RPM = 0;


/**********************************************************************
 * Sets up Timer32_1 to interrupt every 1 second for a 48MHz clock
 *********************************************************************/
void Timer32_1_Init()
{
    TIMER32_1->CONTROL = 0b11100010; //Sets timer 1 for enabled,
                                    //wrapping
                                    //With Interrupt
                                    //No Prescaler, 32 bit mode
                                    //One Shot Mode.  See 589 / reference manual
    TIMER32_1->LOAD = 48000000; //1 seconds
}


/**********************************************************************
 * Saves the value of HallCounter to to SpeedRecord. If the number of
 * stored values is greater than NUM_RECORDS values will begin to be
 * overwritten in the order of oldest to newest.
 *********************************************************************/
void T32_INT1_IRQHandler ( )    //Interrupt Handler for Timer 1
{
    TIMER32_1->INTCLR = 1;      //Clear interrupt flag so it does not interrupt again immediately.
    static uint8_t index = 0;

    SpeedRecord[index++] = (float) hallCounter;

    index = (index > (NUM_RECORDS - 1)) ? 0 : index; //Protects from IndexOutOfBounds
    hallCounter = 0;
    SetProxWarningState(P6->IN & BIT1); //Set the warning state based on the warning input
}

/**********************************************************************
 * Calculates RPM by averaging the hallCounter over a period of
 * one minute.
 *
 * NOTE:
 * This takes into account that the values of hallCount are
 * stored every ONE SECOND, and that hallCount is incremented
 * TWICE for EVERY rotation.
 *********************************************************************/
double MeanRPM()
{
    uint8_t index;
    double sum = 0;

    //Sum all the stored values from hallCounter
    for(index = 0; index < NUM_RECORDS; index++)
    {
        sum = sum + SpeedRecord[index];
    }
    sum += sum * 60 / 2; //Multiply by 60 seconds and divide by the two arms of the motor
    RPM = ((double) sum )/ ((double) NUM_RECORDS); //Save to a global variable RPM

}

/**********************************************************************
 * Increments the number of breaks being recorded by the hall Counter
 *********************************************************************/
void IncrementHallCounter()
{
    hallCounter++;
}

/**********************************************************************
 * Returns the speed of the vehicle in miles per hour
 *********************************************************************/
double MeanSpeed()
{
    MeanRPM();
    const int footRadius = 6;

    double footPM = footRadius * 2 * 3.14159 * RPM;
    double footPH = footPM * 60;
    double milePH = footPH / 5280;
    return milePH;
}

/**********************************************************************
 * Converts the speed to a string that is printed to the LCD with the
 * quick print parameter.
 *********************************************************************/
int HallEffectSpeed()
{
    unsigned char speed[25] = 0;
    int mean = MeanSpeed()*10;
    int tenMean = mean;

    if (mean > 100)
    {
        speed[3] = (mean > 100) ? (mean % 10) + 48 : '0'; //tenth
        mean = mean / 10;
        speed[1] = mean > 10 ? (mean % 10) + 48 : '0'; //one
        mean = mean / 10;
        speed[2] = '.';
        speed[0] = mean > 0 ? (mean % 10) + 48 : '0'; //ten
    }
    else if(mean > 10)
    {
        speed[3] = (mean > 10) ? (mean % 10) + 48 : '0'; //tenth
        mean = mean / 10;
        speed[1] = mean > 1? (mean % 10) + 48 : '0'; //one
        mean = mean / 10;
        speed[2] = '.';
        speed[0] = '0'; //ten
    }
    else if (mean > 1)
    {
        speed[3] = (mean > 1) ? (mean % 10) + 48 : '0'; //tenth
        mean = mean / 10;
        speed[1] = '0'; //one
        mean = mean / 10;
        speed[2] = '.';
        speed[0] = '0'; //ten
    }
    else
    {
        speed[3] = '0';
        speed[1] = '0';
        speed[2] = '.';
        speed[0] = '0';
    }

    strcat(speed, " MPH");

    UpdateSpeed(speed,1);

    //Return the mean times 10
    return tenMean;
}
