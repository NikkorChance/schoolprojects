################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../HallEffect.c \
../LCD7735.c \
../RTC.c \
../Rotary.c \
../ST7735.c \
../SunFlower.c \
../Timers.c \
../main.c \
../startup_msp432p401r_ccs.c \
../system_msp432p401r.c 

C_DEPS += \
./HallEffect.d \
./LCD7735.d \
./RTC.d \
./Rotary.d \
./ST7735.d \
./SunFlower.d \
./Timers.d \
./main.d \
./startup_msp432p401r_ccs.d \
./system_msp432p401r.d 

OBJS += \
./HallEffect.obj \
./LCD7735.obj \
./RTC.obj \
./Rotary.obj \
./ST7735.obj \
./SunFlower.obj \
./Timers.obj \
./main.obj \
./startup_msp432p401r_ccs.obj \
./system_msp432p401r.obj 

OBJS__QUOTED += \
"HallEffect.obj" \
"LCD7735.obj" \
"RTC.obj" \
"Rotary.obj" \
"ST7735.obj" \
"SunFlower.obj" \
"Timers.obj" \
"main.obj" \
"startup_msp432p401r_ccs.obj" \
"system_msp432p401r.obj" 

C_DEPS__QUOTED += \
"HallEffect.d" \
"LCD7735.d" \
"RTC.d" \
"Rotary.d" \
"ST7735.d" \
"SunFlower.d" \
"Timers.d" \
"main.d" \
"startup_msp432p401r_ccs.d" \
"system_msp432p401r.d" 

C_SRCS__QUOTED += \
"../HallEffect.c" \
"../LCD7735.c" \
"../RTC.c" \
"../Rotary.c" \
"../ST7735.c" \
"../SunFlower.c" \
"../Timers.c" \
"../main.c" \
"../startup_msp432p401r_ccs.c" \
"../system_msp432p401r.c" 


