#include "msp.h"
#include "ST7735.h"
#include "LCD7735.h"

#define BACKGROUNDCOLOR ST7735_WHITE
#define FONTCOLOR ST7735_BLACK
#define LINECOLOR ST7735_RED
#define STRINGLENGTH 25

//Possible state of the warning message
enum warningState{
    on, off, prox, tmp
};

//Possible locations for messages to be stored
enum messageLoc{
    left, right, big
};

//A structure that defines the properties of a message
//Was implemented too late to be used well
struct message{
    volatile uint8_t loc;
};

struct message time = {big};
struct message temp = {left};
struct message speed= {right};

//Flags specifying the state of the warning box
volatile uint8_t _warningState = off;
volatile uint8_t warningBox = 0;
volatile uint8_t _proxState = off;
volatile uint8_t _tmpState = off;

//The string for each message
volatile unsigned char leftMessage[STRINGLENGTH];
volatile unsigned char rightMessage[STRINGLENGTH];
volatile unsigned char bigMessage[STRINGLENGTH];
volatile unsigned char warningMessage[STRINGLENGTH] = "Near Obj";


/**********************************************************************
 * Calls the functions required to setup the LCD screen
 *********************************************************************/
void LCD_Init()
{
    System_Clock_Init(); //Setup system to run at 48Mhz, SMCLK at 12Mhz, ACLK to REFOCLK

    ST7735_InitB(); //Initialize the screen to be black

    Output_On(); //Enable the screen
    ST7735_FillScreen(BACKGROUNDCOLOR);
    DrawDisplayBackground(LINECOLOR);

}

/**********************************************************************
 * Changes the master clock to operate at 48 MHz and the submaster clock to
 * operate at 12 MHz.
 *
 * This is from example code supplied by TI
 *********************************************************************/
void System_Clock_Init()
{

    uint32_t currentPowerState;


    currentPowerState = PCM->CTL0 & PCM_CTL0_CPM_MASK;
    if (currentPowerState != PCM_CTL0_CPM_0)
        error();

    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    PCM->CTL0 = PCM_CTL0_KEY_VAL | PCM_CTL0_AMR_1;
    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    if (PCM->IFG & PCM_IFG_AM_INVALID_TR_IFG)
        error();

    if ((PCM->CTL0 & PCM_CTL0_CPM_MASK) != PCM_CTL0_CPM_1)
        error();

    /* Step 2: Configure Flash wait-state to 1 for both banks 0 & 1 */
    FLCTL->BANK0_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK0_RDCTL_WAIT_MASK)) |
    FLCTL_BANK0_RDCTL_WAIT_1;
    FLCTL->BANK1_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK1_RDCTL_WAIT_MASK)) |
    FLCTL_BANK1_RDCTL_WAIT_1;
    /* Step 3: Configure HFXT to use 48MHz crystal, source to MCLK & HSMCLK*/
    PJ->SEL0 |= BIT2 | BIT3;
    PJ->SEL1 &= ~(BIT2 | BIT3 );
    CS->KEY = CS_KEY_VAL;
    CS->CTL2 |= CS_CTL2_HFXT_EN | CS_CTL2_HFXTFREQ_2 | CS_CTL2_HFXTDRIVE;
    while (CS->IFG & CS_IFG_HFXTIFG)
        CS->CLRIFG |= CS_CLRIFG_CLR_HFXTIFG;

    /* Select MCLK & HSMCLK = HFXT, smclk divided to 12MHz */
    CS->CTL1 = CS->CTL1 & ~(CS_CTL1_SELM_MASK |
                CS_CTL1_DIVM_MASK |
                CS_CTL1_SELS_MASK |
                CS_CTL1_DIVHS_MASK) |
                CS_CTL1_SELM__HFXTCLK |
                CS_CTL1_SELS__HFXTCLK;

    CS->CTL1 |= 0x20000000; //clock divided sub master clock
    CS->KEY = 0; //lock CS module from unintended accesses

    CS->KEY = 0; // Lock CS module from unintended accesses

}

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1
 * bit 0 indefinitely.
 *
 * This will trigger due to a power state error. Likely, this happened
 * because the System_Clock_Init function was called more than once.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/
void error(void)
{
    volatile uint32_t i;

    while (1)
    {
        P1->OUT ^= BIT0;
        for(i = 20000; i> 0; i--);   // Blink LED forever
    }
}

/**
 * Reprints the whole LCD screen
 */
void UpdateWholeLCD()
{
    //Clear and print each message
    ClearLeftBox();
    PrintLeftBox(leftMessage);
    ClearRightBox();
    PrintRightBox(rightMessage);
    ClearBigBox();
    PrintBigBox(bigMessage);

//    DrawDisplayBackground(LINECOLOR);

    //If the warning state is on show the warning box and message
    if(_warningState == on)
    {
        //The two warningBox states determine the color of the box
        if(warningBox == 0)
        {
            PrintMiddleBox(warningMessage);
            DrawWarningBox(ST7735_RED);
        }
        else if(warningBox == 1)
        {
            PrintMiddleBox(warningMessage);
            DrawWarningBox(ST7735_YELLOW);
        }
        //Toggle the warning box color
        warningBox = (warningBox == 0) ? 1 : 0;
    }
}

/**********************************************************************
 * Calls all three quick update Functions
 *********************************************************************/
void QuickUpdateWholeLCD()
{
    QuickUpdateLCDSpeed();
    QuickUpdateLCDTime();
    QuickUpdateLCDTemp();
}

/**
 * Updates the LCD with the saved warning message
 */
void UpdateLCDWarningMessage()
{
    static uint8_t memWarn = off;

    if(memWarn != _warningState)
    {
        ClearBigBox();
        PrintBigBox(bigMessage); //Reprint big box for visibility
        ClearWarningBox();
        memWarn = _warningState;

    }
    //If the warning state is on,
    if (_warningState == on)
    {

        if (warningBox == 0)
        {
            PrintMiddleBox(warningMessage);
            DrawWarningBox(ST7735_RED);
        }
        else if (warningBox == 1)
        {
            PrintMiddleBox(warningMessage);
            DrawWarningBox(ST7735_YELLOW);
        }
        //Toggle the warning box color
        warningBox = (warningBox == 0) ? 1 : 0;
    }
    else
    {
        DrawDisplayBackground(LINECOLOR);
    }

}

/**
 * Prints the saved left message on the LCD screen
 */
void UpdateLCDLeftMessage()
{
    static unsigned char left[STRINGLENGTH] = "-1";

    //If the string has changed since the last update
    if(strcmp(left,leftMessage) != 0)
        PrintLeftBox(leftMessage);

    //Update the stored message with the current one
    strcpy(left, leftMessage);
}

/**
 * Prints the saved right message on the LCD screen
 */
void UpdateLCDRightMessage()
{
    static unsigned char right[STRINGLENGTH] = "-1";

    //If the string has changed since the last update
    if(strcmp(right, rightMessage) != 0)
        PrintRightBox(rightMessage);

    //Update the stored message with the current one
    strcpy(right, rightMessage);
}

/**********************************************************************
 * Prints the saved big message on the LCD screen
 *********************************************************************/
void UpdateLCDBigMessage()

{
    static unsigned char big[STRINGLENGTH] = "-1";

    static uint8_t warn = -1;
    uint8_t temp = strcmp(big,bigMessage);
    //If the string or warning state has changed since the last update
    if((strcmp(big,bigMessage) != 0 ) || warn != _warningState)
    {
        warn = _warningState;
        PrintBigBox(bigMessage);
    }

    //Update the stored values with the current ones
    strcpy(big, bigMessage);

}

/**********************************************************************
 * Update the correct LCD box with the saved time string.
 * Use this function so that the entire LCD doesn't need to be updated
 * when only the time needs to be updated.
 *********************************************************************/
void UpdateLCDTime()
{
    ClearBigBox();
    if(time.loc == left){
        UpdateLCDLeftMessage();
    }
    else if(time.loc == right){
        UpdateLCDRightMessage();
    }
    else if(time.loc == big){
        UpdateLCDBigMessage();
    }
    DrawDisplayBackground(LINECOLOR);
}

/**********************************************************************
 * Update the correct LCD box with the saved time string without
 * clearing the screen or redrawing the display background.
 * Use this function so that the entire LCD doesn't need to be updated
 * when only the time needs to be updated.
 *********************************************************************/
void QuickUpdateLCDTime()
{
    if(time.loc == left){
        UpdateLCDLeftMessage();
    }
    else if(time.loc == right){
        UpdateLCDRightMessage();
    }
    else if(time.loc == big){
        UpdateLCDBigMessage();
    }
}

/**********************************************************************
 * Update the correct LCD box with the saved speed string.
 * Use this function so that the entire LCD doesn't need to be updated
 * when only the time needs to be updated.
 *********************************************************************/
void UpdateLCDSpeed()
{
    ClearBigBox();
    if(speed.loc == left){

        UpdateLCDLeftMessage();
    }
    else if(speed.loc == right){
        UpdateLCDRightMessage();
    }
    else if(speed.loc == big){
        UpdateLCDBigMessage();
    }
    DrawDisplayBackground(LINECOLOR);
}

/**********************************************************************
 * Update the correct LCD box with the saved speed string.
 * Use this function so that the entire LCD doesn't need to be updated
 * when only the time needs to be updated.
 *********************************************************************/
void QuickUpdateLCDSpeed()
{
    if(speed.loc == left){
        UpdateLCDLeftMessage();
    }
    else if(speed.loc == right){
        UpdateLCDRightMessage();
    }
    else if(speed.loc == big){
        UpdateLCDBigMessage();
    }
}

/**********************************************************************
 * Update the correct LCD box with the saved temp string.
 * Use this function so that the entire LCD doesn't need to be updated
 * when only the time needs to be updated.
 *********************************************************************/
void UpdateLCDTemp()
{
    ClearBigBox();
    if(temp.loc == left){
        UpdateLCDLeftMessage();
    }
    else if(temp.loc == right){
        UpdateLCDRightMessage();
    }
    else if(temp.loc == big){
        UpdateLCDBigMessage();
    }
    DrawDisplayBackground(LINECOLOR);
}

/**********************************************************************
 * Update the correct LCD box with the saved temp string.
 * Use this function so that the entire LCD doesn't need to be updated
 * when only the time needs to be updated.
 *********************************************************************/
void QuickUpdateLCDTemp()
{
    if(temp.loc == left){
        UpdateLCDLeftMessage();
    }
    else if(temp.loc == right){
        UpdateLCDRightMessage();
    }
    else if(temp.loc == big){
        UpdateLCDBigMessage();
    }
}

/**********************************************************************
 * Re organizes the text boxes counter clockwise
 *********************************************************************/
void RotateTextCounterClockWise()
{
    unsigned char tempor[STRINGLENGTH];
    strcpy(tempor, leftMessage);
    UpdateLeftMessage(rightMessage);
    UpdateRightMessage(bigMessage);
    UpdateBigMessage(tempor);

    temp.loc  = (--temp.loc  > 2)  ? 2  : temp.loc;
    time.loc  = (--time.loc  > 2)  ? 2  : time.loc;
    speed.loc = (--speed.loc > 2)  ? 2  : speed.loc;
}

/**********************************************************************
 * Re organizes the text boxes clockwise
 *********************************************************************/
void RotateTextClockWise()
{
    unsigned char tempor[STRINGLENGTH];
    strcpy(tempor, leftMessage);

    UpdateLeftMessage(bigMessage);
    UpdateBigMessage(rightMessage);
    UpdateRightMessage(tempor);

    time.loc  = (++time.loc  > 2) ? 0 : time.loc;
    speed.loc = (++speed.loc > 2) ? 0 : speed.loc;
    temp.loc  = (++temp.loc  > 2) ? 0 : temp.loc;
}

/**********************************************************************
 * Updates the correct string with a given time string
 *
 * Format as
 * "hh:mm_pmMM/DD/YY__DOW"
 *********************************************************************/
void UpdateTime(unsigned char word[], uint8_t quick)
{
    if(time.loc == left)
        UpdateLeftMessage(word);
    else if(time.loc == right)
        UpdateRightMessage(word);
    else if(time.loc == big)
        UpdateBigMessage(word);

    if(quick)
        QuickUpdateLCDTime();
    else
        UpdateLCDTime();
}

/**********************************************************************
 * Updates the correct string with a given speed string
 *********************************************************************/
void UpdateSpeed(unsigned char word[], uint8_t quick)
{
    if(speed.loc == left)
        UpdateLeftMessage(word);
    else if(speed.loc == right)
        UpdateRightMessage(word);
    else if(speed.loc == big)
        UpdateBigMessage(word);

    //Currently assuming that if this function is called the LCD should
    //also be updated
    if(quick)
        QuickUpdateLCDSpeed();
    else
        UpdateLCDSpeed();
}

/**********************************************************************
 * Updates the correct string with a given temp string
 *********************************************************************/
void UpdateTemp(unsigned char word[], uint8_t quick)
{
    if(temp.loc == left)
        UpdateLeftMessage(word);
    else if(temp.loc == right)
        UpdateRightMessage(word);
    else if(temp.loc == big)
        UpdateBigMessage(word);

    //Currently assuming that if this function is called the LCD should
    //also be updated
    if(quick)
        QuickUpdateLCDTemp();
    else
        UpdateLCDTemp();
}

/**********************************************************************
 * Replace the current message with a new one
 *********************************************************************/
void UpdateLeftMessage(unsigned char word[])
{
    strcpy(leftMessage, word);
}

/**********************************************************************
 * Replace the current message with a new one
 *********************************************************************/
void UpdateRightMessage(unsigned char word[])
{
    strcpy(rightMessage, word);
}

/**********************************************************************
 * Replace the current message with a new one
 *********************************************************************/
void UpdateBigMessage(unsigned char word[])
{
    strcpy(bigMessage, word);
}

/**********************************************************************
 * Replace the current message with a new one
 *********************************************************************/
void UpdateWarningMessage(unsigned char word[])
{
    strcpy(warningMessage, word);
}

/**********************************************************************
 * Prints a given string to the LCD at given x,y coordinates.
 *
 * When font is 1:
 * Max of 16 characters horizontally
 *
 * When font is 4:
 * top left      x = 0   y = 32
 * top right     x = 108 y = 32
 * bottom left   x = 0   y = 132
 * bottom right  x = 108 y = 132
 * center        x = 54  y = 82
 *
 * @param word The string to be printed
 * @param xcor The x coordinate of the top left pixel
 * @param ycor The y coordinate of the top left pixel
 * @param font The desired font for the string
 *********************************************************************/
void PrintLCD(unsigned char word[], uint8_t xcor, uint8_t ycor, uint8_t font)
{
    uint8_t index;
    uint8_t len = strlen(word);

    /* Draws every character from the given string to the LCD
     * individually. */
    for(index = 0; index < len; index++)
    {
        ST7735_DrawCharS(xcor,ycor,word[index],FONTCOLOR,BACKGROUNDCOLOR,font);
        xcor+=8*font;
    }
}

/**********************************************************************
 * Grabs the time and date information stored in a given flash memory location,
 * and then prints that information to the LCD.
 *
 * @param addr_loc The location where the time and date information was saved
 * @param xcor The LCD x coordinate to print the information
 * @param ycor The LCD y coordinate to print the time line, date line will be one line below
 * @param font The size of the font on the LCD
 *********************************************************************/
uint8_t LCDDisplayTimeDateMemory(uint8_t* addr_loc, uint8_t xcor, uint8_t ycor, uint8_t font)
{
    unsigned char timeToPrint[9];
    unsigned char dateToPrint[9];

    //sec
    timeToPrint[6] = (*addr_loc >> 4)+48;
    timeToPrint[7] = *addr_loc % 16 +48;
    *addr_loc++;
    timeToPrint[2] = 58; // ':'

    //min
    timeToPrint[3] = (*addr_loc >> 4)+48;
    timeToPrint[4] = *addr_loc % 16 +48;
    *addr_loc++;
    timeToPrint[5] = 58; // ':'

    //hour
    timeToPrint[0] = (*addr_loc >> 4)+48;
    timeToPrint[1] = *addr_loc % 16 +48;
    *addr_loc++;

    *addr_loc++; //day of the week is skipped

    //month
    dateToPrint[3] = (*addr_loc >> 4)+48;
    dateToPrint[4] = *addr_loc % 16 +48;
    *addr_loc++;
    dateToPrint[2] = 47; // '/'

    //day
    dateToPrint[0] = (*addr_loc >> 4)+48;
    dateToPrint[1] = *addr_loc % 16 +48;
    *addr_loc++;
    dateToPrint[5] = 47; // '/'

    //year
    dateToPrint[6] = (*addr_loc >> 4)+48;
    dateToPrint[7] = *addr_loc % 16 +48;
    *addr_loc++;
    dateToPrint[8] = 0; //Null character

    PrintLCD(timeToPrint, xcor, ycor, font);
    ycor+=8;
    PrintLCD(dateToPrint, xcor, ycor, font);

    return ycor+8;
}

/**********************************************************************
 * Clears the large box
 *********************************************************************/
void ClearBigBox()
{
    ST7735_FillRect(0,85,128,100, BACKGROUNDCOLOR);
}

/**********************************************************************
 * Clears the left box
 *********************************************************************/
void ClearLeftBox()
{
    ST7735_FillRect(0,0,63,84, BACKGROUNDCOLOR);
}

/**********************************************************************
 * Clears the right box
 *********************************************************************/
void ClearRightBox()
{
    ST7735_FillRect(65,0,64,84, BACKGROUNDCOLOR);
}

/**********************************************************************
 * Clears the warning box
 *********************************************************************/
void ClearWarningBox()
{
    ST7735_FillRect(0,72,128,38, BACKGROUNDCOLOR);
}

/**********************************************************************
 * @param word A message less than 16 characters to be printed to the
 * LCD screen
 *********************************************************************/
void PrintLeftBox(unsigned char word[])
{
    uint8_t firstLine  = 50;

    uint8_t secLine1   = 45;
    uint8_t secLine2   = 55;

    uint8_t thiLine1  = 40;
    uint8_t thiLine2  = 50;
    uint8_t thiLine3  = 60;

    uint8_t wordLen = strlen(word);

    unsigned char topWord[9];
    unsigned char midWord[9];
    unsigned char lowWord[9];

    //Split the word into 8 character parts
    findFirst(word, topWord, wordLen); //Store the first 8 characters

    if (wordLen > 8) //If the word has more than 8
        findMid(word, midWord, wordLen); //Save next 8 here

    if (wordLen > 16) //If the word has more than 16
        findLast(word, lowWord, wordLen); //Save next 8 here

    //Print to the screen
    if(wordLen <= 8)
    {
        PrintLCD(topWord, 0, firstLine, 1); //Print on the first line
    }
    else if(wordLen <= 16)
    {
        PrintLCD(topWord, 0, secLine1, 1); //Print on the first line
        PrintLCD(lowWord, 0, secLine2, 1); //Print on the second line
    }
    else
    {
        PrintLCD(topWord, 0, thiLine1, 1); //Print on the first line
        PrintLCD(midWord, 0, thiLine2, 1); //Print on the second line
        PrintLCD(lowWord, 0, thiLine3, 1); //Print on the third line
    }
}

/**********************************************************************
 * @param word A message less than 16 characters to be printed to the
 * LCD screen
 *********************************************************************/
void PrintRightBox(unsigned char word[])
{
    uint8_t firstLine = 50;

    uint8_t secLine1  = 45;
    uint8_t secLine2  = 55;

    uint8_t thiLine1  = 40;
    uint8_t thiLine2  = 50;
    uint8_t thiLine3  = 60;

    uint8_t wordLen = strlen(word);

    unsigned char topWord[9];
    unsigned char midWord[9];
    unsigned char lowWord[9];

    //Split the word into 8 character parts
    findFirst(word, topWord, wordLen); //Store the first 8 characters

    if (wordLen > 8) //If the word has more than 8
        findMid(word, midWord, wordLen); //Save next 8 here

    if (wordLen > 16) //If the word has more than 16
        findLast(word, lowWord, wordLen); //Save next 8 here

    //Print to the screen
    if (wordLen <= 8)
    {
        PrintLCD(topWord, 66, firstLine, 1); //Print on the first line
    }
    else if(wordLen <= 16)
    {
        PrintLCD(topWord, 66, secLine1, 1); //Print on the first line
        PrintLCD(midWord, 66, secLine2, 1); //Print on the second line
    }
    else
    {
        PrintLCD(topWord, 66, thiLine1, 1); //Print on the first line
        PrintLCD(midWord, 66, thiLine2, 1); //Print on the second line
        PrintLCD(lowWord, 66, thiLine3, 1); //Print on the third line
    }
}


/**********************************************************************
 * @param word A message less than 16 characters to be printed to the
 * LCD screen
 *********************************************************************/
void PrintBigBox(unsigned char word[])
{
    uint8_t firstLine;

    uint8_t secLine1;
    uint8_t secLine2;

    uint8_t thiLine1;
    uint8_t thiLine2;
    uint8_t thiLine3;

    //The locations of the lines depend on the _warningState
    if(_warningState == off){
        firstLine = 110;
        secLine1 = 100;
        secLine2 = 120;
        thiLine1 = 95;
        thiLine2 = 115;
        thiLine3 = 135;
    }
    else if(_warningState == on){
        firstLine = 125;
        secLine1 = 125;
        secLine2 = 145;
        thiLine1 = 120;
        thiLine2 = 130;
        thiLine3 = 140;
    }
    uint8_t wordLen = strlen(word);

    unsigned char topWord[9];
    unsigned char midWord[9];
    unsigned char lowWord[9];

    //Split the word into 8 character parts
    findFirst(word, topWord, wordLen); //Store the first 8 characters

    if (wordLen > 8) //If the word has more than 8
        findMid(word, midWord, wordLen); //Save next 8 here

    if (wordLen > 16) //If the word has more than 16
        findLast(word, lowWord, wordLen); //Save next 8 here

    //Print to the screen
    if (wordLen <= 8)
    {
        PrintLCD(topWord, 0, firstLine, 2); //Print on the first line
    }
    else if(wordLen <= 16)
    {
        PrintLCD(topWord, 0, secLine1, 2); //Print on the first line
        PrintLCD(lowWord, 0, secLine2, 2); //Print on the second line
    }
    //There needs to be these two conditionals due to space constraints
    else if(wordLen <= 24 && _warningState == on)
    {
        //Print in smaller text so that it fits but isn't emphasized
        PrintLCD(topWord, 30, thiLine1, 1); //Print on the first line
        PrintLCD(midWord, 30, thiLine2, 1); //Print on the second line
        PrintLCD(lowWord, 30, thiLine3, 1); //Print on the third line
    }
    else if(wordLen <= 24 && _warningState == off)
    {
        PrintLCD(topWord, 0, thiLine1, 2); //Print on the first line
        PrintLCD(midWord, 0, thiLine2, 2); //Print on the second line
        PrintLCD(lowWord, 0, thiLine3, 2); //Print on the third line
    }
}

/**
 * Prints a message in the middle of the screen
 */
void PrintMiddleBox(unsigned char word[])
{
    uint8_t firstLine = 85;
    uint8_t secLine1 = 75;
    uint8_t secLine2 = 95;
    uint8_t wordLen = strlen(word);

    unsigned char topWord[9];
    unsigned char lowWord[9];

    //Split the word into 8 character parts
    findFirst(word, topWord, wordLen); //Store the first 8 characters

    if (wordLen > 8) //If the word has more than 8
        findLast(word, lowWord, wordLen); //Save it here

    //Print to the screen
    if (wordLen <= 8)
    {
        PrintLCD(topWord, 3, firstLine, 2); //Print on the first line
    }
    else
    {
        PrintLCD(topWord, 3, secLine1, 2); //Print on the first line
        PrintLCD(lowWord, 3, secLine2, 2); //Print on the second line
    }
}

/**********************************************************************
 *
 * @param source String with content
 * @param destination String to save first 8 characters to
 *********************************************************************/
void findFirst(unsigned char source[], unsigned char destination[], uint8_t len)
{
    uint8_t index;
    //Copy the characters
    for(index = 0; index <= 7 && index < len; index++)
    {
        destination[index] = source[index];
    }
    destination[index] = 0; //null terminator at the end
}

/**********************************************************************
 *
 * @param source String with content
 * @param destination String to save middle 8 characters to
 *********************************************************************/
void findMid(unsigned char source[], unsigned char destination[], uint8_t len)
{
    uint8_t index;
    //Copy the characters
    for(index = 8; index < len && index < 16; index++)
    {
        destination[index-8] = source[index];
    }
    destination[index-8] = 0; //null terminator at the end
}

/**********************************************************************
 *
 * @param source String with content
 * @param destination String to save last 8 characters to
 *********************************************************************/
void findLast(unsigned char source[], unsigned char destination[], uint8_t len)
{
    uint8_t index;
    //Copy the characters
    for(index = 16; index < len && index < 24; index++)
    {
        destination[index-16] = source[index];
    }
    destination[index-16] = 0; //null terminator at the end
}

/**********************************************************************
 * Draws three boxes on the LCD screen using the DrawFast line commands
 *********************************************************************/
void DrawDisplayBackground(uint16_t color)
{
    uint8_t yBot = 85; //The y position of the bottom of the horizontal line
    uint8_t xRig = 64; //The x position of the right side of the vertical line

    //Draw the horizontal divider
    ST7735_DrawFastHLine(0, (yBot-1), 128, color);
    ST7735_DrawFastHLine(0, yBot, 128, color);

    //Draw the vertcal divider
    ST7735_DrawFastVLine((xRig-1), 0, yBot, color);
    ST7735_DrawFastVLine(xRig, 0, yBot, color);
}

/**********************************************************************
 * Draw the border for the warning box
 *********************************************************************/
void DrawWarningBox(uint16_t color)
{

    uint8_t yTop = 73;
    uint8_t yBot = 111;
    uint8_t xLeft = 1;
    uint8_t xRight = 127;

    //Draw top
    ST7735_DrawFastHLine(0, (yTop-1), 128, color);
    ST7735_DrawFastHLine(0, yTop, 128, color);

    //Draw bottom
    ST7735_DrawFastHLine(0, (yBot-1), 128, color);
    ST7735_DrawFastHLine(0, yBot, 128, color);

    //Draw left
    ST7735_DrawFastVLine(xLeft, yTop, 38, color);
    ST7735_DrawFastVLine(xLeft-1, yTop, 38, color);

    //Draw right
    ST7735_DrawFastVLine(xRight, yTop, 38, color);
    ST7735_DrawFastVLine(xRight-1, yTop, 38, color);
}

/**********************************************************************
 * Set the warning message state
 *********************************************************************/
void SetwarningState()
{
    //    _warningProxState = newProxwarn;
    if (_proxState == prox)
    {
        unsigned char clearBox[25] = 0;
        UpdateWarningMessage(clearBox);
        UpdateWarningMessage("Near Obj");
        _warningState = on;
    }
    else if(_tmpState == tmp)
    {
        unsigned char clearBox[25] = 0;
        UpdateWarningMessage(clearBox);
        UpdateWarningMessage("High Tmp");
        _warningState = on;
    }
    else
        _warningState = off;

}

/**
 * Toggle the value of the warning state flag
 */
void SetProxWarningState(uint8_t newProxwarn)
{
    _proxState = (newProxwarn == 2) ? prox : off;
}

