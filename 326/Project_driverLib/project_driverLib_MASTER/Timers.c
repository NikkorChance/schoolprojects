#include "Timers.h"
#include "msp.h"
#include "string.h"


void Initialize_Clocks()
{
    //Timer 1 is held within HallEffect.c. This was due to issues
    //with sending doubles as a return value. Moving all the
    //functions was the quickest solution.
    Timer32_1_Init();
    Timer32_2_Init();

}




/**********************************************************************
 * Sets up Timer32_2 to interrupt every 1 second for a 48MHz clock
 *********************************************************************/
void Timer32_2_Init()
{
    TIMER32_2->CONTROL = 0b11100010; //Sets timer 2 for disabled,
                                    //wrapping
                                    //With Interrupt
                                    //No Prescaler, 32 bit mode
                                    //One Shot Mode.  See 589 / reference manual
    //48000000 for one second
    TIMER32_2->LOAD = 24000000; //.5 seconds
}


void StartTimer32_1()
{
    TIMER32_1->CONTROL |= BIT7;
}

void StopTimer32_1()
{
    TIMER32_1->CONTROL &=~ BIT7;
}

void StartTimer32_2()
{
    TIMER32_2->CONTROL |= BIT7;
}

void StopTimer32_2()
{
    TIMER32_2->CONTROL &=~ BIT7;
}
