#ifndef LCD7735
#define LCD7735

/**********************************************************************
 * Calls the functions required to setup the LCD screen
 *********************************************************************/
void LCD_Init();

/**********************************************************************
 * Changes the master clock to operate at 48 MHz and the submaster clock to
 * operate at 12 MHz.
 *
 * This is from example code supplied by TI
 *********************************************************************/
void System_Clock_Init();

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1
 * bit 0 indefinitely.
 *
 * This will trigger due to a power state error. Likely, this happened
 * because the System_Clock_Init function was called more than once.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/
void error(void);

/**********************************************************************
 * Prints a given string to the LCD at given x,y coordinates.
 *
 * When font is 1:
 * Max of 16 characters horizontally
 *
 * When font is 4:
 * top left      x = 0   y = 32
 * top right     x = 108 y = 32
 * bottom left   x = 0   y = 132
 * bottom right  x = 108 y = 132
 * center        x = 54  y = 82
 *
 * @param word The string to be printed
 * @param xcor The x coordinate of the top left pixel
 * @param ycor The y coordinate of the top left pixel
 * @param font The desired font for the string
 *********************************************************************/
void PrintLCD(unsigned char word[], uint8_t xcor, uint8_t ycor, uint8_t font);

/**********************************************************************
 * Grabs the time and date information stored in a given flash memory location,
 * and then prints that information to the LCD.
 *
 * @param addr_loc The location where the time and date information was saved
 * @param xcor The LCD x coordinate to print the information
 * @param ycor The LCD y coordinate to print the time line, date line will be one line below
 * @param font The size of the font on the LCD
 *********************************************************************/
uint8_t LCDDisplayTimeDateMemory(uint8_t* addr_loc, uint8_t xcor, uint8_t ycor, uint8_t font);

/**********************************************************************
 * Prints the saved big message on the LCD screen
 *********************************************************************/
void UpdateLCDBigMessage();

/**********************************************************************
 * Replace the current message with a new one
 *********************************************************************/
void UpdateLeftMessage(unsigned char word[]);

/**********************************************************************
 * Replace the current message with a new one
 *********************************************************************/
void UpdateRightMessage(unsigned char word[]);

/**********************************************************************
 * Replace the current message with a new one
 *********************************************************************/
void UpdateBigMessage(unsigned char word[]);

/**********************************************************************
 * Replace the current message with a new one
 *********************************************************************/
void UpdateWarningMessage(unsigned char word[]);

/**********************************************************************
 * Clears the large box
 *********************************************************************/
void ClearBigBox();

/**********************************************************************
 * Clears the left box
 *********************************************************************/
void ClearLeftBox();

/**********************************************************************
 * Clears the right box
 *********************************************************************/
void ClearRightBox();
/**********************************************************************
 * Clears the warning box
 *********************************************************************/
void ClearWarningBox();

/**********************************************************************
 * @param word A message less than 16 characters to be printed to the
 * LCD screen
 *********************************************************************/
void PrintLeftBox(unsigned char word[]);

/**********************************************************************
 * @param word A message less than 16 characters to be printed to the
 * LCD screen
 *********************************************************************/
void PrintRightBox(unsigned char word[]);

/**********************************************************************
 * @param word A message less than 16 characters to be printed to the
 * LCD screen
 *********************************************************************/
void PrintBigBox(unsigned char word[]);

/**
 * Prints a message in the middle of the screen
 */
void PrintMiddleBox(unsigned char word[]);

/**********************************************************************
 *
 * @param source String with content
 * @param destination String to save first 8 characters to
 *********************************************************************/
void findFirst(unsigned char source[], unsigned char destination[], uint8_t len);

/**********************************************************************
 *
 * @param source String with content
 * @param destination String to save middle 8 characters to
 *********************************************************************/
void findMid(unsigned char source[], unsigned char destination[], uint8_t len);

/**********************************************************************
 *
 * @param source String with content
 * @param destination String to save last 8 characters to
 *********************************************************************/
void findLast(unsigned char source[], unsigned char destination[], uint8_t len);

/**********************************************************************
 * Draws three boxes on the LCD screen using the DrawFast line commands
 *********************************************************************/
void DrawDisplayBackground(uint16_t color);

/**********************************************************************
 * Draw the border for the warning box
 *********************************************************************/
void DrawWarningBox(uint16_t color);

/**********************************************************************
 * Turn the warning message
 *********************************************************************/
uint8_t warningStateOn();

/**********************************************************************
 * Turn the warning message off
 *********************************************************************/
uint8_t warningStateOff();

#endif
