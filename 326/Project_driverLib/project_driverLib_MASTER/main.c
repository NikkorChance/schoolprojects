/* DriverLib Includes */
#include "msp.h"
#include "driverlib.h"

#include "math.h"
#include "stdio.h"
#include "LCD7735.h"
#include "ST7735.h"
#include "string.h"
#include "RTC.h"
#include "Rotary.h"
#include "FlashWarn.h"

#define DEGREESYM 248
#define RTC_ADDR 0x68

uint8_t flashLetter = 0;

enum flashingStates
{
    flashon, flashoff, flashwait
};

enum changeState
{
    select, modify
};


void RotarRotateLCD(uint16_t* TE_CW, uint16_t* TE_CCW);

volatile uint16_t rotar_CW  = 0;
volatile uint16_t rotar_CCW = 0;
volatile uint8_t  encodedNum = 0;
volatile uint8_t rotar_buttonPressed = 0;
volatile uint8_t displayFlash = 0;
volatile uint8_t checkWarnState = 0; //Signals that the warning state should be updated


enum haste
{
    normal, quick
};

/**
 * Master program for the 326 Term Project
 */
void main(void)
{
    System_Init();
    unsigned char savedTime[25];
    unsigned char savedTemp[25];
    int savedSpeed;

    MemInit();
    while (1)
    {

//        SetwarningState((P6->IN & BIT1 ) && BIT1);

        //Update the time on the LCD screen and save the printed string
        RTCQuickUpdateTimeLCD(savedTime);

        //Rotate the text on the screen if found necessary
        RotarRotateLCD(&rotar_CW, &rotar_CCW);

        //Update the temp on the LCD, save the printed string
        RTCQuickUpateTempLCD(savedTemp);

        //Update the speed on the LCD, printed string not returned
        savedSpeed = HallEffectSpeed();

        if(checkWarnState)
        {
            UpdateLCDWarningMessage();
        }
        checkWarnState ^= BIT1; //Toggle the update for the warning message

        if (rotar_buttonPressed)
            TimeChangeSelect(savedTime);

        //Send the speed to the slave device
        UpdateSlave(savedSpeed);

        if(displayFlash){
            ShowFlash();
            displayFlash = 0;
        }

        __delay_cycles(4800000);
    }
}

/**
 * Sends the speed to the slave MSP device
 */
void UpdateSlave(int sendSpeed)
{
    static uint8_t overSpeed = 0; //when 1, speed is over 85mph
    if(sendSpeed >= 850 && overSpeed == 0){
        FlashSpeedWarnings(); //save the time that this warning set into flash
        overSpeed = 1;
    }
    else if(sendSpeed < 850 && overSpeed == 1)//if not over 85, reset overSpeed
        overSpeed = 0;

    const int slaveAddress = 0x48;
    unsigned char speedVal[2];
    //Decimal
    speedVal[0] = sendSpeed % 10;
    sendSpeed = sendSpeed / 10; //remove decimal
    //Ones and tens
    speedVal[1] = sendSpeed;

    //The memAdd is the decimal value, the data string is the integer value
    I2C1_burstWrite(slaveAddress, 0x11 , 2, speedVal);
}

/**
 * BIT5 interrupt button: show flash display
 */
void Port1_Init(){
    P1->SEL0 &=~ BIT5;
    P1->SEL1 &=~ BIT5;
    P1->DIR &=~ BIT5;
    P1->REN |= BIT5;
    P1->OUT |= BIT5;

    P1->IES |= BIT5;
    P1->IFG = 0;
    P1->IE |= BIT5;
}

/*
 * BIT5 - toggle show flash to stick or un stick the system in showFlash
 */
void PORT1_IRQHandler(){
    if(P1->IFG & BIT5)
    {
        ToggleShowFlash();
        displayFlash = 1; //set the flag to call flash;
        __delay_cycles(10); //debounce
        P1->IFG &=~BIT5; //clear bit5 flag
    }
    else //reset flags
        P1->IFG = 0;
}


/**********************************************************************
 * Initialize port 5
 *
 * Rotary Encoder:
 * BIT0 is CLK
 * BIT1 is DT
 * BIT2 is SW
 *
 * Hall Effect Sensor
 * BIT6 is ADC
 *********************************************************************/
void Port5_Init()
{
    P5->SEL0 &= ~0x47;
    P5->SEL1 &= ~0x47;
    P5->DIR |= 0x07;
    P5->DIR &= ~BIT6;
    P5->REN |= 0x47;
    P5->OUT |= 0x07;
    P5->OUT &= ~BIT6;

    P5->IES |= 0x47; //CLK is rising edge, SW is falling edge
    P5->IE |= 0x47; //Enable interrupts on CLK and SW
    P5->IFG = 0;
}

/**
 *
 * BIT1 - input rising edge: Proximity warning line
 */
void Port6_Init()
{
    P6->SEL0 &= ~BIT1;
    P6->SEL1 &= ~BIT1;
    P6->DIR  &= ~BIT1;
    P6->REN  |=  BIT1;
    P6->OUT  &= ~BIT1;

//    P6->IES  &=~ BIT1;
//    P6->IFG  &= ~BIT1;
//    P6->IE   |=  BIT1;

}

/**
 * Probably not going to be used
 */
void PORT6_IRQHandler()
{
    if(P6->IFG & BIT1)
    {
        //Set the prox warning state based on the input of P6.1
        SetProxWarningState(P6->IN & BIT1 >> 1);

        P6->IES ^= BIT1; //Change the interrupt edge for bit 1

        P6->IFG &= ~BIT1;
    }
    else
        P6->IFG = 0;
}

/**********************************************************************
 * Is triggered by a rising edge from CLK or a falling edge from SW.
 *
 * If triggered by CLK, DT is checked to determine if rotation was
 * clockwise or counterclockwise.
 * If triggered by SW, a global flag is enabled.
 * If triggred by neither, all interrupt flags are reset.
 *********************************************************************/
void PORT5_IRQHandler()
{
    if(P5->IFG & BIT0 || P5->IFG & BIT1)
    {

        uint8_t encoded = 0;
        uint8_t sum = 0;
        uint8_t outB = ((P5->IN & BIT1 ) == BIT1 );
        uint8_t outA = ((P5->IN & BIT0 ) == BIT0 );
        static uint8_t lastEncoded = 0;

        encoded = (outA << 1) | outB;

        sum = (lastEncoded << 2) | encoded;

        if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
            encodedNum++;
        if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
            encodedNum--;

        lastEncoded = encoded;

        if (P5->IFG & BIT1)
            rotaryEncoder();

        if ((P5->IN & BIT1 ) == BIT1)
            P5->IES |= BIT1; //low to high rising edge

        else if (!((P5->IN & BIT1 ) == BIT1 ))
            P5->IES &= ~BIT1; //high to low falling edge

        if (((P5->IN & BIT0 ) == BIT0 ))
            P5->IES |= BIT0; //high to low falling edge

        else if (!((P5->IN & BIT0 ) == BIT0 ))
            P5->IES &= ~BIT0;

        P5->IFG &= ~BIT1;
        P5->IFG &= ~BIT0;

    }

    //Interrupt from SW?
    else if (P5->IFG & BIT2)
    {
        rotar_buttonPressed++;
        P5->IFG &= ~BIT2; //Disable only the SW interrupt
    }
    //Hall effect sensor
    else if (P5->IFG & BIT6)
    {
        IncrementHallCounter();
        P5->IFG &= ~BIT6; //Disable only the hall effect interrupt
    }
    else
        //If this was reached, an unhandled interrupt occurred
        P5->IFG = 0; //Reset all interrupts
}

/**
 *
 */
void rotaryEncoder()
{
    static uint8_t lastNum = 0;

    if (encodedNum > lastNum)
        rotar_CCW++;

    else if (encodedNum < lastNum)
        rotar_CW++;

    lastNum = encodedNum;
}

/**
 * Verifies the position of the time change selector. If it isn't on
 * the first digit of the time value, it will shift it.
 * This contains some conditions that shouldn't be possible so that
 * there are no issues.
 */
uint8_t TimeChangeSelectChar(uint8_t selectedChar)
{
    int delta = rotar_CW - rotar_CCW;
    //Reset rotary counters
    rotar_CW = 0;
    rotar_CCW = 0;
    return TimeChangeSelectionDelta(selectedChar, delta);
}

/**
 * Utilizes the rotary encoder to change the value of time on the
 * screen. Flashes the selected value0
 */
void timeChangeChar(unsigned char savedTime[], uint8_t selectedChar)
{
    //In case the move happened while the letter was off, turn it back on
    UpdateTime(savedTime, 1); //Quick time update with the change

    unsigned char flashTime[25];//The time with flashed numbers
    strcpy(flashTime, savedTime);
    flashLetter = 0; //Set the flash to show the letter

    //Save the number of turns of the rotary in each direction
    uint16_t save_rotar_CCW = rotar_CCW;
    uint16_t save_rotar_CW  = rotar_CW;
    //Reset the values of the rotary counters
    rotar_CCW = 0;
    rotar_CW  = 0;

    while(!rotar_buttonPressed)
    {

        unsigned char savedTemp[25];
        int savedSpeed;
        //Update the temp on the LCD, save the printed string
        RTCQuickUpateTempLCD(savedTemp);
        //Update the speed on the LCD, printed string not returned
        savedSpeed = HallEffectSpeed();

        UpdateSlave(savedSpeed);


        int delta =  rotar_CW - rotar_CCW;

        if (delta != 0)
        {
            if (selectedChar == 20)
            {
                uint8_t currentDay = whichDay(savedTime);
                uint8_t newDay = currentDay + delta;
                TimeChangeUpdateDay(savedTime, newDay);
            }
            else
            {
                TimeChangeNumeric(savedTime, selectedChar, delta);
            }

            UpdateTime(savedTime, 1); //Quick time update with the change

            //Reset the values of the rotary counters
            rotar_CCW = 0;
            rotar_CW = 0;
        } //Save the changed time to the flashTime.

        //Reset the flashTime and then flash it again
        else if (flashLetter == 1)
        {
            //The state does not stay in reset to free the system
            flashLetter = 2;
            strcpy(flashTime, savedTime);
            flashTime[selectedChar] = 32; //space

            //Quick update time with the flashTime
            UpdateTime(flashTime, 1);
        }
        //Reset the flash time
        else if (flashLetter == 0)
        {
            strcpy(flashTime, savedTime);
            //Quick update time with the flashTime
            UpdateTime(flashTime, 1);
        }
    }
    rotar_buttonPressed = 0; //reset the flag

    //Check to make sure the days are valid for the month chosen
    TimeChangeDaysCheck(savedTime);

    //Restore the values of the rotary counters
    rotar_CCW = save_rotar_CCW;
    rotar_CW = save_rotar_CW;
}

/**
 * Checks on rotary input to select which measure of time should be
 * modified.
 */
void TimeChangeSelect(unsigned char word[])
{
    rotar_buttonPressed = 0; //reset the flag
    uint8_t stayInState = 1;

    unsigned char savedTime[25]; //The time to be changed
    strcpy(savedTime, word);
    //Add the acceptance boxes to the screen
    savedTime[16] = 'O';
    savedTime[17] = 'K';
    savedTime[22] = 'N';
    savedTime[23] = 'O';
    savedTime[24] =   0; //this is here because of an issue somewhere else


    unsigned char flashTime[25];//The time with flashed numbers
    strcpy(flashTime, savedTime);

    uint8_t selectedChar = 1;

    StartTimer32_2(); //start the timer to handle flashing delays
   while(stayInState)
    {
       unsigned char savedTemp[25];
       int savedSpeed;

       //Update the temp on the LCD, save the printed string
       RTCQuickUpateTempLCD(savedTemp);
       //Update the speed on the LCD, printed string not returned
       savedSpeed = HallEffectSpeed();

       UpdateSlave(savedSpeed);

       //Calculate which character to flash
        selectedChar = TimeChangeSelectChar(selectedChar);

        if(rotar_buttonPressed && selectedChar == 17) //save and quit
        {
            UpdateRTC(savedTime);
            stayInState = 0;
        }
        else if(rotar_buttonPressed && selectedChar == 23) //quit
        {
            stayInState = 0;
        }
        else if(rotar_buttonPressed) //change a value
        {
            rotar_buttonPressed = 0; //reset the flag
            timeChangeChar(savedTime, selectedChar); //Change the value of the selected character

        }
        //Save the changed time to the flashTime.
        else if (flashLetter == 3)
        {
            strcpy(flashTime, savedTime);
            flashLetter = 0;
        }
        //Reset the flashTime and then flash it again
        else if (flashLetter == 1)
        {
            //The state does not stay in reset to free the system
            flashLetter = 2;
            strcpy(flashTime, savedTime);
            flashTime[selectedChar] = 32; //space

            //Quick update time with the flashTime
            UpdateTime(flashTime, 1);
        }
        //Reset the flash time
        else if (flashLetter == 0)
        {
            flashTime[selectedChar] = savedTime[selectedChar];
            //Quick update time with the flashTime
            UpdateTime(flashTime, 1);
        }
    }

//   StopTimer32_2(); //stop the timer
   rotar_buttonPressed = 0; //reset the rotary flag
}


/**********************************************************************
 * Used to control the flashing of characters on the screen
 *********************************************************************/
void T32_INT2_IRQHandler ( )    //Interrupt Handler for Timer 1
{
    TIMER32_2->INTCLR = 1;      //Clear interrupt flag so it does not interrupt again immediately.
    if(flashLetter == 0)
    {
        flashLetter = 1;
        TIMER32_2 -> LOAD = 6000000; // seconds
    }
    else if(flashLetter == 3)
    {
        flashLetter = 0;
        TIMER32_2 -> LOAD = 12000000; // seconds
    }
    else if(flashLetter = 2)
    {
        flashLetter = 0;
        TIMER32_2 -> LOAD = 12000000; // seconds
    }
    SetwarningState();
}



/**********************************************************************
 *
 *********************************************************************/
void System_Init()
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    I2C1_init();
    LCD_Init();
    Initialize_Clocks();

    Port1_Init();
    Port5_Init();
    Port6_Init();

    NVIC_EnableIRQ( T32_INT1_IRQn );
    NVIC_EnableIRQ( T32_INT2_IRQn );

    NVIC_EnableIRQ(PORT1_IRQn);
    NVIC_EnableIRQ(PORT5_IRQn);
    NVIC_EnableIRQ(PORT6_IRQn);
    __enable_irq();
}
