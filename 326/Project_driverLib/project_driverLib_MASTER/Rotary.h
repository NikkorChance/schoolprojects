#ifndef ROTARY_H_
#define ROTARY_H_

#include "msp.h"

/**********************************************************************
 * Checks which direction the rotary encoder was turned and rotates
 * the text on the LCD in that direction.
 *********************************************************************/
void RotarRotateLCD(uint16_t* TE_CW, uint16_t* TE_CCW);



#endif /* ROTARY_H_ */
