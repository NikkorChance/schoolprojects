#include "FlashWarn.h"
#include "msp.h"
#include "driverlib.h"
#include "LCD7735.h"

#define INITIAL_ALARMLOCATION 0x000200004
#define INITIAL_SPEEDLOCATION 0x00020002C
#define INITIAL_LOCATION 0x000200000

#define MEMLENGTH 7

volatile uint32_t lastAlarmMem = INITIAL_ALARMLOCATION;
volatile uint32_t lastSpeedMem = INITIAL_SPEEDLOCATION;

const uint32_t alarmIndexMemLoc = 0x000200054;
const uint32_t speedIndexMemLoc = 0x000200060;


volatile uint8_t showFlash = 0; //when 1, the flash screen is enabled

unsigned char warnTimeReadBack[7];

//Holds the values of each alarm
volatile unsigned char alarm1[7];
volatile unsigned char alarm2[7];
volatile unsigned char alarm3[7];
volatile unsigned char alarm4[7];
volatile unsigned char alarm5[7];
uint8_t alarmIndexer;

//Holds the values of each speed excursion
volatile unsigned char speed1[7];
volatile unsigned char speed2[7];
volatile unsigned char speed3[7];
volatile unsigned char speed4[7];
volatile unsigned char speed5[7];
uint8_t speedIndexer;

/**
 *
 */
void MemInit()
{

    //Save the warning values
    ConvertMemToArray(lastAlarmMem,alarm1);
    UpdateWarningMemAddr();
    ConvertMemToArray(lastAlarmMem,alarm2);
    UpdateWarningMemAddr();
    ConvertMemToArray(lastAlarmMem,alarm3);
    UpdateWarningMemAddr();
    ConvertMemToArray(lastAlarmMem,alarm4);
    UpdateWarningMemAddr();
    ConvertMemToArray(lastAlarmMem,alarm5);
    UpdateWarningMemAddr();

    //Save the speed values
    ConvertMemToArray(lastSpeedMem, speed1);
    UpdateSpeedMemAddr();
    ConvertMemToArray(lastSpeedMem, speed2);
    UpdateSpeedMemAddr();
    ConvertMemToArray(lastSpeedMem, speed3);
    UpdateSpeedMemAddr();
    ConvertMemToArray(lastSpeedMem, speed4);
    UpdateSpeedMemAddr();
    ConvertMemToArray(lastSpeedMem, speed5);
    UpdateSpeedMemAddr();

//    alarmIndexer = 1;
//    speedIndexer = 1;
    showFlash = 0;
    GetStartingPoints(alarmIndexMemLoc, speedIndexMemLoc);
}

/**
 * saves the index values
 */
void GetStartingPoints(unsigned char alarm_ptr[], unsigned char speed_ptr[])
{
    alarmIndexer = alarm_ptr[0];
    speedIndexer = speed_ptr[0];
}




/**
 * Converts the stored time to an array
 */
void ConvertMemToArray(unsigned char addr_pointer[], unsigned char tempArray[])
{

    tempArray[0] = addr_pointer[0];
    tempArray[1] = addr_pointer[1];
    tempArray[2] = addr_pointer[2];
    tempArray[3] = addr_pointer[3];
    tempArray[4] = addr_pointer[4];
    tempArray[5] = addr_pointer[5];
    tempArray[6] = addr_pointer[6];
    tempArray[7] = addr_pointer[7];
    return tempArray[8];
}

/**
 * toggle the showFlash flag
 */
void ToggleShowFlash()
{
    showFlash ^= BIT1;
}

/**
 * Consumes the whole screen
 */
void ShowFlash()
 {
    uint32_t lastAlarmMemTEMP = lastAlarmMem;
    lastAlarmMem = INITIAL_ALARMLOCATION;
    uint32_t lastSpeedMemTEMP = lastSpeedMem;
    lastSpeedMem = INITIAL_SPEEDLOCATION;


    uint8_t ycor = 40;
    ClearScreen(); //clear screen
    PrintLCD("Warning Times", 0, ycor, 1);
    ycor+=8;
    //warning
    LCDDisplayTimeDateMemory(lastAlarmMem, 0, ycor, 1);
    ycor+=8;
    UpdateWarningMemAddr();
    LCDDisplayTimeDateMemory(lastAlarmMem, 0, ycor, 1);
    ycor+=8;
    UpdateWarningMemAddr();
    LCDDisplayTimeDateMemory(lastAlarmMem, 0, ycor, 1);
    ycor+=8;
    UpdateWarningMemAddr();
    LCDDisplayTimeDateMemory(lastAlarmMem, 0, ycor, 1);
    ycor+=8;
    UpdateWarningMemAddr();
    LCDDisplayTimeDateMemory(lastAlarmMem, 0, ycor, 1);
    ycor+=8;
    UpdateWarningMemAddr();

    PrintLCD("Speed Times", 0, ycor, 1);
    ycor+=8;
    //speed
    LCDDisplayTimeDateMemory(lastSpeedMem, 0, ycor, 1);
    ycor+=8;
    UpdateSpeedMemAddr();
    LCDDisplayTimeDateMemory(lastSpeedMem, 0, ycor, 1);
    ycor+=8;
    UpdateSpeedMemAddr();
    LCDDisplayTimeDateMemory(lastSpeedMem, 0, ycor, 1);
    ycor+=8;
    UpdateSpeedMemAddr();
    LCDDisplayTimeDateMemory(lastSpeedMem, 0, ycor, 1);
    ycor+=8;
    UpdateSpeedMemAddr();
    LCDDisplayTimeDateMemory(lastSpeedMem, 0, ycor, 1);
    ycor+=8;
    UpdateSpeedMemAddr();

    while(showFlash); //hold until the button is pressed again

    UpdateWholeLCD();

    lastAlarmMem = lastAlarmMemTEMP;
    lastSpeedMem = lastSpeedMemTEMP;

}

/**
 * Updates the warning values and then writes all values to flash
 */
void FlashAlarmWarnings()
{
    Cleaner();
    //Get the time
    GetWarnTime();

    //Updates the right alarm value
    switch (alarmIndexer)
    {
    case 1:
        strcpy(alarm1, warnTimeReadBack);
        break;
    case 2:
        strcpy(alarm2, warnTimeReadBack);
        break;
    case 3:
        strcpy(alarm3, warnTimeReadBack);
        break;
    case 4:
        strcpy(alarm4, warnTimeReadBack);
        break;
    case 5:
        strcpy(alarm5, warnTimeReadBack);
        break;
    }
    uint8_t tempAIndexer[2]=0;
    tempAIndexer[0] = alarmIndexer;

    uint8_t tempSIndexer[2]=0;
    tempSIndexer[0] = speedIndexer;

    WriteToFlash(speedIndexer,speedIndexMemLoc);

    WriteToFlash(alarmIndexer,alarmIndexMemLoc);

    alarmIndexer = (alarmIndexer > 5) ? 0 : alarmIndexer + 1;

    WriteAlarmWarnings();
    WriteSpeedWarnings();
}

/**
 * Updates the speed values and then writes all values to flash
 */
void FlashSpeedWarnings()
{
    Cleaner();
    //Get the alarm time
    GetWarnTime();

    //Update the right speed value
    switch (speedIndexer)
    {
    case 1:
        strcpy(speed1, warnTimeReadBack);
        break;
    case 2:
        strcpy(speed2, warnTimeReadBack);
        break;
    case 3:
        strcpy(speed3, warnTimeReadBack);
        break;
    case 4:
        strcpy(speed4, warnTimeReadBack);
        break;
    case 5:
        strcpy(speed5, warnTimeReadBack);
        break;
    }
    uint8_t tempAIndexer[2]=0;
    tempAIndexer[0] = alarmIndexer;

    uint8_t tempSIndexer[2]=0;
    tempSIndexer[0] = speedIndexer;

    WriteToFlash(speedIndexer,speedIndexMemLoc);

    WriteToFlash(alarmIndexer,alarmIndexMemLoc);
    //Increment the index value
    speedIndexer = (speedIndexer > 5) ? 0 : speedIndexer + 1;

    WriteAlarmWarnings();
    WriteSpeedWarnings();
}

/**
 * Write a time to flash memory. Call this when the system triggers
 * an alarm.
 * The initial location is 0x000200000
 * The end location is 0x000200020
 */
void WriteAlarmWarnings()
{

        //Write every value to flash
        WriteToFlash(alarm1, lastAlarmMem);
        UpdateWarningMemAddr();
        WriteToFlash(alarm2, lastAlarmMem);
        UpdateWarningMemAddr();
        WriteToFlash(alarm3, lastAlarmMem);
        UpdateWarningMemAddr();
        WriteToFlash(alarm4, lastAlarmMem);
        UpdateWarningMemAddr();
        WriteToFlash(alarm5, lastAlarmMem);
        UpdateWarningMemAddr();
}

/**
 * Write a time to flash memory. Call this when the system triggers
 * a speed excursion.
 * The initial location is 0x00020002C
 * The last location is 0x00020004C
 */
void WriteSpeedWarnings()
{
    //Write every value to flash
    WriteToFlash(speed1, lastSpeedMem);
    UpdateSpeedMemAddr();
    WriteToFlash(speed2, lastSpeedMem);
    UpdateSpeedMemAddr();
    WriteToFlash(speed3, lastSpeedMem);
    UpdateSpeedMemAddr();
    WriteToFlash(speed4, lastSpeedMem);
    UpdateSpeedMemAddr();
    WriteToFlash(speed5, lastSpeedMem);
    UpdateSpeedMemAddr();
}

/*
 * Increments and checks the warning memory addr
 */
void UpdateWarningMemAddr()
{
    //Increase the address pointer so that the next time write doesn't overwrite the flash
    lastAlarmMem = (lastAlarmMem >= 0x00200020) ?
    INITIAL_ALARMLOCATION :
                                                  lastAlarmMem + 0x8;
}

/**
 * Increments and checks the SpeedMemAddr value
 */
void UpdateSpeedMemAddr()
{
    //Increase the address pointer so that the next time write doesn't overwrite the flash
    lastSpeedMem = (lastSpeedMem >= 0x0020004C) ?
    INITIAL_SPEEDLOCATION :
                                                  lastSpeedMem + 0x8;
}

void GetWarnTime()
{
    //Retrieve the current time
    I2C1_burstRead(0x68, 0, 7, warnTimeReadBack);
//    return timeDateReadback;
}

/**
 * Saves data to flash at a specified location.
 *
 * @param read_back_data The information to be written to flash
 * @param addr_pointer   The location to save the data
 */
uint8_t WriteToFlash(uint8_t read_back_data[], uint8_t *addr_pointer)
{
    /* Unprotecting Info Bank 0, Sector 0 */
    MAP_FlashCtl_unprotectSector(FLASH_INFO_MEMORY_SPACE_BANK0,
    FLASH_SECTOR0);

    /* Program the flash with the new data. */
    while (!MAP_FlashCtl_programMemory(read_back_data, (void*) addr_pointer,
                                       MEMLENGTH))
        ; // leave first 4 bytes unprogrammed

    /* Setting the sector back to protected */
    MAP_FlashCtl_protectSector(FLASH_INFO_MEMORY_SPACE_BANK0,
    FLASH_SECTOR0);
}

/**
 * Writes 0xFF to all locations in the initial memory sector
 */
void Cleaner()
{
    /* Unprotecting Info Bank 0, Sector 0 */
    MAP_FlashCtl_unprotectSector(FLASH_INFO_MEMORY_SPACE_BANK0, FLASH_SECTOR0);
    /* Erase the flash sector starting INITIAL_LOCATION. */
    while (!MAP_FlashCtl_eraseSector(INITIAL_LOCATION))
        ;
    /* Setting the sector back to protected */
    MAP_FlashCtl_protectSector(FLASH_INFO_MEMORY_SPACE_BANK0, FLASH_SECTOR0);
}
