#include "Rotary.h"
#include "msp.h"


/**********************************************************************
 * Checks which direction the rotary encoder was turned and rotates
 * the text on the LCD in that direction.
 *********************************************************************/
void RotarRotateLCD(uint16_t* TE_CW, uint16_t* TE_CCW)
{
    if (*TE_CW > 0)
    {
        while (*TE_CW)
        {
            RotateTextClockWise();
            UpdateWholeLCD();
            (*TE_CW)--;
        }
    }
    else if ((*TE_CCW) > 0)
    {
        while ((*TE_CCW))
        {
            RotateTextCounterClockWise();
            UpdateWholeLCD();
            (*TE_CCW)--;
        }
    }
}
