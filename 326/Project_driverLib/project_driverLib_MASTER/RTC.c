#include "RTC.h"
#include "msp.h"
#include "string.h"

#define RTC_ADDR 0x68

/**********************************************************************
 * This file contains functions for updating the LCD screen with the
 * time stored on a real time clock (RTC) as well as functions for
 * updating the LCD screen with temperature being measured by
 * that same RTC.
 *********************************************************************/

/**********************************************************************
 * Calls the proper functions to update the LCD with the current time
 * utilizing the RTC quick update function
 *********************************************************************/
void RTCQuickUpdateTimeLCD(unsigned char returnTime[])
{
    unsigned char timeDateReadback[7];
    //Retrieve the current time
    I2C1_burstRead(RTC_ADDR, 0, 7, timeDateReadback);

    unsigned char formattime[23];
    FormatLCDTime(timeDateReadback, formattime);
    UpdateTime(formattime, 1); //Update quick

    //Save the formattime to the return time
    strcpy(returnTime, formattime);
}

/**********************************************************************
 * Returns the most current time from the RTC.
 *
 *
 * This isn't going to work because the value will be erased by
 * the garbage collector. This function is being used right now,
 * but it will need to be changed if it ever needs to be.
 *********************************************************************/
unsigned char* TimeUpdateRTC()
{
    unsigned char timeDateReadback[7];
    //Retrieve the current time
    I2C1_burstRead(RTC_ADDR, 0, 7, timeDateReadback);
    return timeDateReadback;
}

/**********************************************************************
 * Format as
 * "hh:mm:ssMM/DD/YY__DOW"
 *********************************************************************/
unsigned char * FormatLCDTime(unsigned char time[], unsigned char returntime[])
{
    //sec
    returntime[6] = (time[0] >> 4) + 48;
    returntime[7] = (time[0] % 16) + 48;
    returntime[5] = 58; // ':'
    //min
    returntime[3] = (time[1] >> 4) + 48;
    returntime[4] = (time[1] % 16) + 48;
    returntime[2] = 58; // ':'
    //hour
    returntime[0] = (time[2] >> 4) + 48;
    returntime[1] = (time[2] % 16) + 48;

    //month
    returntime[8] = (time[5] >> 4) + 48;
    returntime[9] = (time[5]  % 16) + 48;
    returntime[10] = '/';
    //day
    returntime[11] = (time[4] >> 4) + 48;
    returntime[12] = (time[4] % 16) + 48;
    returntime[13] = '/';
    //year
    returntime[14] = (time[6] >> 4) + 48;
    returntime[15] = (time[6] % 16) + 48;
    //day of theweek
    returntime[16] = ' ';
    returntime[17] = ' ';
    returntime[18] = ' ';
    returntime[19] = 0;
    returntime[20] = 0;
    returntime[21] = 0;
    strcat(returntime, getDayWeek(time[3]));

    returntime[24] = 0; //null
}

/**********************************************************************
 * Returns the day of the week as an array
 *********************************************************************/
unsigned char* getDayWeek(uint16_t hday)
{
    if(hday == 7)
        return "Sun  ";
    else if(hday == 1)
        return "Mon  ";
    else if(hday == 2)
        return "Tue  ";
    else if(hday == 3)
        return "Wed  ";
    else if(hday == 4)
        return "Thu  ";
    else if(hday == 5)
        return "Fri  ";
    else if(hday == 6)
        return "Sat  ";
}

/**********************************************************************
 * Returns a value signifying the day
 *********************************************************************/
uint8_t whichDay(unsigned char time[])
{
    if(strstr(time, "Mon"))
        return 1;
    else if(strstr(time,"Tue"))
        return 2;
    else if(strstr(time,"Wed"))
        return 3;
    else if(strstr(time,"Thu"))
        return 4;
    else if(strstr(time, "Fri"))
        return 5;
    else if(strstr(time, "Sat"))
        return 6;
    else if(strstr(time,"Sun"))
        return 7;
    else
        return 255;
}

/**
 * Updates the time string with the new day
 */
void TimeChangeUpdateDay(unsigned char time[], uint8_t hday)
{
    //Check for roll over
    if(hday > 250)
        hday = 7; //Sunday
    else if(hday > 7)
        hday = 1; //Monday

    //Set the day
    if (hday == 1)
    {
        time[19] = 'M';
        time[20] = 'o';
        time[21] = 'n';
    }
    else if (hday == 2)
    {
        time[19] = 'T';
        time[20] = 'u';
        time[21] = 'e';
    }
    else if (hday == 3)
    {
        time[19] = 'W';
        time[20] = 'e';
        time[21] = 'd';
    }
    else if (hday == 4)
    {
        time[19] = 'T';
        time[20] = 'h';
        time[21] = 'u';
    }
    else if (hday == 5)
    {
        time[19] = 'F';
        time[20] = 'r';
        time[21] = 'i';
    }
    else if (hday == 6)
    {
        time[19] = 'S';
        time[20] = 'a';
        time[21] = 't';
    }
    else if (hday == 7)
    {
        time[19] = 'S';
        time[20] = 'u';
        time[21] = 'n';
    }
}


/**********************************************************************
 * Updates the RTC with a new time
 *********************************************************************/
void UpdateRTC(unsigned char unformatTime[])
{
    unsigned char timeDateToSet[15];
//    timeDateToSet[0] = 0x00;
    //    timeDateToSet[2] = Converter(unformatTime[0],unformatTime[1]); //hr
    //    timeDateToSet[1] = Converter(unformatTime[3],unformatTime[4]); //mm
    //    timeDateToSet[0] = Converter(unformatTime[6],unformatTime[7]); //ss
    //    timeDateToSet[3] = whichDay(unformatTime); //day of the week
    //    timeDateToSet[4] = Converter(unformatTime[11],unformatTime[12]); //DD
    //    timeDateToSet[5] = Converter(unformatTime[8],unformatTime[9]); //MM
    //    timeDateToSet[6] = Converter(unformatTime[14],unformatTime[15]); //YY
    //    timeDateToSet[7] = 0x00;
    timeDateToSet[0] = 0x01; //ss;
    timeDateToSet[1] = Converter(unformatTime[3],unformatTime[4]); //mm
    timeDateToSet[2] = Converter(unformatTime[0],unformatTime[1]); //hr
    timeDateToSet[3] = whichDay(unformatTime); //day of the week
    timeDateToSet[4] = Converter(unformatTime[11],unformatTime[12]); //DD
    timeDateToSet[5] = Converter(unformatTime[8],unformatTime[9]); //MM
    timeDateToSet[6] = Converter(unformatTime[14],unformatTime[15]); //YY
    timeDateToSet[7] = 0x08;
//    unsigned char NNNtimeDateToSet[15] = { Converter(unformatTime[6],unformatTime[7]), Converter(unformatTime[3],unformatTime[4]), Converter(unformatTime[0],unformatTime[1]), whichDay(unformatTime),  Converter(unformatTime[11],unformatTime[12]),  Converter(unformatTime[8],unformatTime[9]), Converter(unformatTime[14],unformatTime[15]), 0x08 };

//                                            ss   mm     hr   dow    DD    MM    YY  null
    unsigned char LLLtimeDateToSet[15] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x19, 0x08 };
//    [CORTEX_M4_0] 3:2:1 4 6-5-207
    I2C1_burstWrite(RTC_ADDR, 0, 7, timeDateToSet);
}


/**
 * Verifies the time on the screen so that it doesn't go over
 * traditional time rules
 */
void TimeChangeHighCheck(unsigned char screenTime[], uint8_t selectedChar)
{
    uint8_t Gozero = 0;
    uint8_t Goone = 0;

    //Converts the time to decimal
    uint8_t tens = 10 * (screenTime[selectedChar - 1] - 48);
    uint8_t ones = screenTime[selectedChar] - 48;
    uint8_t time = tens + ones;

    if(selectedChar == 1) //Hours
    {
        if(time > 23)
            Gozero = 1;
    }
    else if(selectedChar == 4 || selectedChar == 7) //mins and seconds
    {
        if(time > 59)
            Gozero = 1;
    }
    else if(selectedChar == 9) //month
    {
        if(time > 12)
            Goone = 1;
    }
    else if(selectedChar == 12) //days
    {
        uint8_t monthTen = 10 * (screenTime[8] - 48);
        uint8_t monthOne = screenTime[9] - 48;
        uint8_t monthTime = monthTen + monthOne;

        if(monthTime == 1 || monthTime == 3 || monthTime == 5 || monthTime == 7 || monthTime == 8 || monthTime == 10 || monthTime == 12){
            if(time > 31)
                Goone = 1;
        }
        else if(monthTime == 4 || monthTime == 6 || monthTime == 9 || monthTime == 11)
        {
            if(time > 30)
                Goone = 1;
        }
        else if(monthTime == 2){
            if(time > 28)
                Goone = 1;
        }
    }
    else if(selectedChar == 15) //year
    {
        if(screenTime[selectedChar -1] > '9')
            Gozero = 1;
    }
    //Any roll over will result in the same action
    if (Gozero)
    {
        screenTime[selectedChar - 1] = '0';
        screenTime[selectedChar] = '0';
    }
    if(Goone)
    {
        screenTime[selectedChar -1] = '0';
        screenTime[selectedChar] = '1';
    }
}

/**
 *
 */
void TimeChangeDaysCheck(unsigned char screenTime[])
{
    uint8_t Goone = 0;

    //Converts the time to decimal
    uint8_t tens = 10 * (screenTime[11] - 48);
    uint8_t ones = screenTime[12] - 48;
    uint8_t time = tens + ones;

    uint8_t monthTen = 10 * (screenTime[8] - 48);
    uint8_t monthOne = screenTime[9] - 48;
    uint8_t monthTime = monthTen + monthOne;

    if (monthTime == 1 || monthTime == 3 || monthTime == 5 || monthTime == 7
            || monthTime == 8 || monthTime == 10 || monthTime == 12)
    {
        if (time > 31)
            Goone = 1;
    }
    else if (monthTime == 4 || monthTime == 6 || monthTime == 9
            || monthTime == 11)
    {
        if (time > 30)
            Goone = 1;
    }
    else if (monthTime == 2)
    {
        if (time > 28)
            Goone = 1;
    }

    if(Goone)
    {
        screenTime[11] = '0';
        screenTime[12] = '1';
    }

}

/**
 * Verifies the time on the screen so that it doesn't go below
 * traditional time rules.
 * Hours, minutes, and seconds aren't checked because those are allowed
 * to be zero.
 */
void TimeChangeLowCheck(unsigned char screenTime[], uint8_t selectedChar)
{
    if(selectedChar == 1) //hour
    {
        if(screenTime[selectedChar -1] < 48){
            screenTime[selectedChar -1] = '2';
            screenTime[selectedChar] = '3';
        }
    }
    else if(selectedChar == 4) //min
    {
        if(screenTime[selectedChar -1] < 48){
            screenTime[selectedChar -1] = '5';
            screenTime[selectedChar] = '9';
        }

    }
    else if(selectedChar == 7)//sec
    {
        if(screenTime[selectedChar -1] < 48){
            screenTime[selectedChar - 1] = '5';
            screenTime[selectedChar] = '9';
        }
    }
    else if (selectedChar == 12) //days
    {
        uint8_t monthTen = 10 * (screenTime[8] - 48);
        uint8_t monthOne = screenTime[9] - 48;
        uint8_t monthTime = monthTen + monthOne;


        if (monthTime == 1 || monthTime == 3 || monthTime == 5 || monthTime == 7
                || monthTime == 8 || monthTime == 10 || monthTime == 12)
        {
            if (screenTime[selectedChar - 1] < 49 && screenTime[selectedChar] < 49)
            {
                screenTime[selectedChar - 1] = '3';
                screenTime[selectedChar] = '1';
            }
        }
        else if (monthTime == 4 || monthTime == 6 || monthTime == 9
                || monthTime == 11)
        {
            if (screenTime[selectedChar - 1] < 49 && screenTime[selectedChar] < 49)
            {
                screenTime[selectedChar - 1] = '3';
                screenTime[selectedChar] = '0';
            }
        }
        else if (monthTime == 2)
        {
            if (screenTime[selectedChar - 1] < 49 && screenTime[selectedChar] < 49)
            {
                screenTime[selectedChar - 1] = '2';
                screenTime[selectedChar] = '8';
            }
        }
    }
    else if(selectedChar == 9) //month
    {
        if (screenTime[selectedChar] < 49 && screenTime[selectedChar -1] < 49)
        {
            screenTime[selectedChar - 1] = '1';
            screenTime[selectedChar] = '2';
        }
    }
    else if (selectedChar == 15) //year
    {
        if (screenTime[selectedChar -1] < 48)
        {
            screenTime[selectedChar - 1] = '9';
            screenTime[selectedChar] = '9';
        }
    }


}

/**
 * Changes the time of sec, hour, min, day, month, and year
 */
void TimeChangeNumeric(unsigned char savedTime[], uint8_t selectedChar,
                       int delta)
{
    //If the value can be changed with just the delta, do it
    savedTime[selectedChar] = delta + savedTime[selectedChar];
    //If the value would be less than '0' subtract it from '9'
    if (savedTime[selectedChar] < '0')
    {
        savedTime[selectedChar] = '9';
        //Adjust tens place
        savedTime[selectedChar - 1] = savedTime[selectedChar - 1] - 1; //Decrease the value of the tens place
    }
    else if (savedTime[selectedChar] > '9')
    {
        savedTime[selectedChar] = '0';
        //Adjust tens place
        savedTime[selectedChar - 1] = savedTime[selectedChar - 1] + 1; //Increase the value of the tens place
    }

    //After every change in time, check to make sure there is no roll over needed
    TimeChangeLowCheck(savedTime, selectedChar); //Check to see if the decrease was invalid
    TimeChangeHighCheck(savedTime, selectedChar); //Check to see if the increase was invalid
}

/**
 * Verifies the position of the time change selector. If it isn't on
 * the first digit of the time value, it will shift it.
 * This contains some conditions that shouldn't be possible so that
 * there are no issues.
 */
uint8_t TimeChangeSelectionDelta(uint8_t selectedChar, int delta)
{

    if (delta == 0)
        return selectedChar;
    else if (selectedChar == 0)
    {
        if (delta > 0)
            return 4; //min
        else if (delta < 0)
            return 23; //year
    }
    else if (selectedChar == 1)
    {
        if (delta > 0)
            return 4; //min
        else if (delta < 0)
            return 23; //year
    }
    else if (selectedChar == 2)
    {
        if (delta > 0)
            return 4; //min
        else if (delta < 0)
            return 1; //year
    }
    else if (selectedChar == 3)
    {
        if (delta > 0)
            return 4; //min
        else if (delta < 0)
            return 1; //year
    }
    else if (selectedChar == 4)
    {
        if(delta > 0)
            return 7;
        else if(delta < 0)
            return 1;
    }
    else if(selectedChar == 5)
    {
        if(delta > 0)
            return 7;
        else if(delta < 0)
            return 4;
    }
    else if(selectedChar == 6)
    {
        if(delta > 0)
            return 7;
        else if(delta < 0)
            return 4;
    }
    else if(selectedChar == 7){
        if(delta > 0)
            return 9;
        else if(delta < 0)
            return 4;
    }
    else if(selectedChar == 8)
    {
        if(delta > 0)
            return 9;
        else if(delta < 0)
            return 9;
    }
    else if(selectedChar == 9)
    {
        if(delta > 0)
            return 12;
        else if(delta < 0)
            return 7;
    }
    else if(selectedChar == 10)
    {
        if(delta > 0)
            return 12;
        else if(delta < 0)
            return 9;
    }
    else if(selectedChar == 11){
        if(delta > 0)
            return 12;
        else if(delta < 0)
            return 9;
    }
    else if(selectedChar ==  12)
    {
        if(delta > 0)
            return 15;
        else if(delta < 0)
            return 9;
    }
    else if(selectedChar == 13)
    {
        if(delta > 0)
            return 15;
        else if(delta < 0)
            return 12;
    }
    else if(selectedChar == 14)
    {
        if(delta > 0)
            return 15;
        else if(delta < 0)
            return 12;
    }
    else if(selectedChar == 15)
    {
        if(delta > 0)
            return 17;
        else if(delta < 0)
            return 12;
    }
    else if(selectedChar == 16)
    {
        if(delta > 0)
            return 17;
        else if(delta < 0)
            return 15;
    }
    else if(selectedChar == 17) //OK
    {
        if(delta > 0)
            return 20;
        else if(delta < 0)
            return 15;
    }
    //skipped 18-19
    else if(selectedChar == 20) //DOW
    {
        if(delta > 0)
            return 23;
        else if(delta < 0)
            return 17;
    }
    //skipped 18-22
    else if(selectedChar == 23) //No
    {
        if(delta > 0)
            return 1;
        else if(delta < 0)
            return 20;
    }
}

/**********************************************************************
 * Converts the decimal value into a hexadecimal number where the digits
 * of the hexadecimal match the digits of the decimal values.
 * ex: 19 (base 10) -> 19 (base 16)
 * ex: 35 (base 10) -> 35 (base35)
 *********************************************************************/
int Converter(uint8_t tens, uint8_t ones)
{
    return (tens-48)*16+(ones-48);
}

/**********************************************************************
 * Calculates degrees in Fahrenheit out of the temp string received
 * from the RTC.
 *********************************************************************/
int Getfahrenheit(unsigned char tempReturn[])
{
    //The data from memAddr 0x12 comes in as two bits that represent quarters of a degree,
    //and this tree of if statements converts those numbers to decimal.
    int DegreeDecimal;

    if (tempReturn[1] == 192)
        DegreeDecimal = 75;
    else if (tempReturn[1] == 128)
        DegreeDecimal = 50;
    else if (tempReturn[1] == 64)
        DegreeDecimal = 25;
    else if (tempReturn[1] == 0)
        DegreeDecimal = 0;

    return DegreeDecimal;
}

/**
 * Reads the temperature stored by the RTC, converts it to an
 * unsigned char string and then prints that to the LCD using the quick
 * parameter.
 * The string is saved to the tempReturn argument.
 */
void RTCQuickUpateTempLCD(unsigned char tempReturn[])
{
    //Read the temperature from the RTC
    I2C1_burstRead(RTC_ADDR, 0x11, 0x02, tempReturn);

    //Get the decimal value
    int DegreeDecimal = Getfahrenheit(tempReturn);

    //Calculate the integer value in Fahrenheit
    int tempIntval = (1.8 * tempReturn[0]) + 32;

    //Create a value to format the decimal and integer value
    unsigned char deciStr[5] = 0;
    sprintf(deciStr, ".%d F",DegreeDecimal);
    unsigned char intStr[5] = 0;
    sprintf(intStr, "%d", tempIntval);

    //Copy the integer string and then concatenate the decimal string
    strcpy(tempReturn, intStr);
    strcat(tempReturn, deciStr);

//    strcat(tempReturn, deciStr);

    UpdateTemp(tempReturn, 1); //Update quick
}


/**********************************************************************
 * Writes data to a given address on the specified slave device.
 * Data is written via the I2C communication standard.
 *
 * @param slaveAddr The address of the slave device
 * @param memAddr The memory address of the slave to be overwritten
 * @param byteCount The number of bytes being sent
 * @param *data The address of the array storing data to be sent
 *
 * Modified from code written by Dr Kandalaft
 *********************************************************************/
int I2C1_burstWrite(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data)
 {
     if (byteCount <= 0)
         //No write was performed
         return -1;

     //Set up slave address
     EUSCI_B1->I2CSA = slaveAddr;
     //Enable transmitter
     EUSCI_B1->CTLW0 |= 0x0010;
     //Generate start and send slave address
     EUSCI_B1->CTLW0 |= 0x0002;
     //Wait until ready to transmit
     while(!(EUSCI_B1->IFG & 2));
     //Send memory address to slave
     EUSCI_B1->TXBUF = memAddr;

     //Send data one byte at a time
     do
     {
         //Wait until port is ready for transmit
         while (!(EUSCI_B1->IFG & 2));
         //Send data to slave
         EUSCI_B1->TXBUF = *data++;
         byteCount--;
     } while (byteCount > 0);

     //Wait until last transmit is done
     while(!(EUSCI_B1->IFG & 2));
     //Send stop
     EUSCI_B1->CTLW0 |=0x0004;
     //Wait until stop is sent
     while (EUSCI_B1->CTLW0 & 4);

     //Return zero for no error
     return 0;
 }


/**********************************************************************
 * Configure UCB1 as I2C on pins 6.5 (CLK) and 6.4 (SDA).
 *********************************************************************/
void I2C1_init(void)
{
    //Disable UCB1 during configuration
    EUSCI_B1 -> CTLW0 |=  1;
    //7-bit slave address, master, I2C, sync mode, SMCLK
    EUSCI_B1 -> CTLW0  =  0x0F81; //1 1 1 1 1 00 0 00 0 1
    //Set clock prescaler 12 MHz / 120 = 100 kHz
    EUSCI_B1 -> BRW    =  120;
    //P6.5 and P6.4 for UCB1 clk is 6.5, sda is 6.4
    P6       -> SEL0  |=  0x30;
    P6       -> SEL1  &= ~0x30;
    //Enable UCB1 after configuration
    EUSCI_B1 -> CTLW0 &= ~1;
}

/**********************************************************************
 * Use burst read to read multiple bytes from a specified memory
 * address on from a specified slave.
 *
 * @param slaveAddr The address of the slave device
 * @param memAddr The memory address of the slave to be read
 * @param byteCount The number of bytes to retrieve
 * @param *data The address of the array to store the bytes to
 *
 * Modified from code written by Dr Kandalaft
 *********************************************************************/
int I2C1_burstRead(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data)
{
    if (byteCount <= 0)
        //No read was performed
        return -1;

    //Setup slave address
    EUSCI_B1 -> I2CSA  = slaveAddr;
    //Enable transmitter
    EUSCI_B1 -> CTLW0 |= 0x0010;
    //Generate START and send slave address
    EUSCI_B1 -> CTLW0 |= 0x0002;
    //Wait until slave address is sent
    while((EUSCI_B1->CTLW0 & 2));
    //Send memory address to slave
    EUSCI_B1 -> TXBUF = memAddr;
    //Wait till last transmit is done
    while(!(EUSCI_B1 -> IFG & 2));
    //Enable receiver
    EUSCI_B1 -> CTLW0 &= ~0x0010;
    //Generate RESTART and send slave address
    EUSCI_B1 -> CTLW0 |= 0x0002;
    //Wait till RESTART is finished
    while(EUSCI_B1 -> CTLW0 & 2);

    //Receive data one byte at a time
    do
    {
        //When only one byte of data is left
        if (byteCount == 1)
            //Setup to send STOP after the last byte is received
            EUSCI_B1 -> CTLW0 |= 0x0004;
        //Wait till data is received
        while(!(EUSCI_B1 -> IFG & 1));
        //Read the received data
        *data++ = EUSCI_B1 -> RXBUF;
        byteCount--;
    } while (byteCount);

    //Wait until stop is sent
    while(EUSCI_B1 -> CTLW0 & 4) ;

    //Return zero for no error
    return 0;
}
