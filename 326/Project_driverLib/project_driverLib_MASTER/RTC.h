#ifndef RTC_H_
#define RTC_H_

#include "msp.h"

/**********************************************************************
 * Returns the most current time from the RTC.
 *********************************************************************/
unsigned char* TimeUpdateRTC();


/**********************************************************************
 * Format as
 * "hh:mm:ssMM/DD/YY__DOW"
 *********************************************************************/
unsigned char * FormatLCDTime(unsigned char time[], unsigned char returntime[]);

/**********************************************************************
 * Returns the day of the week as an array
 *********************************************************************/
unsigned char* getDayWeek(uint16_t hday);

/**********************************************************************
 * Writes data to a given address on the specified slave device.
 * Data is written via the I2C communication standard.
 *
 * @param slaveAddr The address of the slave device
 * @param memAddr The memory address of the slave to be overwritten
 * @param byteCount The number of bytes being sent
 * @param *data The address of the array storing data to be sent
 *
 * Modified from code written by Dr Kandalaft
 *********************************************************************/
int I2C1_burstWrite(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data);

/**********************************************************************
 * Configure UCB1 as I2C on pins 6.5 (CLK) and 6.4 (SDA).
 *********************************************************************/
void I2C1_init(void);

/**********************************************************************
 * Use burst read to read multiple bytes from a specified memory
 * address on from a specified slave.
 *
 * @param slaveAddr The address of the slave device
 * @param memAddr The memory address of the slave to be read
 * @param byteCount The number of bytes to retrieve
 * @param *data The address of the array to store the bytes to
 *
 * Modified from code written by Dr Kandalaft
 *********************************************************************/
int I2C1_burstRead(int slaveAddr, unsigned char memAddr, int byteCount, unsigned char* data);




#endif /* RTC_H_ */
