#include "msp.h"
#include "stdint.h"
#include "ST7735.h"
#include "string.h"
#include <SunFlower.h>

/**********************************************************************
 * Lab 6 Part II
 * Authors: George, Bilguun
 *
 * Displays an image for 3 seconds, then displays 4 menu options (non selectable).
 *********************************************************************/
void System_Clock_Init(volatile uint32_t i);

void error(void);

int main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW |             // Stop WDT
            WDT_A_CTL_HOLD;


    unsigned short flower = FlowerImage(); //Get the converted bitmap of the flower image

    ST7735_DrawBitmap(4, 159, flower, 127, 159); //Draw a sunflower immage on the LCD

    delayMs(3000); //Delay 3 seconds

    Output_Clear(); //Wipe the screen

    delayMs(1000); //Delay 1 second

    //Write the four menu items on the LCD screen

    char word[] = "Speed";
    LCDWrite(word);

    char word1[] = "Time";
    LCDWrite(word1);

    char word2[] = "Temp";
    LCDWrite(word2);

    char word3[] = "Mileage";
    LCDWrite(word3);

    while (1);
}

/**********************************************************************
 * Calls all initialization functions necessary for the program to run
 *********************************************************************/
void System_Init()
{
   volatile uint32_t i;
   System_Clock_Init(i); //Set the master clock to 48MHz and smclk to 12MHz

   ST7735_InitB(); //Initialize the screen to be black

   Output_On(); //Enable the screen
}

/**********************************************************************
 * Writes a word to the LCD screen and will center the word on the
 * screen.
 *
 * 4 is the lowest y
 * 0 is the lowest x
 *********************************************************************/
void LCDWrite(char *word)
{
    int len = strlen(word); //The length of the word sent
    int temp; //Used as an indexing variable

    int xcor = 12-(((len+10)+10)/2-5); //Calculate the starting x-coordinate
                                       //based on the length of the string
    static int ycor = 5; //y-coordinate of the first line written

    /*
     * Draw each character of the string individually. After each write
     * increment the x-coordinate of the character by 2.
     * Incrementing by two so that the letters don't overwrite each other.
     * Two is the value to increment by because that is the default scaling
     * factor of the DrawString function.
     */
    for(temp = 0; temp < len;temp++)
    {
        char* letter[] = word[temp]; //Store the character to be printed in a new array
        ST7735_DrawString(xcor,ycor,letter,32000);
        xcor+=2;
    }

    ycor += 2;
}

/**********************************************************************
 * System delay for a given number of miliseconds
 *
 * @param n number of miliseconds to delay
 *********************************************************************/
void delayMs(int n)
{
    int j, i;
    for(j = 0; j < n; j++)
        //Delay 1 ms
        for(i = 3000; i > 0; i--);
}

/**********************************************************************
 * Changes the master clock to operate at 48 MHz and the submaster clock to
 * operate at 12 MHz.
 *
 * This is example code supplied by TI. Some code to control LEDs were
 * removed. The code was also modified to divide the smclk to 12MHz.
 *********************************************************************/
void System_Clock_Init(volatile uint32_t i)
{
    /* NOTE: This example assumes the default power state is AM0_LDO.
     * Refer to  msp432p401x_pcm_0x code examples for more complete PCM operations
     * to exercise various power state transitions between active modes.
     */
    /* Step 1: Transition to VCORE Level 1: AM0_LDO --> AM1_LDO */
    /* Get current power state, if it's not AM0_LDO, error out */


    uint32_t currentPowerState;


    currentPowerState = PCM->CTL0 & PCM_CTL0_CPM_MASK;
    if (currentPowerState != PCM_CTL0_CPM_0)
        error();

    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    PCM->CTL0 = PCM_CTL0_KEY_VAL | PCM_CTL0_AMR_1;
    while ((PCM->CTL1 & PCM_CTL1_PMR_BUSY))
        ;

    if (PCM->IFG & PCM_IFG_AM_INVALID_TR_IFG)
        error();

    if ((PCM->CTL0 & PCM_CTL0_CPM_MASK) != PCM_CTL0_CPM_1)
        error();

    /* Step 2: Configure Flash wait-state to 1 for both banks 0 & 1 */
    FLCTL->BANK0_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK0_RDCTL_WAIT_MASK)) |
    FLCTL_BANK0_RDCTL_WAIT_1;
    FLCTL->BANK1_RDCTL = (FLCTL->BANK0_RDCTL & ~(FLCTL_BANK1_RDCTL_WAIT_MASK)) |
    FLCTL_BANK1_RDCTL_WAIT_1;
    /* Step 3: Configure HFXT to use 48MHz crystal, source to MCLK & HSMCLK*/
    PJ->SEL0 |= BIT2 | BIT3;
    PJ->SEL1 &= ~(BIT2 | BIT3 );
    CS->KEY = CS_KEY_VAL;
    CS->CTL2 |= CS_CTL2_HFXT_EN | CS_CTL2_HFXTFREQ_2 | CS_CTL2_HFXTDRIVE;
    while (CS->IFG & CS_IFG_HFXTIFG)
        CS->CLRIFG |= CS_CLRIFG_CLR_HFXTIFG;
    /* Select MCLK & HSMCLK = HFXT, no divider */
    CS->CTL1 = CS->CTL1
            & ~(CS_CTL1_SELM_MASK | CS_CTL1_DIVM_MASK | CS_CTL1_SELS_MASK
                    | CS_CTL1_DIVHS_MASK) |
    CS_CTL1_SELM__HFXTCLK | CS_CTL1_SELS__HFXTCLK;
    CS->KEY = 0; // Lock CS module from unintended accesses
}

/*********************************************************************
 * If an error occurred during the clock init, flash the LED on port 1 bit 0 indefinitely.
 *
 * This is was supplied with the sample code from TI.
 *********************************************************************/
void error(void)
{
    volatile uint32_t i;

    while (1)
    {
        P1->OUT ^= BIT0;
        for(i = 20000; i> 0; i--);          // Blink LED forever
    }
}

