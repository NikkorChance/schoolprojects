#include "msp.h"
#include "math.h"

uint8_t Read_Keypad();
void keypad_init();
void SysTick_Init();
void SysTick_delay(uint16_t delay);
void Timer_A0_init();

uint8_t num, pressed;

volatile int CCW = 0; //The number of counter clockwise turns
volatile int CW = 0; //The number of clockwise turns
volatile int buttonPressed = 0; //Rotar Button

volatile int input[3];

/**
 * Program for 326 lab 4 Part II
 * Authors: George, Bilguun
 *
 * Turning the rotary encoder in a direction and then pressing the button
 * on the encoder will print the net direction of the turns as well as the
 * difference of the turns.
 */
void main(void)
{

    SystemInit();

    input[0] = 0;
    pressed = 0;


    while (1)
    {

        if(buttonPressed) //If the rotary encoder button is pressed
        {
            //Find the difference between the two counters
            int delta = CCW - CW;

            //Reset the counters
            CCW = 0;
            CW = 0;

            if(delta > 0) //If rotated CCW more than CW
                printf("Direction was CCW: %d\n", abs(delta));

            else if(delta < 0) //If rotated CW more than CCW
                printf("Direction was CW: %d\n", abs(delta));

            else //If rotated an equal number of times
                printf("There was no displacement along the encoder: %d\n", abs(delta));

            //Reset the button flag
            buttonPressed = 0;
        }

    }
}

/**
 * Calls the functions needed to initialize the program and enables
 * interrupts.
 */
void SystemInit()
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD; // stop watchdog timer
   keypad_init(); // Call initialization functions for functionality
   __enable_irq();
   SysTick_Init();
   Port5_Init(); //Rotary encoder initialization
   Timer_A0_init();


   NVIC_EnableIRQ(PORT5_IRQn);
   __enable_irq( );
}
]

/**
 * Initialize port 5 to interface with the rotary encoder.
 *
 * BIT0 is CLK
 * BIT1 is DT
 * BIT2 is SW
 */
void Port5_Init()
{
    P5->SEL0 &= ~0x07;
    P5->SEL1 &= ~0x07;
    P5->DIR |= 0x07;
    P5->REN |= 0x07;
    P5->OUT |= 0x07;

    P5->IES |= 0x01; //CLK is rising edge, SW is falling edge
    P5->IE |= 0x05; //Enable interrupts on CLK and SW
    P5->IFG = 0;
}


/**
 * Is triggered by a rising edge from CLK or a falling edge from SW.
 *
 * If triggered by CLK, DT is checked to determine if rotation was
 * clockwise or counterclockwise.
 * If triggered by SW, a global flag is enabled.
 * If triggred by neither, all interrupt flags are reset.
 */
void PORT5_IRQHandler()
{
    //Interrupt from CLK?
    if (P5->IFG & BIT0)
    {
        //If DT is on, rotation was clockwise
        if (P5->IN & BIT1)
        {
            CW++;
        }
        //If DT isn't on, rotation was counterclockwise
        else
            CCW++;

        P5->IFG &= ~BIT0; //Disable only the CLK interrupt
    }
    //Interrupt from SW?
    else if (P5->IFG & BIT2)
    {
        buttonPressed++;
        P5->IFG &= ~BIT2; //Disable only the SW interrupt
    }
    else //If this was reached, an unhandled interrupt occurred
        P5->IFG = 0; //Reset all interrupts

}

/**
 * Initializes the keypad. All pins, 0-6 are outputs sending HIGH
 **/
void keypad_init()
{
    P4->SEL0 &= ~0x7F;
    P4->SEL1 &= ~0x7F;
    P4->DIR &= ~0xFF;
    P4->REN |= 0xFF;
    P4->OUT |= 0xFF;
// 456 are columns
// 0123 are rows
}

/**
 * Reads the keypad, and calculates the value of the button that was pressed.
 * This is done by setting every column to be an input, and then each column,
 * individually, is set to an output sending LOW. If any row is read to be LOW,
 **/
uint8_t Read_Keypad()
{
    uint8_t col, row, num;

    //Determine the column
    for (col = 0; col < 3; col++)
    {
        P4->DIR = 0x00; // Set Columns to inputs
        P4->DIR |= BIT(4 + col); // Set column 3 to output
        P4->OUT &= ~BIT(4 + col); // Set column 3 to LOW
        SysTick_delay(10); // Delay the while loop
        row = P4->IN & 0x0F; // read all rows
        while (!(P4IN & BIT0 ) | !(P4IN & BIT1 ) | !( P4IN & BIT2 )
                | !( P4IN & BIT3 )) ; //This is a debouncer
        if (row != 0x0F)
            break; // if one of the input is low, some key is pressed.
    }

    P4->DIR = 0x00; // Set Columns to inputs

    //Determine the button that was pressed
    if (col == 3)
        return 0;
    if (row == 0x0E)
        num = 0 + col + 1; // key in row 0
    else if (row == 0x0D)
        num = 3 + col + 1; // key in row 1
    else if (row == 0x0B)
        num = 6 + col + 1; // key in row 2
    else if (row == 0x07)
        num = 9 + col + 1; // key in row 3
    else
        num = 99;


    input[0] = num; //Saves the number to the input[0] global variable
    return num; //Returns as pressed
}

/**
 * Initializes Systick to have max load, no interrupts, and be on.
 **/
void SysTick_Init()
{
    SysTick->CTRL = 0;
    SysTick->LOAD = 0x00FFFFFF; //Use max load
    SysTick->VAL = 0;
    SysTick->CTRL = 0X00000005; //Core clock, enabled
}

/**
 * Uses an input of mili seconds to delay the system.
 *
 * @delay the desired number of miliseconds to delay
 **/
void SysTick_delay(uint16_t delay)
{
    SysTick->LOAD = ((delay * 3000) - 1);
    SysTick->VAL = 0;
    while ((SysTick->CTRL & 0x00010000) == 0)
        ;
}

