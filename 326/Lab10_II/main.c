#include "msp.h"

enum Bistate
{
    Step1, Step2, Step3, Step4
};
enum direction
{
    CCW, CW
};

#define BILINEA  BIT3
#define BILINEB  BIT6
#define BILINEAP BIT5
#define BILINEBP BIT7

#define BITURNDELAY 20

volatile uint8_t direction = CCW;
volatile uint8_t NextWire = Step1;
volatile uint8_t button6Flag = 0;
volatile uint8_t button7Flag = 0;
volatile float motorDeg;

/**
 *
 */
void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer
    System_Init();


    double steps = 0;


    while(1)
    {
        uint16_t potVal = Potentiometer_Position();

        float potPercent = ((float)potVal/((float)(16384-1))); //Calculate the position as a percent
        int potDegree = potPercent * 315;  //Use the percent and calculate the

        if(motorDeg < potDegree - 2)
        {
            BiMotor_Clockwise(1);
            motorDeg += .525;
        }
        else if(motorDeg > potDegree + 2)
        {
            uint8_t motorStep = 1;
            BiMotor_Counterclockwise(1);
            motorDeg -= .525;
        }

    }

}

void matchMotor()
{

}

/**********************************************************************
 * Performs all functions calls and register adjustments to initialize
 * the program.
 *********************************************************************/
void System_Init()
{
    __enable_irq();
    NVIC_EnableIRQ(PORT1_IRQn);
    SysTick_Init();
    ADC14_init();
    Port2_Init(); //Bipolar Motor
    Port1_Init(); //Buttons
    Motor_Position_Init(); //Move the motor to the 0 position.
}

/**********************************************************************
 * Runs the bipolar motor so through 610 steps to ensure that the
 * motor is in the zero degree position.
 *********************************************************************/
uint8_t Motor_Position_Init()
{
    int step = 0;

    //Run the motor through 600 steps.
    for(step = 0; step < 610; step++)
    {
        BiMotor_Counterclockwise(1);
    }
    motorDeg = 0; //Set the motor position degree to zero
}

/**********************************************************************
 * Accepts the desired number of steps for the motor to turn in the
 * counter clockwise direction, and runs the motor through that many
 * steps.
 *
 *
 * @param steps The number of steps to rotate the motor
 *********************************************************************/
void BiMotor_Counterclockwise(uint8_t steps)
{
    steps++;  //Increment steps by 1 to accommodate the NextWire change

    NextWire++; //Change the next wire to be the current wire

    //If NextWire was pushed passed LineBp, NextWire should be LineA
    if (NextWire > Step4)
        NextWire = Step1;

    //Turn off all wires
    P2->OUT &= ~0xF0;

    //Run the motor through every step
    while (steps > 0)
    {
        RunBiMotorClockWise();
        steps--;
    }
}

/**********************************************************************
 * Accepts the desired number of steps for the motor to turn in the
 * clockwise direction, and runs the motor through that many steps.
 *
 *
 * @param steps The number of steps to rotate the motor
 *********************************************************************/
void BiMotor_Clockwise(uint8_t steps)
{
    steps++; //Increment steps by 1 to accommodate the NextWire change

    NextWire--; //Change the next wire to be the current wire

    if (NextWire == 255)
        NextWire = Step1; //Subtracting from LineA will cause roll over, this corrects that

    //Turn off all wires
    P2->OUT &= ~0xF0;

    //Run the motor through every step
    while (steps > 0)
    {
        RunBiMotorCounterClockWise();
        steps--;
    }
}

/**********************************************************************
 * Will run one step of the motor based on the current state of the motor
 *********************************************************************/
void RunBiMotorCounterClockWise()
{
    switch (NextWire)
    {
    case (Step1):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEAP | BILINEBP;
        P2->OUT |= BILINEA | BILINEB;
        NextWire = Step2;
        break;
    case (Step2):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEBP;
        P2->OUT |= BILINEB | BILINEAP;
        NextWire = Step3;
        break;
    case (Step3):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEB;
        P2->OUT |= BILINEAP | BILINEBP;
        NextWire = Step4;
        break;
    case (Step4):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEB | BILINEAP;
        P2->OUT |= BILINEA | BILINEBP;
        NextWire = Step1;
        break;
    }
    SysTick_3MHz_delay(BITURNDELAY);
}

/**********************************************************************
 * Will run one step of the motor based on the current state of the motor
 *********************************************************************/
void RunBiMotorClockWise()
{
    switch (NextWire)
    {
    case (Step1):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEAP | BILINEBP;
        P2->OUT |= BILINEA | BILINEB;
        NextWire = Step4;
        break;
    case (Step2):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEBP;
        P2->OUT |= BILINEB | BILINEAP;
        NextWire = Step1;
        break;
    case (Step3):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEA | BILINEB;
        P2->OUT |= BILINEAP | BILINEBP;
        NextWire = Step2;
        break;
    case (Step4):
        P2->OUT &= ~0xF0;
        P2->OUT &= ~BILINEB | BILINEAP;
        P2->OUT |= BILINEA | BILINEBP;
        NextWire = Step3;
        break;
    }
    SysTick_3MHz_delay(BITURNDELAY);
}

/**********************************************************************
 * Setup a bit 6 port 1 as an input.
 *********************************************************************/
void Port1_Init()
{
    //GPIO on bit 6,7
    P1->SEL0 &= ~BIT6 | BIT7;
    P1->SEL1 &= ~BIT6 | BIT7;
    //Input on bit 6,7
    P1->DIR &= ~BIT6 | BIT7;
    //Pull up/down resistor on bit 6,7
    P1->REN |= BIT6 | BIT7;
    //Pull up on bit 6,7
    P1->OUT |= BIT6 | BIT7;

    //Interrupt on a falling edge
    P1->IES &= ~BIT6 | BIT7;
    //All interrupt flags are disabled on port 1
    P1->IFG = 0;
    //Enable the interrupt on bit 6,7
    P1->IE |= BIT6 | BIT7;
}

/**********************************************************************
 * If the button on port 6 is pressed, set a flag so that the button is debonced.
 * If the button on port 7 is pressed, set a flag so that the button is debonced.
 *
 *********************************************************************/
void PORT1_IRQHandler()
{
    if (P1->IFG & BIT6)
    {
        button6Flag = 1;
        button7Flag = 0;
        P1->IFG &= ~BIT6;
    }
    else if (P1->IFG & BIT7)
    {
        button7Flag = 1;
        button6Flag = 0;
        P1->IFG &= ~BIT7;
    }
    else
        P1->IFG = 0; //Disable all interrupt flags if none were handled.
}

/**********************************************************************
 * Setup port 2 to run a motor
 *
 * P2.1 is LineA
 * P2.2 is LineA'
 * P2.2 is LineB
 * P2.3 is LineB'
 *********************************************************************/
void Port2_Init()
{
    //GPIO on bits 4,5,6,7
    P2->SEL0 &= ~0xF8;
    P2->SEL1 &= ~0xF8;
    //Output on BIT4,5,6,7
    P2->DIR |= 0xF8;
    //All outputs are zero
    P2->OUT = 0;
}

/**********************************************************************
 * Check if the button was pressed. If it was, set the direction
 *********************************************************************/
uint8_t Button6Debounce()
{
    static uint16_t button = 0; //Current debounce status
    button = (button << 1) | (P1->IN & BIT6 ) | 0xe000;

    if (button == 0xf000)
    {
        button6Flag = 0;
        direction = CCW;
        return 1;
    }

    return 0;
}

/**********************************************************************
 * Check if the button was pressed. If it was, set the direction
 *********************************************************************/
uint8_t Button7Debounce()
{
    static uint16_t button = 0; //Current debounce status
    button = (button << 1) | (P1->IN & BIT7 ) | 0xe000;

    if (button == 0xf000)
    {
        button7Flag = 0;
        direction = CW;
        return 1;
    }

    return 0;
}

/**********************************************************************
 * Initializes Systick to have maximum load, no interrupts, and to be enabled
 **********************************************************************/
void SysTick_Init()
{
    SysTick->CTRL = 0;
    SysTick->LOAD = 0x00FFFFFF; //Max load
    SysTick->VAL = 0;
    SysTick->CTRL = 0X00000005; //Use core clock, enabled
}

/**********************************************************************
 * Uses an input of mili seconds to delay the system.
 **********************************************************************/
void SysTick_3MHz_delay(uint16_t delay)
{
    SysTick->LOAD = ((delay * 3000) - 1);
    SysTick->VAL = 0;
    while ((SysTick->CTRL & 0x00010000) == 0)
        ;
}

void ADC14_init(void)
{
    P5SEL0 |= 0X20;                     // configure pin 5.5 for A0 input
    P5SEL1 |= 0X20;
    ADC14->CTL0 &= ~0x00000002;         // disable ADC14ENC during configuration
    ADC14->CTL0 |= 0x04400110;        // S/H pulse mode, SMCLK, 16 sample clocks
    ADC14->CTL1 = 0x00000030;          // 14 bit resolution
    ADC14->CTL1 |= 0x00000000;        // Selecting ADC14CSTARTADDx mem0 REGISTER
    ADC14->MCTL[0] = 0x00000000;            // ADC14INCHx = 0 for mem[0]
// ADC14->MCTL[0] = ADC14_MCTLN_INCH_0;
    ADC14->CTL0 |= 0x00000002; // enable ADC14ENC, starts the ADC after configuration
}

uint16_t Potentiometer_Position()
{
    static volatile uint16_t result;
    float nADC;

    ADC14->CTL0 |= 1;              //start conversation
    while (!ADC14->IFGR0)
        ;          //wait for conversation to complete
    result = ADC14->MEM[0];            // get the value from the ADC
    return result;
//    nADC = (result * 3.3) / 16384; // Calculate the voltage received

}
